/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include "link.h"

struct _Link
{
  gint src_x;
  gint src_y;
  gint src_width;
  gint src_height;

  gint dst_x;
  gint dst_y;
  gint dst_width;
  gint dst_height;
};

Link *
link_new (void)
{
  Link *link = g_malloc0 (sizeof (Link));
  return link;
}

void
link_destroy (Link *link)
{
  g_free (link);
}

gboolean
link_contains (Link *link,
               gint  x,
               gint  y)
{
  if (link->src_x                    < x &&
      link->src_x + link->src_width  > x &&
      link->src_y                    < y &&
      link->src_y + link->src_height > y)
    return TRUE;
  return FALSE;
}

void
link_set_src (Link *link,
              gint  x,
              gint  y,
              gint  width,
              gint  height)
{
  link->src_x      = x;
  link->src_y      = y;
  link->src_width  = width;
  link->src_height = height;
}

void
link_set_dst (Link *link,
              gint  x,
              gint  y,
              gint  width,
              gint  height)
{
  link->dst_x      = x;
  link->dst_y      = y;
  link->dst_width  = width;
  link->dst_height = height;
}

void
link_get_src (Link *link,
              gint *x,
              gint *y,
              gint *width,
              gint *height)
{
  if (x)
    *x = link->src_x;
  if (y)
    *y = link->src_y;
  if (width)
    *width = link->src_width;
  if (height)
    *height = link->src_height;
}

void
link_get_dst (Link *link,
              gint *x,
              gint *y,
              gint *width,
              gint *height)
{
  if (x)
    *x = link->dst_x;
  if (y)
    *y = link->dst_y;
  if (width)
    *width = link->dst_width;
  if (height)
    *height = link->dst_height;
}

static void
op_scale (Canvas *canvas,
          gint    dst_x,
          gint    dst_y,
          gint    dst_width,
          gint    dst_height,
          gint    src_x,
          gint    src_y,
          gint    src_width,
          gint    src_height)
{
  gint x,y;
  guchar *src_buf;
  guchar *dst_buf;
  CanvasRect *src_rect;
  CanvasRect *dst_rect;

  src_buf = g_malloc0 (src_width * src_height * 4);
  if (!src_buf)
    return;
  dst_buf = g_malloc0 (dst_width * dst_height * 4);
  if (!dst_buf)
    {
      g_free (src_buf);
      return;
    }

  dst_rect = canvas_rect_new (canvas, dst_x, dst_y, dst_width, dst_height);
  canvas_rect_export_buf (dst_rect, dst_buf);
  g_object_unref (dst_rect);
  
  for (y=0;y<src_height;y++)
    for (x=0;x<src_width;x++)
      {
        gint i;
        for (i=0;i<4;i++)
          {
            gint u,v;
            u = x * dst_width / src_width;
            v = y * dst_height / src_height;
            src_buf [(y * src_width + x) * 4 + i] = 
              dst_buf [(v * dst_width + u) * 4 + i];
          }
      }
  g_free (dst_buf);
  
  src_rect = canvas_rect_new (canvas, src_x, src_y, src_width, src_height);
  canvas_rect_import_buf (src_rect, src_buf);
  g_object_unref (src_rect);

  g_free (src_buf);
}

void
link_redraw (Canvas *canvas,
             Link   *link)
{
  gint sx,sy,sw,sh;
  gint dx,dy,dw,dh;

  link_get_src (link, &sx, &sy, &sw, &sh);
  link_get_dst (link, &dx, &dy, &dw, &dh);

  op_scale (canvas, dx, dy, dw, dh, sx, sy, sw, sh);

  canvas_outline_box (canvas, sx, sy, sw, sh, 0, 0, 65536, 65536);
  canvas_outline_box (canvas, sx-1, sy-1, sw+2, sh+2, 0, 0, 65536, 65536);
}

/******/

#include <stdio.h>

static GSList   *links = NULL;

Link *
links_get (gint x,
           gint y)
{
  GSList *list = links;
  while (list)
    {
      Link *link = list->data;

      if (link_contains (link, x, y))
        return link;

      list = list->next;
    }
  return NULL;
}

void
links_add (Link   *link)
{
  links = g_slist_append (links, link);
}

void
links_delete (Link   *link)
{
  links = g_slist_remove (links, link);
  link_destroy (link);
}

void
links_load (const char *path)
{
  FILE *file;
  char buf[256];
  /*char path[256];
  sprintf (path, "%s/links", canvas->path);*/

  file = fopen (path, "r");
  if (!file)
    {
      g_warning ("unable to open link file %s for reading", path);
      return;
    }

  while (fgets (buf, 256, file))
    {
      Link *link = link_new ();
      gint src_x, src_y, src_width, src_height;
      gint dst_x, dst_y, dst_width, dst_height;

      sscanf (buf, "%i %i %i %i - %i %i %i %i\n",
         &src_x, &src_y, &src_width, &src_height,
         &dst_x, &dst_y, &dst_width, &dst_height);

      link_set_src (link, src_x, src_y, src_width, src_height);
      link_set_dst (link, dst_x, dst_y, dst_width, dst_height);

      links_add (link);
    }

  fclose (file);
  //canvas_draw_links (canvas);
}

void
links_save (const gchar *path)
{
  FILE *file;
  GSList *list = links;
  //char path[256];
  //sprintf (path, "%s/links", canvas->path);

  file = fopen (path, "w");
  if (!file)
    {
      g_warning ("unable to open %s for writing", path);
      return;
    }

  while (list)
    {
      Link *link = list->data;
      gint src_x, src_y, src_width, src_height;
      gint dst_x, dst_y, dst_width, dst_height;

      link_get_src (link, &src_x, &src_y, &src_width, &src_height);
      link_get_dst (link, &dst_x, &dst_y, &dst_width, &dst_height);
      
      fprintf (file, "%i %i %i %i - %i %i %i %i\n",
         src_x, src_y, src_width, src_height,
         dst_x, dst_y, dst_width, dst_height);

      list = list->next;
    }

  fclose (file);
}

void
links_destroy (void)
{
  GSList *list;

  while ((list = links))
    {
      Link *link = list->data;
      links = g_slist_remove (links, link);
      link_destroy (link);
    }
}
