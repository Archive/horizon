/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _LINK_H
#define _LINK_H

#include "canvas/canvas.h"

typedef struct _Link Link;

Link *   link_new      (void);

void     link_destroy  (Link   *link);

gboolean link_contains (Link   *link,
                        gint    x,
                        gint    y);

void     link_set_src  (Link   *link,
                        gint    x,
                        gint    y,
                        gint    width,
                        gint    height);

void     link_set_dst  (Link   *link,
                        gint    x,
                        gint    y,
                        gint    width,
                        gint    height);


void     link_get_src  (Link   *link,
                        gint   *x,
                        gint   *y,
                        gint   *width,
                        gint   *height);

void     link_get_dst  (Link   *link,
                        gint   *x,
                        gint   *y,
                        gint   *width,
                        gint   *height);

void     link_redraw   (Canvas *canvas,
                        Link   *link);

/* the following work on a global database of links */
void     links_add     (Link        *link);
void     links_delete  (Link        *link);
void     links_destroy (void);
void     links_load    (const gchar *path);
void     links_save    (const gchar *path);
Link *   links_get     (gint         x,
                        gint         y);
void     links_redraw  (Canvas      *canvas);

#endif
