/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

/* XSP support temporarily disabled */
//#define USE_XSP

#include <glib.h>
#include <glib/gprintf.h>

#ifdef USE_XSP
#include <X11/extensions/Xsp.h>
#include "timer.h"
#endif

#include "config.h"
#include "stylus.h"

#define STYLUS_MAX_EVENTS  256

typedef struct StylusCalibration
{
  gint x[4];
  gint y[4];

  gint max_pressure;
  gint min_pressure;
} StylusCalibration;

typedef struct StylusEvent
{
  gint   x;
  gint   y;
  gint   pressure;  /* if pressure is non 0 it is a drag/click event */
  gulong time;
} StylusEvent;


struct _Stylus
{
  Display *dpy;


  StylusEvent events[STYLUS_MAX_EVENTS];  /* circular buffer */

  gint n;
  gint next_read;
  gint next_write;

  /* xmouse */
  gint xmouse_pressure;
  gint screen_width;
  gint screen_height;

  gint calibration_mode;
#ifdef USE_XSP
  Timer   *timer;

  gint use_xsp;

  /* calibration data */
  gint x_min; 
  gint x_max;
  gint y_min;
  gint y_max;

  gint pressure_min;  /* minimum significant pressure above 0 encountered
                        ("touchdown threshold")*/;
  gint pressure_max;  /* maximum pressure encountered */;

  gint dx;  /* cached value for x_max-x_min */
  gint dy;  /* cached value for y_max-y_min */
  gint dp;

  /* xsp */
  gint xsp_event_base;
#endif
};

static void stylus_peek_next (Stylus *stylus,
                              gint    *x,
                              gint    *y,
                              gint    *pressure,
                              gulong  *time);

static int stylus_process_xmotion (Stylus       *stylus,
                                   XMotionEvent *xmotion_event);
#ifdef USE_XSP
static int stylus_process_xsp     (Stylus                 *stylus,
                                   XSPRawTouchscreenEvent *xsp_event);
static void stylus_recalculate_deltas (Stylus *stylus);
#endif

void
stylus_set_screen_size (Stylus *stylus,
                        gint    screen_width,
                        gint    screen_height)
{
  stylus->screen_width  = screen_width;
  stylus->screen_height = screen_height;
}

Stylus *
stylus_init (Display *dpy)
{
  Stylus *stylus = g_malloc0 (sizeof (Stylus));
  if (!dpy)
    {
      g_print ("no dpy passed\n");
      g_assert (0);
    }
  stylus->dpy = dpy;

  stylus_set_screen_size (stylus, 800, 480);

#ifdef USE_XSP
   /* get xsp event base */
  {gint foo,bar,baz;
   XSPQueryExtension(dpy,
                     &stylus->xsp_event_base,
                     &foo,
                     &bar,
                     &baz);
  }

  if (stylus->xsp_event_base <= 0)
    {
      stylus->use_xsp = 0;
    }
  else
    {
      stylus->use_xsp = 1;
    }

  if (stylus->use_xsp)
    {
      XSPSetTSRawMode(stylus->dpy, True);
    }
  stylus->timer = timer_new ();
#endif


   //stylus_set_calibration (stylus, "replace this with pippin's values ;)");
  return stylus;
}

void
stylus_destroy (Stylus  *stylus)
{
#ifdef USE_XSP
  if (stylus->use_xsp)
    {
      XSPSetTSRawMode(stylus->dpy, False);
    }
  XSync(stylus->dpy, True);
  XFlush(stylus->dpy);
  timer_free (stylus->timer);
#endif

  g_free (stylus);
}

gint
stylus_set_calibration (Stylus      *stylus,
                        const gchar *calibration)
{
#ifdef USE_XSP
  gint v[7];
  gchar *s=(char *) calibration;

#define get_int(dest) if(s!=NULL && *s!='\0') {gchar *ts;dest=g_ascii_strtoull(s,&(ts), 10);s=ts;}

  get_int(v[0]);
  get_int(v[1]);
  get_int(v[2]);
  get_int(v[3]);
  get_int(v[4]);
  get_int(v[5]);
  get_int(v[6]);

  stylus->x_max = - v[2] / v[0];
  stylus->x_min = 1.0 * stylus->screen_width / v[0] * 65536 + stylus->x_max;
  stylus->y_max = - v[5] / v[4];
  stylus->y_min = 1.0 * stylus->screen_height / v[4] * 65536 + stylus->y_max;
  stylus->pressure_min = 105;
  stylus->pressure_max = 125;

  stylus_recalculate_deltas (stylus);
#undef get_int
#endif
  return 0;
}

static gint
stylus_add_event (Stylus *stylus,
                  gint    x,
                  gint    y,
                  gint    pressure,
                  gulong  time)
{
  if (stylus->n >= STYLUS_MAX_EVENTS)
    {
      g_print ("discarding event, buffer full\n");
      return -1;
    }
  stylus->events[stylus->next_write].x = x;
  stylus->events[stylus->next_write].y = y;
  stylus->events[stylus->next_write].pressure = pressure;
  stylus->events[stylus->next_write].time=time;

  stylus->n++;
  stylus->next_write++;
  stylus->next_write %= STYLUS_MAX_EVENTS;
  return 0;
}

int
stylus_process_event (Stylus *stylus,
                      XEvent *event)
{
  #ifdef USE_XSP
  if (stylus->use_xsp && 
      event->type == stylus->xsp_event_base)
    {
      stylus_process_xsp (stylus, (XSPRawTouchscreenEvent *) event);
      return 1;
    }
  #endif
  switch (event->type)
    {
      case MotionNotify:
        stylus_process_xmotion (stylus, (XMotionEvent*)event);
        return 1;
      case ButtonPress:
        stylus->xmouse_pressure = 65536*1.0;
        stylus_add_event (stylus, ((XMotionEvent*)event)->x * 65536 / stylus->screen_width,
                                  ((XMotionEvent*)event)->y * 65536 / stylus->screen_height, 65536,
                                  ((XMotionEvent*)event)->time
                                  );
        return 1;
      case ButtonRelease:
        stylus->xmouse_pressure = 65536*0.0;
        return 1;
      default:
        return 0;
    }
  return 0;
}

void
stylus_flush (Stylus *stylus)
{
  while (stylus_has_next (stylus))
    {
      stylus_get_next (stylus, NULL, NULL, NULL, NULL);
    }
}


gint
stylus_has_next (Stylus *stylus)
{
  return (stylus->n);
}

gint
stylus_get_next (Stylus *stylus,
                 gint   *x,
                 gint   *y,
                 gint   *pressure,
                 gulong *time)
{
  if (!stylus_has_next (stylus))
    return 0;

  stylus_peek_next (stylus, x, y, pressure, time);
  stylus->n--;
  stylus->next_read++;
  stylus->next_read %= STYLUS_MAX_EVENTS;

  return 1;
}

static void stylus_peek_next (Stylus *stylus,
                              gint   *x,
                              gint   *y,
                              gint   *pressure,
                              gulong *time)
{
  /* argument and state checking done in stylus_get_next */
  if (x)
    *x = stylus->events[stylus->next_read].x;
  if (y)
    *y = stylus->events[stylus->next_read].y;
  if (pressure)
    *pressure = stylus->events[stylus->next_read].pressure;
  if (time)
    *time = stylus->events[stylus->next_read].time;
}

static gint
stylus_process_xmotion (Stylus       *stylus,
                        XMotionEvent *mev)
{
  /* additional overhead of looking up display size should be
   * neglible on the host, it is the xsp case that should work
   * as fast as possible.
   */
  int x = mev->x;
  int y = mev->y;

  if (x>=stylus->screen_width)
    x=799;
  if (y>=stylus->screen_height)
    y=479;
  if (x<0)
    x=0;
  if (y<0)
    y=0;
    
  
  x = x * 65536 / stylus->screen_width;
  y = y * 65536 / stylus->screen_height;

  
  if (stylus->xmouse_pressure > 0 ||
      stylus->calibration_mode)
    {
      stylus_add_event (stylus, x,y, stylus->xmouse_pressure, mev->time);
    }
  return 0;
}

/*   266, 3692,     341,   3647   */

#ifdef USE_XSP

static gint histogram[150]={0,0,0};
static gint total=0;

static gint
stylus_process_xsp (Stylus                 *stylus,
                    XSPRawTouchscreenEvent *xsp_event)
{
  gint x, y, p;

  x = xsp_event->x;
  y = xsp_event->y;
  p = xsp_event->pressure;

  histogram[p]++;
  total++;

  if (stylus->calibration_mode)
    {
      if (x < stylus->x_min)
        stylus->x_min = x;
      if (x > stylus->x_max)
         stylus->x_max = x;
      if (y < stylus->y_min)
         stylus->y_min = y;
      if (y > stylus->y_max)
         stylus->y_max = y;
      
      { /* ignore the lower 1/10 of available pressure data (that
           seems to provide sane results on my device, need some more
           testing of misc devices to figure out if this assumption
           holds */
        int i;
        int sum=0;
        for (i=0;i<150;i++)
          {
            sum+=histogram[i];
            if (sum > total / 10)
              {
                stylus->pressure_min = i;
                break;
              }
          }
      }
      if (p > stylus->pressure_max)
         stylus->pressure_max = p;

      stylus_recalculate_deltas (stylus);
    }
  
  {
    x -= stylus->x_min;
    y -= stylus->y_min;

    x = 65536 - 65536 * x / stylus->dx;
    y = 65536 - 65536 * y / stylus->dy;
  }

  if (p > stylus->pressure_min || 
      stylus->calibration_mode)
  {
     p -= stylus->pressure_min;
     p = 65536 * p / stylus->dp;
     stylus_add_event (stylus, x, y, p, timer_get_elapsed_ms (stylus->timer));
  } 
  
  return 0;
}

static void
stylus_recalculate_deltas (Stylus *stylus)
{
    stylus->dx = (stylus->x_max-stylus->x_min); 
    if (stylus->dx == 0)
      stylus->dx = 1;
    stylus->dy = (stylus->y_max-stylus->y_min); 
    if (stylus->dy == 0)
      stylus->dy = 1;
    stylus->dp = (stylus->pressure_max-stylus->pressure_min); 
    if (stylus->dp == 0)
      stylus->dp = 1;
}

#endif


void
stylus_calibration_start (Stylus *stylus)
{
  stylus->calibration_mode=1;
#ifdef USE_XSP
  {
    int i;
    for (i=0;i<150;i++)
      histogram[i]=0;
    total = 0;
  }

  /* sets extreme values */
  stylus->x_min = stylus->y_min =  65536*10;
  stylus->x_max = stylus->y_max = -65536*10;
  stylus->pressure_min = 65536;
  stylus->pressure_max = 0;
#endif
}

void
stylus_calibration_stop (Stylus *stylus)
{
#ifdef USE_XSP
#if 0
    {
      int i;
      for (i=0;i<150;i++)
        {
          fprintf (stderr, "(%i: %i)", i, histogram[i]);
        }
      fprintf (stderr ,"\n");
    }
#endif
  stylus->calibration_mode=0;
#endif
}
