#include <glib.h>
#include "teleport.h"
#include "timer.h"

typedef struct _MotionPath MotionPath;

struct _MotionPath
{
  gint        x;
  gint        y;
  gint        scale;
  int        ms;
  MotionPath *next;
};

static MotionPath *
motion_path_add (MotionPath *path,
                 gint        x,
                 gint        y,
                 gint        scale,
                 gint        ms)
{
  MotionPath *ret = path;

  if (path)
    {
      while (path->next)
        path=path->next;
    }
  if (path)
    {
      path->next = g_malloc0 (sizeof (MotionPath));
      path=path->next;
    }
  else
    {
      path = g_malloc0 (sizeof (MotionPath));
      ret = path;
    }
  path->x=x;
  path->y=y;
  path->scale=scale;
  path->ms=ms;
  return ret;
}

static void
motion_path_destroy      (MotionPath *path)
{
  while (path)
    {
      MotionPath *iter=path->next;
      g_free (path);
      path=iter;
    }
}

static void
motion_path_interpolate  (MotionPath *path,
                          gint        time,
                          gint       *x,
                          gint       *y,
                          gint       *scale)
{
  gint elapsed=0;

  while (path->next && elapsed + path->ms < time)
    {
      elapsed+=path->ms;
      path=path->next;
    }
  if (!path->next)
    {
      if (x)
        *x  = path->x;
      if (y)
        *y  = path->y;
      if (scale)
        *scale = path->scale;
      return;
    }
  {
  gint interval = path->ms;
  gint portion  = time-elapsed;

  if (x)
    *x = path->x * (interval-portion)/interval + path->next->x * portion/interval;
  if (y)
    *y = path->y * (interval-portion)/interval + path->next->y * portion/interval;
  if (scale)
    *scale = path->scale * (interval-portion)/interval + path->next->scale * portion/interval;
  }
}

void
horizon_teleport (Horizon     *horizon,
                  gint         dest_x,
                  gint         dest_y,
                  gint         dest_scale,
                  gint         time)
{
    View       *view     = (View*)horizon->easel;
    Timer      *timer    = timer_new ();
    MotionPath *path     = NULL;
    gint        max      = time;

    path = motion_path_add (path, view_get_center_x (view),
       view_get_center_y (view),
       view_get_scale (view), max);

    path = motion_path_add (path, dest_x, dest_y,
       dest_scale, 5);

    time=0;
    while (time<max)
      {
        gint x,y,scale;

        time = timer_get_elapsed_ms (timer);
        
        motion_path_interpolate (path, time, &x, &y, &scale);

        view_set_center_scale (view, scale);
        view_set_center_x (view, x);
        view_set_center_y (view, y);

        canvas_flush_dirty (view_get_canvas (view));
        box_render (horizon->root);
        views_flush ();
      }

    motion_path_destroy (path);
    timer_free (timer);
  }
