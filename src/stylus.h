/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef PEN_H
#define PEN_H

#include <glib.h>
#include <X11/Xlib.h>

typedef struct _Stylus Stylus;

/**
 *  create a new stylus, for input events coming from
 *
 *  @param display the X display to handle events from
 *
 *  @return a new stylus object
 * */
Stylus *  stylus_init              (Display    *display);

/**
 *  destroy a stylus, frees the resources associated
 *  with a stylus.
 *
 *  @param stylus the stylus to destroy
 */
void    stylus_destroy             (Stylus      *stylus);

/**
 * sets the calibration of a stylus to prior configuration
 *
 * @param stylus the stylus to change
 * @param calibration \0 terminated string containing calibration info
 *
 * @return non-zero on failure
 */
int     stylus_set_calibration   (Stylus         *stylus,
                                  const gchar    *calibration);

/**
 *  puts the stylus into calibration mode, in calibration mode the
 *  tablet will automatically calibrate the ranges used for outputting
 *  values.
 *
 *  In calibration mode the device will also return pressure information
 *  for extremely light touches (or pressure==0 when emulating with a mouse)
 *
 *  @param stylus the stylus to calibrate
 */
void    stylus_calibration_start (Stylus         *stylus);

/**
 *  Stop calibrating, this should be called when enough data
 *  as been collected, (needs a cooperating user, e.g. visit all
 *  corners in one stroke and lift stylus in the middle of the pad.
 */
void    stylus_calibration_stop  (Stylus         *stylus);

/**
 *  Process a mouse/tablet event, and add the resulting stylus event
 *  to the stylus's event queue.
 *
 *  Pass it all XEvents delivered to a fullscreen window, and it
 *  will indicate which events it found relevant (mouse and tablet events).
 *
 *  @return 1 if the event was processed 0 otherwize.
 */
gint    stylus_process_event     (Stylus         *stylus,
                                  XEvent         *event);

/**
 *  Check if the stylus's event queue has any data
 *
 *  @param stylus the stylus to query
 *  @return 0 if there is no event non-zero if there is any events.
 */
gint    stylus_has_next          (Stylus        *stylus);

/**
 *  Retrieve a stylus event.
 *
 *  Coordinates and pressure information is in the interval 0..65536 multiply
 *  the returned values with the corresponding screen dimension to get 16.16
 *  fixed point subpixel values.
 *
 *  Note: If passed in storage pointers are NULL, the data is not stored.
 *
 *  @param stylus the instance to retrieve data from
 *  @param x   location for storing the c coordinate of the event.
 *  @param y   location for storing the y coordinate of the event.
 *  @param pressure location for storing the pressure info for the event.
 *  @param time 
 * 
 *  @return 0 if no event was processed, 1 if an event was returned.
 */
gint   stylus_get_next          (Stylus        *stylus,
                                 gint          *x,
                                 gint          *y,
                                 gint          *pressure,
                                 gulong        *time);

/**
 *  Empties the event buffer. 
 */
void   stylus_flush             (Stylus        *stylus);


void   stylus_set_screen_size   (Stylus        *stylus,
                                 gint           width,
                                 gint           height);

#endif
