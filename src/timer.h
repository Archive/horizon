/*_license(*/
/* license automatically inserted by boilc
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Copyright 2004 Øyvind Kolås
 *
 */

          /*) *//*_top(*/
#ifndef _header_timer
#define _header_timer

typedef struct _Timer Timer;

/*)*/

/*_body(*/
void      timer_start (Timer * self);

void      timer_stop (Timer * self);

Timer    *timer_new (void);

void      timer_free (Timer * self);

double    timer_get_elapsed_seconds (Timer * self);

long      timer_get_elapsed_ms (Timer * self);

/*)*/

/*_bottom(*/

#endif /* _header_timer */
/*)*/
