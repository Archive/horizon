/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "brush.h"

#include "config.h"

/* TODO: Make tile_stamp and tile_clone_stamp use more common code somehow. */

static void tile_stamp (Tile *tile,
                        gint x,
                        gint y,
                        gint radius,
                        gint opacity,
                        gint hardness,
	                gint r,
	                gint g,
	                gint b)
{
   guchar *data;
   gint tile_width = tile_get_width (tile);
   gint tile_height = tile_get_height (tile);
   guint radius_squared = (radius*radius);
   guint inner_radius_squared = (radius*hardness/65536);
   gint  soft_range;
   gint i,j;
  
   gint start_y, end_y;
   gint start_x, end_x;
   
   guint *hrowbuf;
/*   guint *vrowbuf;*/

   if (radius == 0)
     return;
   
   start_y = MAX (0, (y - radius) / SPP);
   end_y = MIN (tile_height - 1, (y + radius + (SPP - 1)) / SPP);
   start_x = MAX (0, (x - radius) / SPP);
   end_x = MIN (tile_width - 1, (x + radius + (SPP - 1)) / SPP);
   
   inner_radius_squared = (inner_radius_squared * inner_radius_squared);
   soft_range = radius_squared - inner_radius_squared;

   r = r * 255/65536;
   g = g * 255/65536;
   b = b * 255/65536;
   
   hrowbuf = g_malloc (sizeof (guint) * (end_x - start_x + 1));
/*   vrowbuf = g_malloc (sizeof (guint) * (end_y - start_y + 1));*/
   
   for (i= start_x; i <= end_x; i++)
     hrowbuf[i-start_x] = ((i*SPP)-x)*((i*SPP)-x);
  
   /* revision increase before changing contents */
   /* this could also be interpreted as a request to turn the tile data
    * writable, and thus be the entrypoint for COW
    */
   tile_lock (tile);
   data = tile_get_data (tile);

   /* TODO : As the brush shape is circular, almost 25% of pixels could be
    *        skipped by having each horizontal row start&end at&to
    *        the brush outline for that given row. */
   /* TODO : Remove conditionals from inside loops. */
   for (j = start_y; j <= end_y; j++)
     {
        guchar *dst = data;
	guint jcalc = ((j*SPP)-y)*((j*SPP)-y);
        dst += (j) * tile_width * 4;
        dst += (start_x) * 4;

        for (i = 0; i <= (end_x - start_x); i++)
          {
            guint o = (hrowbuf [i] + jcalc);
	    
	    if (o < inner_radius_squared)
                o = opacity;
            else if (o < radius_squared)
	      {
                o = (256 - (256 * (o-inner_radius_squared) / (soft_range))) * opacity / 256;
                
                if (o>65536)
                  o=65536;
	      }
	     
            if ((hrowbuf [i] + jcalc) < radius_squared)
              {
                guint red   = dst[0];
                guint green = dst[1];
                guint blue  = dst[2];
             

                red   = (red   * (65536-o) + r * o) / 65536;
                green = (green * (65536-o) + g * o) / 65536;
                blue  = (blue  * (65536-o) + b * o) / 65536;
                 
                if (red>255)
                  red=255;
                if (green>255)
                  green=255;
                if (blue>255)
                  blue=255;

                dst[0] = red;
                dst[1] = green;
                dst[2] = blue;
              }

            dst+=4;
          }
     }
  tile_unlock (tile);
  g_free (hrowbuf);
/*  g_free (vrowbuf);*/
}

static void tile_clone_stamp (Tile *tile,
                        gint x,
                        gint y,
                        gint radius,
                        gint opacity,
                        gint hardness,
			gint dst_offset_x,
			gint dst_offset_y,
			Tile *src_tile,
			gint src_offset_x,
			gint src_offset_y,
		        gint copy_area_width,
			gint copy_area_height)
{
   guchar *data;
   guchar *src_data = tile_get_data (src_tile);
   gint tile_width = tile_get_width (tile);
   guint radius_squared = (radius*radius);
   guint inner_radius_squared = (radius*hardness/65536);
   gint  soft_range;
   gint i,j;
  
   gint start_y, end_y;
   gint start_x, end_x;
   
   guint *hrowbuf;
/*   guint *vrowbuf;*/

#if SPP
   gint k,l;
   radius /= SPP;
   k = x % SPP;
   l = y % SPP;
   x /= SPP;
   y /= SPP;
   src_offset_x /= SPP;
   src_offset_y /= SPP;
   dst_offset_x /= SPP;
   dst_offset_y /= SPP;
   copy_area_width /= SPP;
   copy_area_height /= SPP;
#endif
   /* radius not needed here as it's calculated in earlier. */
   start_x = dst_offset_x;
   end_x = dst_offset_x + copy_area_width - 1;
   start_y = dst_offset_y;
   end_y = dst_offset_y + copy_area_height - 1;
   
   inner_radius_squared = (inner_radius_squared * inner_radius_squared);
   soft_range = radius_squared - inner_radius_squared;

   hrowbuf = g_malloc (sizeof (guint) * (end_x - start_x + 1));
/*   vrowbuf = g_malloc (sizeof (guint) * (end_y - start_y + 1));*/
   
   for (i= start_x; i <= end_x; i++)
     hrowbuf[i-start_x] = (((i-x)*SPP)-k)*(((i-x)*SPP)-k);
   
   tile_lock (tile);
   data = tile_get_data (tile);
   
   for (j = 0; j <= (end_y - start_y); j++)
     {
        guchar *dst = data;
	guchar *src = src_data;
	guint jcalc = (((j+start_y-y)*SPP)-l)*(((j+start_y-y)*SPP)-l);
        dst += (j + dst_offset_y) * tile_width * 4;
        dst += (start_x) * 4;
	src += (j + src_offset_y) * tile_width * 4;
        src += (src_offset_x) * 4;

        for (i = 0; i <= (end_x - start_x); i++)
          {
            guint o = (hrowbuf [i] + jcalc);
	    
	    if (o < inner_radius_squared)
                o = opacity;
            else if (o < radius_squared)
	      {
             
                o = (256 - (256 * (o-inner_radius_squared) / (soft_range))) * opacity / 256;
                
                if (o>65536)
                  o=65536;
		    
	      }
	     
            if ((hrowbuf [i] + jcalc) < radius_squared)
              {
                guint red   = dst[0];
                guint green = dst[1];
                guint blue  = dst[2];

		guint src_red = src[0];
		guint src_green = src[1];
		guint src_blue = src[2];

                red   = (red   * (65536-o) + src_red * o) / 65536;
                green = (green * (65536-o) + src_green * o) / 65536;
                blue  = (blue  * (65536-o) + src_blue * o) / 65536;
                 
                if (red>255)
                  red=255;
                if (green>255)
                  green=255;
                if (blue>255)
                  blue=255;

                dst[0] = red;
                dst[1] = green;
                dst[2] = blue;
              }

	    dst+=4;
	    src+=4;
          }
     }
   tile_unlock (tile);
  g_free (hrowbuf);
/*  g_free (vrowbuf);*/
}

/*  might stamp multiple tiles, as long as radius is smaller than the tile size
 *  a maximum of 4 tiles should be affected.
 */
void canvas_stamp (Canvas *canvas, 
                          gint x,
                          gint y,
                          gint radius,
                          gint opacity,
                          gint hardness,
      	                  gint r,
	                  gint g,
	                  gint b)
{
  Tile *tile;

  gint tile_width = canvas_get_tile_width (canvas);
  gint tile_height = canvas_get_tile_height (canvas);
  
  gint center_tile_x = tile_tile (x / SPP);
  gint center_tile_y = tile_tile (y / SPP);

  if ((radius == 0) || (radius > HARD_MAX_RADIUS))
    return;

  tile_width *= SPP;
  tile_height *= SPP;

    {
      gint tx,ty;
      gint tilex, tiley;

      /*FIXME: depends on SPP and is nasty */
      tx = x % (tile_width);
      ty = y % (tile_height);

      if (x<0)
        tx += (tile_width);
      if (y<0)
        ty += (tile_height);
      
      for (tiley = tile_tile ((y - radius) / SPP); tiley <= tile_tile ((y + radius + (SPP - 1)) / SPP); tiley++)
        for (tilex = tile_tile ((x - radius) / SPP); tilex <= tile_tile ((x + radius + (SPP - 1)) / SPP); tilex++)
	  {
            tile = canvas_get_tile (canvas, tilex & 0xffffff, tiley & 0xffffff);
            tile_stamp (tile, tx + (tile_width * (center_tile_x - tilex)), ty + (tile_height * (center_tile_y - tiley)), radius, opacity, hardness, r,g,b);
            g_object_unref (G_OBJECT (tile));
	  }
    }
}

/*  might stamp multiple tiles, as long as radius is smaller than the tile size
 *  a maximum of 4 tiles should be affected.
 */
void canvas_clone_stamp (Canvas *canvas, 
                          gint x,
                          gint y,
                          gint radius,
                          gint opacity,
                          gint hardness,
			  gint offset_x,
			  gint offset_y)
{
  gint tile_width = canvas_get_tile_width (canvas);
  gint tile_height = canvas_get_tile_height (canvas);

  radius *= SPP;
  tile_width *= SPP;
  tile_height *= SPP;
  offset_x *= SPP;
  offset_y *= SPP;

  gint dst_tile_x;
  gint dst_tile_y;
  
  /* Get affected dst pixels */
  gint dst_start_x = (x - radius);
  gint dst_end_x = (x + radius);
  gint dst_start_y = (y - radius);
  gint dst_end_y = (y + radius);

  for (dst_tile_y = tile_tile (dst_start_y / SPP); dst_tile_y <= tile_tile (dst_end_y / SPP); dst_tile_y++)
    for (dst_tile_x = tile_tile (dst_start_x / SPP); dst_tile_x <= tile_tile (dst_end_x / SPP); dst_tile_x++)
      {
	Tile *dst_tile;
	
	/* Get all needed src pixels for this dst_tile */
        gint src_start_x = MAX ((dst_tile_x * tile_width + offset_x), (x - radius) + offset_x);
        gint src_end_x = MIN (((dst_tile_x + 1) * tile_width - SPP + offset_x), (x + radius) + offset_x);

        gint src_start_y = MAX ((dst_tile_y * tile_height + offset_y), (y - radius) + offset_y);
        gint src_end_y = MIN (((dst_tile_y + 1) * tile_height - SPP + offset_y), src_end_y = (y + radius) + offset_y);

	gint src_tile_x;
	gint src_tile_y;
	
	/* If this is true, then there's nothing to draw here, move along. */
	if ((src_start_x > src_end_x) || (src_start_y > src_end_y))
	  continue;

        dst_tile = canvas_get_tile (canvas, dst_tile_x & 0xffffff, dst_tile_y & 0xffffff);

	/* For every source tile that will be used in the dest tile... */
        for (src_tile_y = tile_tile (src_start_y / SPP); src_tile_y <= tile_tile (src_end_y / SPP); src_tile_y++)
          for (src_tile_x = tile_tile (src_start_x / SPP); src_tile_x <= tile_tile (src_end_x / SPP); src_tile_x++)
	    {
	      Tile *src_tile = canvas_get_tile (canvas, src_tile_x, src_tile_y);
	      
	      
	      /* Calculate common area of the current dst and src tiles so that
	         we can keep tile_clone_stamp work on single input/output tiles pair. */
	      gint src_tile_clone_x = MAX (0, src_start_x - (src_tile_x * tile_width));
	      gint src_tile_clone_y = MAX (0, src_start_y - (src_tile_y * tile_height));

	      gint dst_tile_clone_x = MAX (0, (dst_start_x - (dst_tile_x * tile_width)) + MAX (0, (src_tile_x * tile_width - offset_x) - dst_start_x));
	      gint dst_tile_clone_y = MAX (0, (dst_start_y - (dst_tile_y * tile_height)) + MAX (0, (src_tile_y * tile_height - offset_y) - dst_start_y));
	      
	      gint copy_area_width = MIN (dst_end_x - dst_start_x + SPP,
		                     MIN (MAX (SPP, dst_end_x - (dst_tile_clone_x + dst_tile_x * tile_width) + SPP),
				     MIN (tile_width - dst_tile_clone_x,
				     MIN (src_end_x - src_start_x + SPP,
				     MIN (MAX (SPP, src_end_x - (src_tile_clone_x + src_tile_x * tile_width) + SPP),
				     MIN (tile_width - src_tile_clone_x,
					  tile_width
					 ))))));
	      gint copy_area_height = MIN (dst_end_y - dst_start_y + SPP,
		                      MIN (MAX (SPP, dst_end_y - (dst_tile_clone_y + dst_tile_y * tile_height) + SPP),
				      MIN (tile_height - dst_tile_clone_y,
				      MIN (src_end_y - src_start_y + SPP,
				      MIN (MAX (SPP, src_end_y - (src_tile_clone_y + src_tile_y * tile_height) + SPP),
				      MIN (tile_height - src_tile_clone_y,
					   tile_height
					  ))))));

              tile_clone_stamp (dst_tile, 
			        x - (tile_width * dst_tile_x),
			        y - (tile_height * dst_tile_y),
			        radius, opacity, hardness,
				dst_tile_clone_x,
				dst_tile_clone_y,
			        src_tile,
			        src_tile_clone_x,
				src_tile_clone_y,
				copy_area_width,
				copy_area_height);

	      g_object_unref (G_OBJECT (src_tile));
	    }
	g_object_unref (G_OBJECT (dst_tile));
      }
}
