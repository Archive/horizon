/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <glib.h>
#include <math.h>
#include "path.h"
#include "config.h"

/* the path is a series of commands in a linked list,
 * the first node is the path state, it contains the pens last coordinates.
 *
 * The second node is always a move to, and specifies the start of the
 * first segment. Curves are automatically turned into lines upon generation.
 *              
 * s: 40, 50   
 * m: 100, 100 
 * l: 200, 100
 * l: 200, 50
 */

typedef struct Point
{
  gint x;
  gint y;
} Point;

struct _Path
{
  Point point;
  Path *next;
  char  type;
};

typedef struct _Head Head;

struct _Head
{
  Path  path;
  Tool *tool;
};


/*** fixed point subdivision bezier ***/

/* linear interpolation between two point */
static void
lerp (Point *dest,
      Point *a,
      Point *b,
      gint t)
{
  dest->x = a->x + (b->x-a->x) * t / 65536;
  dest->y = a->y + (b->y-a->y) * t / 65536;
}

#define iter1(N) \
    try = root + (1 << (N)); \
    if (n >= try << (N))   \
    {   n -= try << (N);   \
        root |= 2 << (N); \
    }

static inline guint isqrt (guint n)
{
    guint root = 0, try;
    iter1 (15);    iter1 (14);    iter1 (13);    iter1 (12);
    iter1 (11);    iter1 (10);    iter1 ( 9);    iter1 ( 8);
    iter1 ( 7);    iter1 ( 6);    iter1 ( 5);    iter1 ( 4);
    iter1 ( 3);    iter1 ( 2);    iter1 ( 1);    iter1 ( 0);
    return root >> 1;
}

static gint
point_dist (Point *a,
            Point *b)
{
  return isqrt ((a->x-b->x)*(a->x-b->x) +
                (a->y-b->y)*(a->y-b->y));
}


/* evaluate a point on a bezier-curve.
 * t goes from 0 to 65536
 *
 * curve contains four control points
 */
static void
bezier (Point **curve,
        Point *dest,
        gint   t)
{
  Point ab,bc,cd,abbc,bccd;
 
  lerp (&ab, curve[0], curve[1], t);
  lerp (&bc, curve[1], curve[2], t);
  lerp (&cd, curve[2], curve[3], t);
  lerp (&abbc, &ab, &bc,t);
  lerp (&bccd, &bc, &cd,t);
  lerp (dest, &abbc, &bccd, t);
}


gint
path_last_x (Path   *path)
{
  if (path)
    return path->point.x;
  return 0;
}

gint
path_last_y (Path   *path)
{
  if (path)
    return path->point.y;
  return 0;
}

/* types:
 *   'u' : unitilitlalized state
 *   's' : initialized state (last pen coordinates)
 *   'm' : move_to
 *   'l' : line_to
 *   'c' : curve_to
 *   '.' : curve_to_
 *   'C' : curve_to_
 */

static Path *
path_add (Path *head,
          gchar type,
          gint  x,
          gint  y)
{
  Path *iter = head;
  Path *prev = NULL;

  Path *curve[4]={NULL, NULL, NULL, NULL};

  if (iter)
    {
      while (iter->next)
        {
          switch (iter->type)
            {
              case 'c':
                if (prev)
                  curve[0]=prev;
                curve[1]=iter;
                break;
              case '.':
                curve[2]=iter;
                break;
              case 'C':
                curve[3]=iter;
                break;
            }
          prev=iter;
          iter=iter->next;
        }
      /* XXX: code duplication from above*/
      switch (iter->type)
        {
          case 'c':
            if (prev)
              curve[0]=prev;
            curve[1]=iter;
            break;
          case '.':
            curve[2]=iter;
            break;
          case 'C':
            curve[3]=iter;
            break;
        }
    }
  if (iter)
    {
      iter->next = g_malloc0 (sizeof (Path));
      iter=iter->next;
    }
  else /* creating new path */
    { 
      head = g_malloc0 (sizeof (Head));
      ((Head*)head)->tool = NULL;
      head->type = 'u';
      if (type=='u')
        {
          iter=head;
        }
      else
        {
          head->next = g_malloc0 (sizeof (Path));
          iter=head->next;
        }
    }

  if (head->type=='u' &&
      type!='u' &&
      type!='m')
        {
          g_error ("move_to is the only legal initial element for a path");
          return NULL;
        }

  iter->type=type;

  switch (type)
    {
      case 'm':
        if (head->type=='u')
          head->type='s';

        iter->point.x = x;
        iter->point.y = y;
        break;
      case 'c':
        iter->point.x = x;
        iter->point.y = y;
        break;
      case '.':
        iter->point.x = x;
        iter->point.y = y;
        break;
      case 'C':
        curve[3]=iter;
        iter->point.x = x;
        iter->point.y = y;

        /* chop off unneeded elements */
        curve[0]->next = NULL;
        
        { /* create piecevize linear approximation of bezier curve */
           gint   i;
           Point  foo[4];
           Point *pts[4];

           for (i=0;i<4;i++)
             {
               pts[i]=&foo[i];
               pts[i]->x=curve[i]->point.x;
               pts[i]->y=curve[i]->point.y;
             }

           for (i=0;i<65536;i+=65536 / BEZIER_SEGMENTS)
             {
                Point iter;

                bezier (pts, &iter, i);
                
                head = path_add (head, 'l', iter.x, iter.y);
             }
        }

        /* free amputated stubs when they are no longer useful */
        g_free (curve[1]);
        g_free (curve[2]);
        g_free (curve[3]);
        break;
      case 'l':
        iter->point.x = x;
        iter->point.y = y;
        break;
      case 'u':
        break;
      default:
        g_error ("unknown path instruction '%c'", type);
        break;
    }

  head->point.x=x;
  head->point.y=y;
  return head;
}

Path *
path_new (void)
{
  return path_add (NULL, 'u', 0, 0);
}

Path *
path_destroy      (Path *path)
{
  Head *head = (Head*)path;

  if (head &&
      head->tool)
    {
      g_free (head->tool);
    }

  
  while (path)
    {
      Path *iter=path->next;
      g_free (path);
      path=iter;
    }
  return NULL;
}

Path *
path_move_to (Path *path,
              gint  x,
              gint  y)
{
  path = path_add (path, 'm', x, y);
  return path;
}

Path *
path_line_to (Path *path,
              gint  x,
              gint  y)
{
  path = path_add (path, 'l', x, y);
  return path;
}

Path *
path_curve_to (Path *path,
               gint  x1,
               gint  y1,
               gint  x2,
               gint  y2,
               gint  x3,
               gint  y3)
{
  path = path_add (path, 'c', x1, y1);
  path = path_add (path, '.', x2, y2);
  path = path_add (path, 'C', x3, y3);
  return path;
}

Path *
path_rel_move_to (Path *path,
                  gint  x,
                  gint  y)
{
  return path_move_to (path, path->point.x + x, path->point.y + y);
}

Path *
path_rel_line_to (Path *path,
                  gint  x,
                  gint  y)
{
  return path_line_to (path, path->point.x + x, path->point.y + y);
}

Path *
path_rel_curve_to (Path *path,
                   gint  x1,
                   gint  y1,
                   gint  x2,
                   gint  y2,
                   gint  x3,
                   gint  y3)
{
  return path_curve_to (path,
                        path->point.x + x1, path->point.y + y1,
                        path->point.x + x2, path->point.y + y2,
                        path->point.x + x3, path->point.y + y3);
}


/* drawing code follows */

#include "canvas/canvas.h"
#include "brush.h"
#include "tools/tool_library.h"

/* code to stroke with a specified tool */

void
path_stroke_tool (Path   *path,
                  Canvas *canvas,
                  Tool   *tool)
{
  Path *iter = path;
  gint traveled_length = 0;
  gint need_to_travel = 0;
  gint x = 0,y = 0;

  if (!tool)
    tool = lookup_tool ("Paint");

  gboolean had_move_to = FALSE;

  while (iter)
    {
      //fprintf (stderr, "%c, %i %i\n", iter->type, iter->point.x, iter->point.y);
      switch (iter->type)
        {
          case 'm':
            x = iter->point.x;
            y = iter->point.y;
            need_to_travel = 0;
            traveled_length = 0;
            had_move_to = TRUE;
            break;
          case 'l':
            {
              Point a,b;
              ToolSetting *ts = tool_with_brush_get_toolsetting (tool);

              gint spacing;
              gint local_pos;
              gint distance;
              gint offset;
              gint leftover;
              

              a.x = x;
              a.y = y;

              b.x = iter->point.x; 
              b.y = iter->point.y;

              spacing = ts->spacing * (ts->radius * MAX_RADIUS / 65536) / 65536;

              if (spacing < MIN_SPACING * SPP / 65536)
                spacing = MIN_SPACING * SPP / 65536;

              distance = point_dist (&a, &b);

              leftover = need_to_travel - traveled_length;
              offset = spacing - leftover;

              local_pos = offset;

              if (distance > 0)
                for (;
                     local_pos <= distance;
                     local_pos += spacing)
                  {
                    Point spot;
                    gint ratio = 65536 * local_pos / distance;
                    gint radius;

                    lerp (&spot, &a, &b, ratio);
                    
                    radius = ts->radius  * MAX_RADIUS  / 65536,

                    canvas_stamp (canvas,
                      spot.x, spot.y, radius,
                      ts->opacity * MAX_OPACITY / 65536,
                      ts->hardness,
                      ts->red, ts->green, ts->blue);

                    traveled_length += spacing;
                  }

              need_to_travel += distance;

              x = b.x;
              y = b.y;
            }

            break;
          case 'u':
            g_error ("stroking uninitialized path\n");
            break;
          case 's':
            break;
          default:
            g_error ("can't stroke for instruction: %i\n", iter->type);
            break;
        }
      iter=iter->next;
    }
}

extern Tool *tool_paint_new ();

static Tool *path_tool (Path *path)
{
  Head *head = (Head*)path;
  if (!head)
    return NULL;
  if (head->tool == NULL)
    head->tool = tool_paint_new ();
  return head->tool;
}

void
path_set_line_width (Path *path,
                     gint  line_width)
{
  ToolSetting *ts = tool_with_brush_get_toolsetting (path_tool (path));
  ts->radius = line_width / 2;
}

void
path_set_rgba (Path *path,
               gint  red,
               gint  green,
               gint  blue,
               gint  alpha)
{
  ToolSetting *ts = tool_with_brush_get_toolsetting (path_tool (path));
  ts->red   = red;
  ts->green = green;
  ts->blue  = blue;
  ts->opacity = alpha;
}

void
path_set_hardness (Path *path,
                   gint  hardness)
{
  ToolSetting *ts = tool_with_brush_get_toolsetting (path_tool (path));
  ts->hardness = hardness;
}

void
path_stroke (Path   *path,
             Canvas *canvas)
{
  path_stroke_tool (path, canvas, path_tool (path));
}
