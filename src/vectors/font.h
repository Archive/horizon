/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#ifndef _FONT_H
#define _FONT_H

#include "path.h"

typedef struct _HorizonFont HorizonFont;

/* create a reference to a new font font 
 */
HorizonFont * font_new      (void);

/* destroy a font font
 */
void          font_destroy  (HorizonFont *font);

/* Change the height in pixels at 1:1 of the font.
 */
HorizonFont * font_set_size (HorizonFont *font,
                             gint size);

gint          font_get_size (HorizonFont *font);
    
/* appends a path of the given ascii text.
 *
 * font  : the font characteristics to use.
 * path  : the path to append to (or NULL for new path)
 * x,y   : positioning of baseline for first characther
 * width : the right margin. 0 means no wrapping 
 * ascii : a text string that may contain newlines.
 *
 */
Path *        font_path     (HorizonFont *font,
                             Path        *path,
                             gint         x,
                             gint         y,
                             gint         width,
                             const gchar *ascii);

#endif
