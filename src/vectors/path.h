/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#ifndef _PATH_H
#define _PATH_H

#include <glib.h>
#include "canvas/canvas.h"
#include "tools/tool.h"

typedef struct _Path Path;


Path *
path_new          (void);

/* destroys a path object (returns NULL)
 */
Path *
path_destroy      (Path *path);


/* stroke the path with the provided tool
 */
void
path_stroke_tool  (Path   *path,
                   Canvas *canvas,
                   Tool   *tool);

/* retrieves the last coordinate added to a path
 */
gint
path_last_x       (Path   *path);

gint
path_last_y       (Path   *path);

/* Path construction has the same semantics as cairo.
 * All methods returning a path returns the resulting
 * current path reference.
 */

Path *
path_move_to      (Path *path,
                   gint  x,
                   gint  y);

Path *
path_line_to      (Path *path,
                   gint  x,
                   gint  y);

Path *
path_curve_to     (Path *path,
                   gint  x1,
                   gint  y1,
                   gint  x2,
                   gint  y2,
                   gint  x3,
                   gint  y3);

Path *
path_rel_move_to  (Path *path,
                   gint  x,
                   gint  y);

Path *
path_rel_line_to  (Path *path,
                   gint  x,
                   gint  y);

Path *
path_rel_curve_to (Path *path,
                   gint  x1,
                   gint  y1,
                   gint  x2,
                   gint  y2,
                   gint  x3,
                   gint  y3);

void path_set_line_width (Path *path,
                          gint  line_width);

void path_set_rgba (Path *path,
                    gint  red,
                    gint  green,
                    gint  blue,
                    gint  alpha);

void path_set_hardness (Path *path,
                        gint  hardness);

void path_stroke (Path   *path,
                  Canvas *canvas);

#endif
