/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2004, 2006 Keith Packard <keithp@keithp.com>
                             Øyvind Kolås <pippin@gimp.org>,

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

/* glue for twin_glyph, copied from keithp's twin system */


extern const signed char _twin_gtable[];
extern const unsigned short _twin_g_offsets[];

#define twin_glyph_left(g)      ((g)[0])
#define twin_glyph_right(g)     ((g)[1])
#define twin_glyph_ascent(g)    ((g)[2])
#define twin_glyph_descent(g)   ((g)[3])
#define twin_glyph_n_snap_x(g)  ((g)[4])
#define twin_glyph_n_snap_y(g)  ((g)[5])
#define twin_glyph_snap_x(g)    (&g[6])
#define twin_glyph_snap_y(g)    (twin_glyph_snap_x(g) + twin_glyph_n_snap_x(g))
#define twin_glyph_draw(g)      (twin_glyph_snap_y(g) + twin_glyph_n_snap_y(g))

static const signed char *
_twin_g_base (char ascii)
{
  return _twin_gtable + _twin_g_offsets[(int)ascii];
}

/* end of glue */


#include "path.h"

typedef struct Point
{
  gint x;
  gint y;
} Point;

#include "font.h"
#include "config.h"

struct _HorizonFont
{
  gint size;
  gint line_spacing;
  gint word_spacing;
};

static gint
block_width (HorizonFont *font,
             gchar        ascii)
{
  const signed char *b = _twin_g_base (ascii);
  return (twin_glyph_right (b) + 10) * SPP * font->size/42;
}

static Path *
font_ascii_path (HorizonFont *font,
                 Path        *path,
                 gint         x,
                 gint         y,
                 gchar        ascii)
{
  const signed char *b = _twin_g_base (ascii);
  const signed char *g = twin_glyph_draw(b);

  Point origin;
  gint  x1, y1, x2, y2, x3, y3;
  gint  next_x;
  gint  baseline;

  origin.x = path_last_x (path);
  origin.y = path_last_y (path);
  next_x = origin.x + block_width (font, ascii);
  baseline = origin.y;

  #define SX(xx)  (origin.x + (xx) * SPP * font->size/42 )
  #define SY(yy)  (origin.y + (yy) * SPP * font->size/42 )
  
  for (;;)
    {
      switch (*g++) {
      case 'm':
          x1 = SX(*g++);
          y1 = SY(*g++);
          /* will create a path if the passed in path is NULL */
          path = path_move_to (path, x1, y1);
          continue;
      case 'l':
          x1 = SX(*g++);
          y1 = SY(*g++);
          path = path_line_to (path, x1, y1);
          continue;
      case 'c':
          x1 = SX(*g++);
          y1 = SY(*g++);
          x2 = SX(*g++);
          y2 = SY(*g++);
          x3 = SX(*g++);
          y3 = SY(*g++);
          path = path_curve_to (path, x1, y1, x2, y2, x3, y3);
          continue;
      case 'e':
          break;
      }
      break;
   }

   path = path_move_to (path, next_x, baseline);
   return path;
}

static gint
wordlength (HorizonFont *font,
            const gchar *ascii)
{
  gint sum = 0;
  while (*ascii &&
         *ascii != ' ' &&
         *ascii != '\n')
    {
      sum += block_width (font, *ascii);
      ascii++;
    }
  return sum;
}

#if 0
Path *
font_path (HorizonFont *font,
           Path        *path,
           gint         x,
           gint         y,
           gint         width,
           const gchar *ascii)
{
  path = path_move_to (path, x, y);

  while (*ascii)
    {
      if (*ascii == '\n')
        {
          path = path_move_to (path, x, y += font->line_spacing);
        }
      else if (*ascii == ' ')
        {
           if (width && 
               path_last_x (path) + wordlength (font, ascii+1) > x + width &&
               wordlength (font, ascii+1) < width)
             {
               path = path_move_to (path, x, y += font->line_spacing);
             }
           else
             {
               path = path_move_to (path, path_last_x (path) + font->word_spacing, y);
             }
        }
      else
        {
          path = font_ascii_path (font, path, x,y, *ascii);
        }
      ascii++;
    }
  return path;
}
#endif

Path *
font_path (HorizonFont *font,
           Path        *path,
           gint         x,
           gint         y,
           gint         width,
           const gchar *ascii)
{
  path = path_move_to (path, x, y);

  while (*ascii)
    {
      if (*ascii == '\n')
        {
          path = path_move_to (path, x, y += font->line_spacing * 1.5);
        }
      else if (*ascii == ' ')
        {
           if (width && 
               path_last_x (path) + wordlength (font, ascii+1) > x + width &&
               wordlength (font, ascii+1) < width)
             {
               path = path_move_to (path, x, y += font->line_spacing);
             }
           else
             {
               path = path_move_to (path, path_last_x (path) + font->word_spacing, y);
             }
        }
      else
        {
          path = font_ascii_path (font, path, x,y, *ascii);
        }
      ascii++;
    }
  return path;
}

HorizonFont * font_new (void)
{
  HorizonFont *font = g_malloc (sizeof (HorizonFont));

  font_set_size (font, 42);

  return font;
}

void
font_destroy (HorizonFont *font)
{
  if (font)
    g_free (font);
}

HorizonFont * font_set_size (HorizonFont *font,
                            gint     size)
{
  font->size = size;
  font->line_spacing = 1.5 * SPP * 42 * size/42;
  font->word_spacing = 0.5 * SPP * 42 * size/42;
  return font;
}

gint   font_get_size (HorizonFont *font)
{
  return font->size;
}
