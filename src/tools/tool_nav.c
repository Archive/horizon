/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "tool_nav.h"
#include "tool.h"
#include "brush.h"
#include "toolkit/slider.h"
#include "toolkit/navigator.h"
#include "toolkit/view.h"
#include "canvas/canvas.h"
#include "teleport.h"
#include "actions.h"
#include "link.h"

#include "config.h"


struct _ToolNav {
  Tool         tool;
  Slider      *zoom_slider;
  View        *thumbnail;

  gboolean     teleport;
  gint         teleport_x;
  gint         teleport_y;
  gint         teleport_time; /* used to store the time after a successful
                                 teleport, to compress teleport requests
                                 */

  /* this is a hack */
  void     (*stolen_slider_render) (View     *view,
                                    gpointer  data);
};


static void
tool_nav_start (Tool  *tool,
                gint    x,
                gint    y,
                gint    pressure,
                gulong  time)
{
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;
  Link *link;

  x /= SPP;
  y /= SPP;
 
  link = links_get (x, y);


  if (link)
    {
      gint scale_w; 
      gint scale_h;
      gint dst_x, dst_y, dst_width, dst_height;
      link_get_dst (link, &dst_x, &dst_y, &dst_width, &dst_height);

      scale_w = 65536 * (box_get_width ((Box*)view)) / dst_width;
      scale_h = 65536 * (box_get_height ((Box*)view)) / dst_height;

      if (scale_w < scale_h)
        {
        /*
          view_teleport (view, dst_x + dst_width/2, dst_y + dst_height /2, scale_w, 1000);
          */
          view_set_x (view, dst_x);
          view_set_y (view, dst_y);
          view_set_scale (view, scale_w);
        }
      else
        {
        /*
          view_teleport (view, dst_x + dst_width/2, dst_y + dst_height /2, scale_h, 1000);
          */
          view_set_x (view, dst_x);
          view_set_y (view, dst_y);
          view_set_scale (view, scale_h);
        }
      tool->prev_time = 0;
      return;
    }
  
  tool->prev_x = x;  /* prev is used as the /grabbed coordinate/ in pan tool */
  tool->prev_y = y;
  tool->prev_time = time;
}

static void
tool_nav_continue (Tool  *tool,
                   gint   x,
                   gint   y,
                   gint   pressure,
                   gulong time)
{
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;
  x /= SPP;
  y /= SPP;

  view_set_x (view, view_get_x (view) - (x-tool->prev_x));
  view_set_y (view, view_get_y (view) - (y-tool->prev_y));

  tool->prev_pressure=pressure;
  tool->prev_time=time;
}

void
tool_nav_process_coordinate (Tool   *tool,
                             gint    x,
                             gint    y,
                             gint    pressure,
                             gulong  time)
{
  if (time - tool->prev_time > 200)
    {
      tool_nav_start (tool, x, y, pressure, time);
    }
  else
    {
      tool_nav_continue (tool, x, y, pressure, time);
    }
}

int
tool_nav_flush (Tool *tool)
{/*
  View *view = tool->view;
  ToolNav *tool_nav = (ToolNav*)tool;
*/
  return 0;
}

#define UI_ZOOM_GAMMA 1.5

void
tool_nav_zoom_slider (Slider   *slider,
		      gpointer  data)
{
  Tool  *tool  = data;
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;
  gfloat scale;

  scale = 65536 - slider_get_value (slider);
  scale = pow (scale / 65536.0, UI_ZOOM_GAMMA) * ZOOM_MAX;
  
  /* snap zoom value */
  if ((scale > 0.25 * (65536.0 - 65536 * 0.1)) &&
      (scale < 0.25 * (65536.0 + 65536 * 0.1)))
    scale = 0.25 * 65536;
  if ((scale > 0.5 * (65536.0 - 65536 * 0.1)) &&
      (scale < 0.5 * (65536.0 + 65536 * 0.1)))
    scale = 0.5 * 65536;
  if ((scale > (65536.0 - 65536 * 0.1)) &&
     (scale < (65536.0 + 65536 * 0.1)))
    scale = 65536;
  if ((scale > 2 * (65536.0 - 65536 * 0.1)) &&
      (scale < 2 * (65536.0 + 65536 * 0.1)))
    scale = 2 * 65536;

  if (scale < ZOOM_MIN)
    scale = ZOOM_MIN;
  
  view_set_center_scale (view, scale);

  /* the sliders value is update again in update_zoom slider */
}

static void
update_zoom_slider (View *view,
                    gpointer data)
{
  Tool    *tool     = (Tool*) data;
  ToolNav *tool_nav = (ToolNav*)tool;
  gint     scale    = view_get_scale ((View*)tool->easel);
  
  slider_set_value ((Slider *)view, 65536 - pow (scale/ZOOM_MAX, (1.0/UI_ZOOM_GAMMA)) * 65536);
  tool_nav->stolen_slider_render (view, NULL);
}

void tool_nav_fill_tool_specific_settings_box (Tool *tool)
{
  ToolNav *tool_nav = (ToolNav*) tool;
  Box *vbox;
  
  vbox = vbox_new (2);
  box_set_ratio (vbox, 0.92 * 65536);

  tool->tool_settings_box = vbox;
  /* Create zoom slider. */
  tool_nav->zoom_slider = slider_new (tool->canvas);

  slider_set_value_changed_function (tool_nav->zoom_slider, tool_nav_zoom_slider, tool);
  
  box_set_child (vbox, 1, (Box*)tool_nav->zoom_slider);

  /* ugly hack to force updates of slider */
  tool_nav->stolen_slider_render = ((View*)(tool_nav->zoom_slider))->render;
  ((View*)(tool_nav->zoom_slider))->render = update_zoom_slider;
  ((View*)(tool_nav->zoom_slider))->render_data = tool;
  
  /* Create thumbnail view. */
  tool_nav->thumbnail = (View*)navigator_new (tool->canvas);
  navigator_set_easel ((Navigator*)tool_nav->thumbnail, tool->easel);
  box_set_child (vbox, 0, (Box*)tool_nav->thumbnail);

  tool_nav_flush (tool);
}

static void change_mode (Tool *tool)
{
  horizon_show_prefs (tool->horizon);
}

Tool *
tool_nav_new ()
{
  ToolNav *tool_nav = g_malloc0 (sizeof (ToolNav));
  Tool *tool = (Tool*) tool_nav;
  
  tool_init (tool);
  
  tool->process_coordinate = tool_nav_process_coordinate;
  tool->flush = tool_nav_flush;
  tool->tool_fill_tool_specific_settings_box = tool_nav_fill_tool_specific_settings_box;
  tool->short_name = "Nav";
  tool->change_mode = change_mode;
 
  tool_nav->teleport = FALSE; 
  tool_nav->teleport_time = 0;

  return tool;
}

void
tool_nav_destroy (Tool *tool)
{
  g_free ((ToolNav *)tool);
}
