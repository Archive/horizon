/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "brush.h"
#include "tool.h"
#include "tool_library.h"
#include "toolkit/easel.h"
#include "toolkit/view.h"
#include "toolkit/slider.h"
#include "toolkit/widget.h"
#include "vectors/path.h"

#include "config.h"

void
tool_set_horizon (Tool    *tool,
                  Horizon *horizon)
{
  tool->horizon = horizon;
}


void 
tool_set_easel (Tool   *tool,
                Easel  *easel)
{
  tool->easel = easel;
  tool->canvas = view_get_canvas ((View*)easel);
}

void
tool_set_toolbox_label (Tool  *tool,
                        Label *label)
{
  tool->toolbox_label = label;
}

Label *
tool_get_toolbox_label (Tool  *tool)
{
  return tool->toolbox_label;
}

static void tool_selected (Tool *tool)
{
}

static void tool_deselected (Tool *tool)
{
}

void
tool_init (Tool *tool)
{
  g_assert (tool != NULL);
  
  tool->process_coordinate = NULL;
  tool->flush = NULL;
  tool->tool_fill_tool_specific_settings_box = NULL;
  tool->select = tool_selected;
  tool->deselect = tool_deselected;
  tool->is_selected = FALSE;
  tool->tool_icons_created = FALSE;
  tool->uses_brush = FALSE;
  tool->short_name = "";
}
   
Tool *
tool_new (void)
{
  Tool *tool = g_malloc0 (sizeof (Tool));
  
  tool_init (tool);

  return tool;
}

void
tool_destroy (Tool *tool)
{
  g_free (tool);
}

void
tool_process_coordinate (Tool   *tool,
                         gint    x,
                         gint    y,
                         gint    pressure,
                         gulong  time)
{
  if (tool != NULL)
    {
      if (tool->process_coordinate != NULL)
        tool->process_coordinate (tool, x, y, pressure, time);
    }
}

int
tool_flush (Tool *tool)
{
  if (tool != NULL)
    {
      if (tool->flush != NULL)
        return tool->flush (tool);
    }
  
  return 0;
}

void tool_reset (Tool *tool)
{
  tool->prev_time = 0;
}



void tool_select (Tool *tool, Canvas *canvas)
{
  
  tool->is_selected = TRUE;

  if (tool->select)
    tool->select (tool);
}

void tool_deselect (Tool *tool, Canvas *canvas)
{
  
  tool->is_selected = FALSE;


  if (tool->deselect)
    tool->deselect (tool);
}

Box *tool_get_settings_box (Tool *tool)
{
  if (tool->tool_settings_box == NULL)
    {
      if (tool->tool_fill_tool_specific_settings_box != NULL)
        tool->tool_fill_tool_specific_settings_box (tool);
    }
  return tool->tool_settings_box;
}
