/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Eero Tanskanen <yendor@nic.fi>
                             Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include "tool_library.h"
#include "tool.h"
#include "tool_nav.h"
#include "tool_paint.h"
#include "tool_erase.h"
#include "tool_clone.h"
#include "tool_file.h"
#include "tool_link.h"
#include "toolkit/label.h"
#include "config.h"
#include "string.h"

static Tool   *current_tool = NULL;
static GSList *tools = NULL;

static Horizon *horizon = NULL;

/* FIME: nasty hack to make it compile during deglobalization */
void
tool_library_set_horizon (Horizon *new_horizon)
{
  horizon = new_horizon;
}

static void
tool_select_tool_handler (Label *label,
                          gpointer tool_ptr);

void
tool_library_create_tool_uis (Horizon *horizon)
{
  GSList *iter = tools;
  while (iter)
    {
      Tool *tool = iter->data;
      tool_set_horizon (tool, horizon);
      tool_set_easel (tool, horizon->easel);
      iter=iter->next;
    }
}

Box *tool_library_get_tool_box (Canvas *canvas)
{
  Box    *grid;
  GSList *iter = tools;
  gint    tool_count = g_slist_length (tools);
  gint    grid_pos = 0;
  
  grid = grid_new (2, (tool_count/ 2) + (tool_count % 2));
  while (iter)
    {
      Tool  *tool  = iter->data;
      Label *label = label_new (canvas);

      label_set_event (label, tool_select_tool_handler, tool);
      label_set_label (label, tool->short_name);

      box_set_child (grid, grid_pos++, (Box*)label);
      tool_set_toolbox_label (tool, label);

      iter=iter->next;
    }
  return grid;
}

void set_current_tool (Tool *new_tool)
{
  if (current_tool == new_tool)
    return;
  /* Unselect old tool */
  if (current_tool != NULL)
    tool_deselect (current_tool, NULL);
  
  current_tool = new_tool;
  
  tool_select (new_tool, NULL);
  tool_reset (new_tool);
}

Tool *get_current_tool (void)
{
  return current_tool;
}

Tool *lookup_tool (const gchar *name)
{
  GSList *iter = tools;
  while (iter)
    {
      Tool *tool = iter->data;
      if (!strcmp (tool->short_name, name))
        return tool;
      iter=iter->next;
    }
  g_warning ("couldn't find tool %s", name);
  return NULL;
}

void create_tools (void)
{
  tools = g_slist_append (tools, tool_paint_new ());
  tools = g_slist_append (tools, tool_erase_new ());
  tools = g_slist_append (tools, tool_nav_new ());
  tools = g_slist_append (tools, tool_clone_new ());
  tools = g_slist_append (tools, tool_link_new ());
  tools = g_slist_append (tools, tool_file_new ());
}

void tools_destroy (void)
{
#if 0
  tool_paint_destroy (tools[TOOL_PAINT]);
  tool_nav_destroy (tools[TOOL_NAV]);
  tool_erase_destroy (tools[TOOL_ERASE]);
  tool_clone_destroy (tools[TOOL_CLONE]);
  tool_file_destroy (tools[TOOL_EXPORT]);
  tool_link_destroy (tools[TOOL_LINK]);
#endif
}

void tool_library_set_tool (Tool *tool)
{
  set_current_tool (tool);
  
  /* If the needed tool settings box is not already 
    the displayed tool settings box replace the current
    settings box. */

  if (horizon->root != NULL)
    {
      Box *tool_options = box_id (horizon->root, "tool_options");

      if (box_get_child (tool_options, 0) != tool_get_settings_box (tool))
        box_set_child (tool_options, 0, tool_get_settings_box (tool));
      tool->tool_icons_created = TRUE;
    }

  {
    GSList *iter = tools;
    while (iter)
      {
        Tool *tool_i = iter->data;

        Label *label = tool_get_toolbox_label (tool_i);
        if (label)
          label_set_active (label, tool_i == tool);

        iter=iter->next;
      }
  }
}

static void
tool_select_tool_handler (Label    *label,
                          gpointer  tool_ptr)
{
  Tool *tool = tool_ptr;
  tool_library_set_tool (tool);
}
