/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "canvas/canvas.h"
#include "brush.h"
#include "tool.h"
#include "tool_paint.h"
#include "toolkit/navigator.h"
#include "config.h"

void tool_erase_fill_tool_specific_settings_box (Tool *tool)
{
  ToolSetting *ts;
  Box *vbox;
  ToolPaint *toolpaint = (ToolPaint*) toolpaint;
  Canvas *canvas = tool->canvas;

  if (tool->uses_brush == FALSE)
    return;

  ts = &((ToolWithBrush *) tool)->ts;
  ts->canvas = canvas;
 
  { 
    vbox = vbox_new (3);
    gint ratios[]={3,4,7};
    box_set_ratios (vbox, ratios);
  }
  tool->tool_settings_box = vbox;

  box_set_child (vbox, 0, tool_setting_get_brush_sliders (ts, canvas));
  box_set_child (vbox, 1, tool_setting_get_brush_preview (ts, canvas));

  {
    Navigator *navigator = navigator_new (canvas);
    navigator_set_easel (navigator, tool->easel);
    box_set_child (vbox, 2, (Box*)navigator);
  }

}

Tool *
tool_erase_new ()
{
  Tool *tool = tool_paint_new ();
  ToolSetting *ts = &((ToolWithBrush*)tool)->ts;
  
  tool->tool_fill_tool_specific_settings_box = tool_erase_fill_tool_specific_settings_box;
  
  /* overriding mode change for eraser tool, since it doesn't
   * have a color picker mode
   */
  tool->change_mode = NULL;
  
  ts->red    = 65536 * 1.0;
  ts->green  = 65536 * 1.0;
  ts->blue   = 65536 * 1.0;
  ts->radius = 65536 * 0.3;

  tool->short_name = g_strdup ("Erase");
  
  return tool;
}
  
void
tool_erase_destroy (Tool *tool)
{
  g_free ((ToolPaint *)tool);
}
