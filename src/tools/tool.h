/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#ifndef _TOOL_H
#define _TOOL_H

typedef struct _Tool Tool;

#include <glib.h>

#include "horizon.h"
#include "toolkit/view.h"
#include "toolkit/label.h"
#include "toolkit/slider.h"
#include "toolkit/easel.h"
#include "canvas/canvas.h"

/* Tool functions. */

struct _Tool {
  gint      number;
  gboolean  is_selected;
  gboolean  tool_icons_created;

  Horizon  *horizon;
  Easel    *easel;
  Canvas   *canvas;

  gint      prev_x;
  gint      prev_y;
  gint      prev_pressure;
  gulong    prev_time;
 
  Label    *toolbox_label; 
  Box      *tool_settings_box;
  
  gboolean  uses_brush;
  
  gchar    *short_name;
  
  void    (*process_coordinate) (Tool   *tool,
                                 gint    x,
                                 gint    y,
                                 gint    pressure,
                                 gulong  time);

  /* returns 1 if toolparams have changed (hack) */
  gint    (*flush) (Tool *tool);
  void    (*tool_fill_tool_specific_settings_box) (Tool *tool);
  void    (*change_mode) (Tool *tool);
  void    (*select)   (Tool *tool);
  void    (*deselect) (Tool *tool);
};

void
tool_init (Tool *tool);

Tool *
tool_new (void);

void
tool_set_horizon (Tool    *tool,
                  Horizon *horizon);

void 
tool_set_easel (Tool  *tool,
                Easel *easel);

void
tool_destroy (Tool *tool);

void
tool_process_coordinate (Tool   *tool,
                         gint    x,
                         gint    y,
                         gint    pressure,
                         gulong  time);

/* returns 1 if toolparams have changed (hack) */
int
tool_flush (Tool *tool);

void tool_reset (Tool *tool);

void tool_set_toolbox_label (Tool  *tool,
                             Label *label);


Label *tool_get_toolbox_label (Tool  *tool);

void tool_select (Tool *tool, Canvas *canvas);
void tool_deselect (Tool *tool, Canvas *canvas);
Box *tool_get_settings_box (Tool *tool);

#endif
