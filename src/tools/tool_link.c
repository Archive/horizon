/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "brush.h"
#include "tool_link.h"
#include "tool.h"
#include "canvas/canvas.h"
#include "vectors/path.h"
#include "toolkit/label.h"
#include "toolkit/navigator.h"
#include "tool_library.h"
#include "link.h"
#include "actions.h"

#include "config.h"

static const char *
state_label[] = {"nav",
                 "set target",
                 "position source",
                 "create",
                 "delete",
                 "null"};

static void tool_link_set_state (Tool          *tool,
	                         ToolLinkState  state);


static void
update_dst_rect (Tool *tool)
{
  ToolLink *tool_link = (ToolLink*)tool;
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;

  gint color[4]         = {0,     0,     0,     65536*0.5};
  gint border_color[4]  = {65535, 65535, 65536, 65535};
  gint surround_color[4]= {65535, 65535, 65536, 0};

  view_set_rect_x (view, tool_link->dst.x);
  view_set_rect_y (view, tool_link->dst.y);
  view_set_rect_width (view, tool_link->dst.width);
  view_set_rect_height (view, tool_link->dst.height);

  view_set_rect_color (view, color);
  view_set_rect_border_color (view, border_color);
  view_set_rect_surround_color (view, surround_color);
}


static void
update_src_rect (Tool *tool)
{
  ToolLink *tool_link = (ToolLink*)tool;
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;

  gint color[4]         = {0,     0,     0,     65536*0.5};
  gint border_color[4]  = {65535*0.0, 65535 * 0.0, 65536*1.0, 65535*0.75};
  gint surround_color[4]= {65535, 65535, 65536, 0};

  view_set_rect_x (view, tool_link->src.x);
  view_set_rect_y (view, tool_link->src.y);
  view_set_rect_width (view, tool_link->src.width);
  view_set_rect_height (view, tool_link->src.height);

  view_set_rect_color (view, color);
  view_set_rect_border_color (view, border_color);
  view_set_rect_surround_color (view, surround_color);
}


static void
tool_link_set_state_dst (Label    *label,
			 gpointer  data)
{
  Tool     *tool      = data;
  ToolLink *tool_link = data;
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;

  view_set_rect_visible (view, FALSE);

  tool_link->src.width=0;
  tool_link->src.height=0;
  tool_link->dst.width=0;
  tool_link->dst.height=0;

  tool_link_set_state (tool, TOOL_LINK_STATE_DST);
}

static void
tool_link_set_state_pan (Label    *label,
			 gpointer  data)
{
  Tool     *tool      = data;

  update_dst_rect (tool); 
  tool_link_set_state (tool, TOOL_LINK_STATE_PAN);
}

static void
tool_link_set_state_src (Label    *label,
			 gpointer  data)
{
  Tool     *tool      = data;
  ToolLink *tool_link = data;
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;

  if (tool_link->src.width == 0)
    {
      tool_link->src.width = box_get_width ((Box*)view) * 65536 / view_get_scale (view);
      tool_link->src.height = box_get_height ((Box*)view) * 65536 / view_get_scale (view);

      tool_link->src.width /= 2;
      tool_link->src.height /= 2;

      tool_link->src.x = view_get_x (view) + tool_link->src.width/2;
      tool_link->src.y = view_get_y (view) + tool_link->src.height/2;
    }

  update_src_rect (tool); 
  tool_link_set_state (tool, TOOL_LINK_STATE_SRC);
}

static void
tool_link_set_state_link (Label    *label,
			  gpointer  data)
{
  Tool     *tool = data;
  Easel    *easel = tool->easel;
  View     *view  = (View*)easel;
  ToolLink *tool_link = data;
  Link     *link;

  if (tool_link->src.width == 0)
    return;

  link = link_new ();
  link_set_src (link, tool_link->src.x, tool_link->src.y,
                      tool_link->src.width, tool_link->src.height);
  link_set_dst (link, tool_link->dst.x, tool_link->dst.y,
                      tool_link->dst.width, tool_link->dst.height);
  links_add (link);

  horizon_status (tool->horizon, "Creating link..");
  action (tool->horizon, "refresh");
  
  link_redraw (view_get_canvas (view), link);
  view_set_rect_visible (view, FALSE);
  tool_link_set_state_dst (NULL, tool);
  horizon_status (tool->horizon, "Created link.");
}

static void
tool_link_set_state_delete (Label    *label,
			    gpointer  data)
{
  Tool     *tool = data;
  ToolLink *tool_link = data;
  Easel    *easel = tool->easel;
  View     *view  = (View*)easel;

  if (tool_link->selected_link == NULL)
    return;

  links_delete (tool_link->selected_link);
  tool_link->selected_link = NULL;
  
  view_set_rect_visible (view, FALSE);
  tool_link_set_state_dst (NULL, tool);
}

static void
tool_link_start (Tool    *tool,
                  gint    x,
                  gint    y,
                  gint    pressure,
                  gulong  time)
{
  ToolLink *tool_link = (ToolLink *)tool;
  Easel    *easel = tool->easel;
  View     *view  = (View*)easel;
  
  x /= SPP;
  y /= SPP;

  switch (tool_link->state)
    {
      case TOOL_LINK_STATE_DST:
        tool_link->selected_link = links_get (x, y);
        if (tool_link->selected_link)
          {
            gint color[4]         = {0,     0,     0,     65536*0.5};
            gint border_color[4]  = {65535*0.0, 65535 * 0.0, 65536*1.0, 65535*0.75};
            gint surround_color[4]= {65535, 65535, 65536, 0};
            gint x,y,w,h;

            link_get_src (tool_link->selected_link, &x, &y, &w, &h);

            view_set_rect_x (view, x);
            view_set_rect_y (view, y);
            view_set_rect_width (view, w); 
            view_set_rect_height (view, h);

            view_set_rect_color (view, color);
            view_set_rect_border_color (view, border_color);
            view_set_rect_surround_color (view, surround_color);
          }
        break;
      case TOOL_LINK_STATE_PAN:
        break;
      case TOOL_LINK_STATE_SRC:
        {
          gint rel_x = (x - tool_link->src.x) * 65536 / (tool_link->src.width);
          gint rel_y = (y - tool_link->src.y) * 65536 / (tool_link->src.height);

          if (rel_x < 0 ||
              rel_y < 0 ||
              rel_x > 65536 ||
              rel_y > 65536)
            { /* outside rectangle */
              tool_link->src_mode = TOOL_LINK_SRC_MODE_PAN;
            }
          else
            { /* inside rectangle */
              if (rel_x > 65536 / 3 &&
                  rel_y > 65536 / 3 &&
                  rel_x < 65536 * 2 / 3 &&
                  rel_y < 65536 * 2 / 3)
                { /* in middle of rectangle */
                  tool_link->src_mode = TOOL_LINK_SRC_MODE_MOVE;
                }
              else
                {
                  if (rel_x < 65536 / 3)
                    {
                      tool_link->src_mode = TOOL_LINK_SRC_MODE_LEFT;
                    }
                  else if (rel_x > 65536 * 2 / 3)
                    {
                      tool_link->src_mode = TOOL_LINK_SRC_MODE_RIGHT;
                    }
                  else
                    {
                      tool_link->src_mode = TOOL_LINK_SRC_MODE_MOVE; /* overriden by y coord modes */
                    }

                  if (rel_y < 65536 / 3)
                    {
                      if (tool_link->src_mode == TOOL_LINK_SRC_MODE_LEFT)
                        tool_link->src_mode = TOOL_LINK_SRC_MODE_TOP_LEFT;
                      else if (tool_link->src_mode == TOOL_LINK_SRC_MODE_RIGHT)
                        tool_link->src_mode = TOOL_LINK_SRC_MODE_TOP_RIGHT;
                      else
                        tool_link->src_mode = TOOL_LINK_SRC_MODE_TOP;
                    }
                  else if (rel_y > 65536 * 2 / 3)
                    {
                      if (tool_link->src_mode == TOOL_LINK_SRC_MODE_LEFT)
                        tool_link->src_mode = TOOL_LINK_SRC_MODE_BOTTOM_LEFT;
                      else if (tool_link->src_mode == TOOL_LINK_SRC_MODE_RIGHT)
                        tool_link->src_mode = TOOL_LINK_SRC_MODE_BOTTOM_RIGHT;
                      else
                        tool_link->src_mode = TOOL_LINK_SRC_MODE_BOTTOM;
                    }
                }
            }
        }
        break;
      case TOOL_LINK_STATE_LINK:
        break;
      case TOOL_LINK_STATE_DELETE:
        break;
      case TOOL_LINK_STATES:
        break;
    } 

  tool->prev_x = x;
  tool->prev_y = y;
  tool->prev_time = time;
}

static void
tool_link_continue (Tool   *tool,
                    gint    x,
                    gint    y,
                    gint    pressure,
                    gulong  time)
{
  ToolLink *tool_link = (ToolLink *)tool;
  Easel *easel = tool->easel;
  View *view   = (View*)easel;

  x /= SPP;
  y /= SPP;

  switch (tool_link->state)
    {
      case TOOL_LINK_STATE_DST:
      case TOOL_LINK_STATE_PAN:
        view_set_x (view, view_get_x (view) - (x-tool->prev_x));
        view_set_y (view, view_get_y (view) - (y-tool->prev_y));
        break;
      case TOOL_LINK_STATE_SRC:
        {
          gint ratio = tool_link->dst.width * 65536 / tool_link->dst.height;
          gint center_x = tool_link->src.x + tool_link->src.width/2;
          gint center_y = tool_link->src.y + tool_link->src.height/2;

          switch (tool_link->src_mode)
            {
              case TOOL_LINK_SRC_MODE_PAN:
                view_set_x (view, view_get_x (view) - (x-tool->prev_x));
                view_set_y (view, view_get_y (view) - (y-tool->prev_y));
                break;
              case TOOL_LINK_SRC_MODE_MOVE:
                tool_link->src.x += (x-tool->prev_x);
                tool_link->src.y += (y-tool->prev_y);
                break;
              case TOOL_LINK_SRC_MODE_LEFT:
                tool_link->src.width += (tool->prev_x - x) * 2;
                tool_link->src.height = tool_link->src.width * 65536 / ratio;
                break;
              case TOOL_LINK_SRC_MODE_RIGHT:
                tool_link->src.width += (x-tool->prev_x) * 2;
                tool_link->src.height = tool_link->src.width * 65536 / ratio;
                break;
              case TOOL_LINK_SRC_MODE_TOP:
                tool_link->src.height += (tool->prev_y - y) * 2;
                tool_link->src.width = tool_link->src.height * ratio / 65536;
                break;
              case TOOL_LINK_SRC_MODE_BOTTOM:
                tool_link->src.height += (y-tool->prev_y) * 2;
                tool_link->src.width = tool_link->src.height * ratio / 65536;
                break;
              case TOOL_LINK_SRC_MODE_TOP_LEFT:
                tool_link->src.height += (tool->prev_y - y) * 2;
                tool_link->src.width = tool_link->src.height * ratio / 65536;
                tool_link->src.width += (tool->prev_x - x) * 2;
                tool_link->src.height = tool_link->src.width * 65536 / ratio;
                break;
              case TOOL_LINK_SRC_MODE_TOP_RIGHT:
                tool_link->src.height += (tool->prev_y - y) * 2;
                tool_link->src.width = tool_link->src.height * ratio / 65536;
                tool_link->src.width += (x-tool->prev_x) * 2;
                tool_link->src.height = tool_link->src.width * 65536 / ratio;
                break;
              case TOOL_LINK_SRC_MODE_BOTTOM_LEFT:
                tool_link->src.height += (y-tool->prev_y) * 2;
                tool_link->src.width = tool_link->src.height * ratio / 65536;
                tool_link->src.width += (tool->prev_x - x) * 2;
                tool_link->src.height = tool_link->src.width * 65536 / ratio;
                break;
              case TOOL_LINK_SRC_MODE_BOTTOM_RIGHT:
                tool_link->src.height += (y-tool->prev_y) * 2;
                tool_link->src.width = tool_link->src.height * ratio / 65536;
                tool_link->src.width += (x-tool->prev_x) * 2;
                tool_link->src.height = tool_link->src.width * 65536 / ratio;
                break;
            }

          switch (tool_link->src_mode)
            {
              case TOOL_LINK_SRC_MODE_PAN:
                break;
              case TOOL_LINK_SRC_MODE_TOP_LEFT:
              case TOOL_LINK_SRC_MODE_TOP:
              case TOOL_LINK_SRC_MODE_TOP_RIGHT:
              case TOOL_LINK_SRC_MODE_LEFT:
              case TOOL_LINK_SRC_MODE_RIGHT:
              case TOOL_LINK_SRC_MODE_BOTTOM:
              case TOOL_LINK_SRC_MODE_BOTTOM_LEFT:
              case TOOL_LINK_SRC_MODE_BOTTOM_RIGHT:
                tool_link->src.x = center_x - tool_link->src.width / 2;
                tool_link->src.y = center_y - tool_link->src.height/ 2;
              case TOOL_LINK_SRC_MODE_MOVE:
                tool->prev_x = x;
                tool->prev_y = y;
                break;
            }
        }
        break;
      case TOOL_LINK_STATE_LINK:
        break;
      case TOOL_LINK_STATE_DELETE:
        break;
      case TOOL_LINK_STATES:
        break;
    } 
  tool->prev_time = time;
}

int
tool_link_flush (Tool *tool)
{
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;

  ToolLink *tool_link = (ToolLink *)tool;
  switch (tool_link->state)
    {
      case TOOL_LINK_STATE_DST:
        tool_link->dst.x = view_get_x (view);
        tool_link->dst.y = view_get_y (view);
        tool_link->dst.width = box_get_width ((Box*)view) * 65536 / view_get_scale (view);
        tool_link->dst.height = box_get_height ((Box*)view) * 65536 / view_get_scale (view);
        break;
      case TOOL_LINK_STATE_PAN:
        break;
      case TOOL_LINK_STATE_SRC:
        update_src_rect (tool); 
        break;
      case TOOL_LINK_STATE_LINK:
        break;
      case TOOL_LINK_STATE_DELETE:
        break;
      case TOOL_LINK_STATES:
        break;
    } 
  return 1;
}

void
tool_link_set_button_states (Tool *tool)
{
  gint i;
  ToolLink *toollink = (ToolLink*) tool;

  for (i=0; i<TOOL_LINK_STATES; i++)
    {
      if (i==toollink->state)
        label_set_active ((Label*)toollink->button[i], TRUE);
      else
        label_set_active ((Label*)toollink->button[i], FALSE);
    }
}

void tool_link_fill_tool_specific_settings_box (Tool *tool)
{
  gint i;
  Box *vbox = vbox_new (2);
  Box *states_box = vbox_new (TOOL_LINK_STATES);
  ToolLink *toollink = (ToolLink*) tool;
  Canvas *canvas = tool->canvas;

  tool->tool_settings_box = vbox;
  
  box_set_child (vbox, 1, states_box);
  {
    Navigator *navigator = navigator_new (canvas);
    navigator_set_easel (navigator, tool->easel);
    box_set_child (vbox, 0, (Box*)navigator);
  }
  for (i=0; i<TOOL_LINK_STATES; i++)
    {
      Label *label = label_new(canvas);
      label_set_label (label, state_label [i]);
      toollink->button[i] = label;

      switch (i)
        {
          case TOOL_LINK_STATE_SRC:
            label_set_event (label, tool_link_set_state_src, tool);
            break;
          case TOOL_LINK_STATE_PAN:
            label_set_event (label, tool_link_set_state_pan, tool);
            break;
          case TOOL_LINK_STATE_DST:
            label_set_event (label, tool_link_set_state_dst, tool);
            break;
          case TOOL_LINK_STATE_LINK:
            label_set_event (label, tool_link_set_state_link, tool);
            break;
          case TOOL_LINK_STATE_DELETE:
            label_set_event (label, tool_link_set_state_delete, tool);
            break;
        }
      box_set_child (states_box, i, (Box*)label);
    }
  tool_link_set_button_states (tool);
}

void
tool_link_process_coordinate (Tool   *tool,
                              gint    x,
                              gint    y,
                              gint    pressure,
                              gulong  time)
{
  if (time - tool->prev_time > 300)
   {
     tool_link_start (tool, x, y, pressure, time);
   }
  tool_link_continue (tool, x, y, pressure, time);
}


void
tool_link_set_state (Tool          *tool,
                     ToolLinkState  newstate)
{
  ToolLink *toollink = (ToolLink *)tool;
  
  if (newstate == toollink->state)
    return;
  
  toollink->state = newstate;
  if (tool->tool_icons_created == TRUE)
    tool_link_set_button_states (tool);
}

static void
deselect (Tool *tool)
{
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;

  view_set_rect_visible (view, FALSE);
}

static void change_mode (Tool *tool)
{
  ToolLink *tool_link = (ToolLink*)tool;
  switch (tool_link->state)
    {
    case TOOL_LINK_STATE_DST:
      tool_link_set_state_pan (NULL, tool);
      break;
    case TOOL_LINK_STATE_PAN:
      tool_link_set_state_src (NULL, tool);
      break;
    case TOOL_LINK_STATE_SRC:
      tool_link_set_state_link (NULL, tool);
      break;
    default:
      g_warning ("strange state in link tool when changing mode");
      break;
    }
}

Tool *
tool_link_new ()
{
  Tool *toollink = g_malloc0 (sizeof (ToolLink));
  Tool *tool = (Tool*) toollink;
  
  tool_init (tool);
  
  tool->flush = tool_link_flush;
  tool->tool_fill_tool_specific_settings_box = tool_link_fill_tool_specific_settings_box;
  tool->process_coordinate = tool_link_process_coordinate;
  tool->uses_brush = FALSE;
  tool->short_name = "Link";
  tool->deselect = deselect;
  tool->change_mode = change_mode;
  
  tool_link_set_state (tool, TOOL_LINK_STATE_DST);

  return tool;
}

void
tool_link_destroy (Tool *tool)
{
  g_free ((ToolLink *)tool);
}
