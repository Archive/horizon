/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _TOOL_PAINT_H
#define _TOOL_PAINT_H

#include <glib.h>
#include "tool_with_brush.h"
#include "toolkit/label.h"

typedef struct _ToolPaint ToolPaint;

struct _ToolPaint {
  ToolWithBrush  toolwithbrush;

  gboolean       pick_mode;
  Label         *pick_state_toggle;
  
  gint           traveled_length;
  gint           need_to_travel;
};

Tool * tool_paint_new ();
void   tool_paint_destroy (Tool *tool);


#endif
