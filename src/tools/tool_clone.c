/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "canvas/canvas.h"
#include "vectors/path.h"
#include "brush.h"
#include "tool_clone.h"
#include "tool.h"
#include "tool_library.h"
#include "toolkit/navigator.h"
#include "toolkit/label.h"

#include "config.h"

struct _ToolClone {
  ToolWithBrush  toolwithbrush;
  gint           mode;
  gint           aim_x;
  gint           aim_y;
  gint           offset_x;
  gint           offset_y;

  gboolean       start_stroke;
  
  Label         *clone_button;
  Label         *aim_button;
};

void
tool_clone_set_mode (Tool *tool,
		     gint  newmode);
gint
tool_clone_get_mode (Tool *tool);

static void
tool_clone_start (Tool   *tool,
                  gint    x,
                  gint    y,
                  gint    pressure,
                  gulong  time)
{
  ToolSetting *ts;
  ToolClone *tool_clone = (ToolClone *)tool;
  
  if (tool->uses_brush == FALSE)
    return;
  
  ts = &((ToolWithBrush *) tool)->ts;
  
  if (tool_clone->mode == 0)
    {
      tool_clone->offset_x = tool_clone->aim_x - x / SPP;
      tool_clone->offset_y = tool_clone->aim_y - y / SPP;
      tool_clone_set_mode (tool, 1);
    }
   
  /* only presume it is a new undostep if there is more than
   * 0.5 second between strokes */
  if (time - tool->prev_time > 500)
    {
      undo_stack_new_step (canvas_get_undo_stack (tool->canvas));
    }

  tool->prev_x = x;
  tool->prev_y = y;
  tool->prev_pressure = pressure;
  tool->prev_time=time;

  tool_clone->start_stroke = TRUE;
}

static void
tool_clone_continue (Tool   *tool,
                     gint    x,
                     gint    y,
                     gint    pressure,
                     gulong  time)
{
  ToolClone   *tool_clone = (ToolClone *)tool;
  ToolSetting *ts;
  gint         dist;
  gint         radius;
  gint         opacity;
  gint         spacing;
  gint         hardness;
   
  if (tool->uses_brush == FALSE)
    return;
  ts = &((ToolWithBrush *) tool)->ts;
   
  opacity = ts->opacity * MAX_OPACITY / 65536;
  
  spacing = ts->spacing * (ts->radius * MAX_RADIUS / 65536) / 65536;
  hardness = ((ts->hardness / 65536.0) * ( ts->hardness / 65536.0) * 65536);

  if (spacing<1)
    spacing=1;

/*
          canvas_stamp (tool->canvas,
           x ,y,
           ts->radius * MAX_RADIUS / 65536, ts->opacity, ts->hardness,
          ts->red, ts->green, ts->blue);
          return;
*/
  {
    gint dx,dy;
    dx = x - tool->prev_x;
    dy = y - tool->prev_y;
    dist = sqrt (dx*dx+dy*dy);
  }

  if (tool_clone->start_stroke == TRUE)
    {
      tool_clone->start_stroke = FALSE;
      dist = spacing;
    }

  if (dist >= spacing)
    {
      gint i;
      gint dots = dist/spacing;
      gint tx=0,ty=0,tp=0;

      for (i=0;i<dots;++i)
        {
          tx = (i*tool->prev_x + (dots - i) * x)/dots;
          ty = (i*tool->prev_y + (dots - i) * y)/dots;
          tp = (i*tool->prev_pressure + (dots - i) * pressure)/dots;

         // ts->min_opacity + tp * (ts->max_opacity-ts->min_opacity) / 65536;
          //radius  = ts->min_radius  + tp * (ts->max_radius -ts->min_radius) / 65536;
          radius  = ts->radius * MAX_RADIUS / 65536 / SPP;
          canvas_clone_stamp (tool->canvas,
                tx, ty,
                radius, opacity, hardness,
                tool_clone->offset_x, tool_clone->offset_y);
        }
      tool->prev_x=x;
      tool->prev_y=y;
      tool->prev_pressure=tp;
      tool->prev_time=time;
    }
}

int
tool_clone_flush (Tool *tool)
{
  return 0;
}


static void
tool_clone_set_clone_mode (Label    *label,
			   gpointer  data)
{
  Tool *tool = (Tool*) data;
  if (tool_clone_get_mode (tool) != 2)
    tool_clone_set_mode (tool, 0);
}

static void
tool_clone_set_aim_mode (Label *label,
			 gpointer  data)
{
  Tool *tool = (Tool*) data;
  if (tool_clone_get_mode (tool) != 2)
    tool_clone_set_mode (tool, 3);
}

void
tool_clone_set_button_states (Tool *tool)
{
  ToolClone *tool_clone = (ToolClone*) tool;
  if (tool_clone_get_mode (tool) >= 2)
    {
      label_set_active (tool_clone->clone_button, FALSE);
      label_set_active (tool_clone->aim_button, TRUE);
    }
  else
    {
      label_set_active (tool_clone->clone_button, TRUE);
      label_set_active (tool_clone->aim_button, FALSE);
    }
}

void tool_clone_fill_tool_specific_settings_box (Tool *tool)
{
  ToolSetting *ts;
  Box *vbox;
  Box *states_box;
  ToolClone *tool_clone = (ToolClone*) tool;
  Canvas *canvas = tool->canvas;
  gint ratios[]={3,4,5,2};

  if (tool->uses_brush == FALSE)
    return;
  ts = &((ToolWithBrush *) tool)->ts;
  ts->canvas = canvas;
  
  tool->tool_settings_box = vbox = vbox_new (4);
  box_set_ratios (vbox, ratios);
  
  states_box = vbox_new (2);
 
  tool_clone->clone_button = label_new (canvas);
  label_set_label (tool_clone->clone_button, "Clone");
  tool_clone->aim_button = label_new (canvas);
  label_set_label (tool_clone->aim_button ,"Set source");

  box_set_child (states_box, 0, (Box*)tool_clone->clone_button); 
  box_set_child (states_box, 1, (Box*)tool_clone->aim_button); 
  
  label_set_event (tool_clone->clone_button, tool_clone_set_clone_mode, tool);
  label_set_event (tool_clone->aim_button, tool_clone_set_aim_mode, tool);
  
  tool_clone_set_button_states (tool);
  
  box_set_child (vbox, 0, tool_setting_get_brush_sliders (ts, canvas));
  box_set_child (vbox, 1, tool_setting_get_brush_preview (ts, canvas));

  {
    Navigator *navigator = navigator_new (canvas);
    navigator_set_easel (navigator, tool->easel);
    box_set_child (vbox, 2, (Box*)navigator);
  }
  box_set_child (vbox, 3, states_box);

}

void
tool_clone_process_coordinate (Tool   *tool,
                               gint    x,
                               gint    y,
                               gint    pressure,
                               gulong  time)
{
  /* remove subpixel precision */
  x = (x/SPP) * SPP;
  y = (y/SPP) * SPP;
  if (time - tool->prev_time > 100)
   {
     tool_clone_start (tool, x, y, pressure, time);
   }
  tool_clone_continue (tool, x, y, pressure, time);
}

void
tool_clone_aim_process_coordinate (Tool   *tool,
                         gint    x,
                         gint    y,
                         gint    pressure,
                         gulong  time)
{
  ToolClone *tool_clone = (ToolClone *)tool;

  /* remove subpixel precision */
  x = (x/SPP) * SPP;
  y = (y/SPP) * SPP;

  tool_clone->aim_x = x / SPP;
  tool_clone->aim_y = y / SPP;
  tool_clone_set_mode (tool, 3);
}

void
tool_clone_set_mode (Tool *tool,
		     gint  newmode)
{
  ToolClone *tool_clone = (ToolClone *)tool;
  
  if (newmode == tool_clone->mode)
    return;
  
  switch (newmode)
    {
      case 0:
      case 1:
        tool->process_coordinate = tool_clone_process_coordinate;
        break;
      case 2:
      case 3:
        tool->process_coordinate = tool_clone_aim_process_coordinate;
        break;
      default:
        break;
    }
   
  tool_clone->mode = newmode;
  if (tool->tool_icons_created == TRUE)
    tool_clone_set_button_states (tool);
}

gint
tool_clone_get_mode (Tool *tool)
{
  ToolClone *tool_clone = (ToolClone *)tool;
  return tool_clone->mode;
}

static void change_mode (Tool *tool)
{
  if (tool_clone_get_mode (tool) != 3)
    tool_clone_set_aim_mode (NULL, tool);
  else
    tool_clone_set_clone_mode (NULL, tool);
}

Tool *
tool_clone_new ()
{
  ToolClone   *tool_clone = g_malloc0 (sizeof (ToolClone));
  Tool        *tool       = (Tool*) tool_clone;
  
  tool_with_brush_init (tool);
  
  tool->flush = tool_clone_flush;
  tool->tool_fill_tool_specific_settings_box = tool_clone_fill_tool_specific_settings_box;
  tool->uses_brush = TRUE;
  tool->short_name = "Clone";
  tool->change_mode = change_mode;
  
  tool_clone_set_mode (tool, 2);
  ((ToolWithBrush*)tool)->ts.radius  = 65536 * 0.4;
  ((ToolWithBrush*)tool)->ts.opacity = 65536 * 0.6;

  return tool;
}

void
tool_clone_destroy (Tool *tool)
{
  g_free ((ToolClone *)tool);
}
