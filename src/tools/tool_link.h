/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _TOOL_LINK_H
#define _TOOL_LINK_H

#include <glib.h>
#include "tool.h"
#include "link.h"
#include "canvas/canvas.h"
#include "toolkit/label.h"

typedef struct _ToolLink ToolLink;

typedef enum _ToolLinkState
{
  TOOL_LINK_STATE_DST,
  TOOL_LINK_STATE_PAN,
  TOOL_LINK_STATE_SRC,
  TOOL_LINK_STATE_LINK,
  TOOL_LINK_STATE_DELETE,
  TOOL_LINK_STATES
} ToolLinkState;

typedef enum _ToolLinkSrcMode
{
  TOOL_LINK_SRC_MODE_PAN,
  TOOL_LINK_SRC_MODE_MOVE,
  TOOL_LINK_SRC_MODE_TOP_LEFT,
  TOOL_LINK_SRC_MODE_TOP,
  TOOL_LINK_SRC_MODE_TOP_RIGHT,
  TOOL_LINK_SRC_MODE_RIGHT,
  TOOL_LINK_SRC_MODE_BOTTOM_RIGHT,
  TOOL_LINK_SRC_MODE_BOTTOM,
  TOOL_LINK_SRC_MODE_BOTTOM_LEFT,
  TOOL_LINK_SRC_MODE_LEFT
} ToolLinkSrcMode;

typedef struct _Rect
{
  gint x;
  gint y;
  gint width;
  gint height;
} Rect;

struct _ToolLink
{
  Tool            tool;
  ToolLinkState   state;
  ToolLinkSrcMode src_mode;

  Rect            src;
  Rect            dst;

  Link           *selected_link;
 
  Label          *button[TOOL_LINK_STATES]; 
};

Tool * tool_link_new ();
void   tool_link_destroy (Tool *tool);


#endif
