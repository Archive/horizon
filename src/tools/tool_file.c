/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "tool_file.h"
#include "tool_library.h"
#include "config.h"
#include "toolkit/label.h"
#include "toolkit/navigator.h"
#include "canvas/png_util.h"
#include "actions.h"

typedef struct _Rect
{
  gint x;
  gint y;
  gint width;
  gint height;
} Rect;

#define FILE_ITEMS 14


struct _ToolFile
{
  Tool            tool;

  Rect            src;
  Rect            saved_src;

  Label          *status1;

  Box            *bin;
  Box            *export_options;
  Box            *import_options;
  
  gchar          *path; /* also used to check whether we are in import or export mode */
  Box            *file_list;
};

static char *get_export_path (void);

static void  update_src_rect (Tool *tool)
{
  ToolFile *tool_file = (ToolFile*)tool;
  Easel *easel = tool->easel;
  View *view = (View*)easel;

  gint color[4]         = {65535,     65535, 65536,     0};
  gint border_color[4]  = {65535*0.5, 65535, 65536*0.5, 65535*0.5};
  gint surround_color[4]= {0,         0,     0,         65536*0.5};

  view_set_rect_x (view, tool_file->src.x);
  view_set_rect_y (view, tool_file->src.y);
  view_set_rect_width (view, tool_file->src.width);
  view_set_rect_height (view, tool_file->src.height);

  view_set_rect_color (view, color);
  view_set_rect_border_color (view, border_color);
  view_set_rect_surround_color (view, surround_color);

  if(tool->tool_icons_created)
    { 
      static gchar buf[256];
      sprintf (buf, "%ix%i",
         tool_file->src.width, tool_file->src.height);
      label_set_label (tool_file->status1, buf);
    }
}

static void
tool_file_reset (Tool *tool)
{
  ToolFile *tool_file = (ToolFile *)tool;
  Easel *easel = tool->easel;
  View *view = (View*)easel;

  gint view_width;
  gint view_height;

  if (!view)
    return;

  view_width  = box_get_width ((Box*)view) * 65536 / view_get_scale (view);
  view_height = box_get_height ((Box*)view) * 65536 / view_get_scale (view);

  tool_file->src.width  =  view_width * 0.66;
  tool_file->src.height = view_height * 0.66;

  tool_file->src.x = view_get_x (view) + view_width * 0.33 * 0.5;
  tool_file->src.y = view_get_y (view) + view_height * 0.33 * 0.5;

  update_src_rect (tool); 

  tool_file->saved_src.width=0;
  tool_file->saved_src.height=0;
}

static char *get_export_path2 (const char *base, char *path, gint maxlen)
{
  gint        no = 0;

  while (no<1000)
    {
      FILE *file;
      snprintf (path, maxlen, "%shorizon-%03i.png", base, no);
      file = fopen (path, "rb");
      if (!file)
        {
          file = fopen (path, "wb");
          if (file)
            {
              fclose (file);
              return path;
            }
        }
      else
        {
          fclose (file);
        }
      no ++;
    }
  return NULL;
}


static char *get_export_path (void)
{
  static char path[512];
  char *ret;

  ret = get_export_path2   ("/media/mmc1/", path, 512);
  if (!ret)
    ret = get_export_path2 ("/tmp/", path, 512);
  return ret;
}

static void
tool_file_export (Label    *label,
                  gpointer  data)
{
  Tool     *tool      = data;
  ToolFile *tool_file = (ToolFile *)tool;
  gchar *path;

  if (tool_file->saved_src.x == tool_file->src.x &&
      tool_file->saved_src.y == tool_file->src.y &&
      tool_file->saved_src.width == tool_file->src.width &&
      tool_file->saved_src.height == tool_file->src.height)
    {
      return;
    }
  
  if (!(path=get_export_path ()))
    {
      horizon_status (tool->horizon, "unable to find a writable file in /media/mmc1 and /tmp");
      return;
    }


  horizon_status (tool->horizon, "Saving...");
  action (tool->horizon, "refresh");
  canvas_export_png (tool->canvas, path, tool_file->src.x, tool_file->src.y,
                                         tool_file->src.width, tool_file->src.height);
  tool_file->saved_src=tool_file->src;
  horizon_status (tool->horizon, path);

  return;
}

static void
coordinate_start (Tool   *tool,
                  gint    x,
                  gint    y,
                  gint    pressure,
                  gulong  time)
{
  x /= SPP;
  y /= SPP;

  tool->prev_x = x;
  tool->prev_y = y;
  tool->prev_time = time;
}

static void
coordinate_continue (Tool   *tool,
                     gint    x,
                     gint    y,
                     gint    pressure,
                     gulong  time)
{
  ToolFile *tool_file = (ToolFile *)tool;
  Easel *easel = tool->easel;
  View  *view = (View*)easel;

  gint        rel_x;
  gint        rel_y;

  x /= SPP;
  y /= SPP;

  rel_x = (x - tool_file->src.x) * 65536 / (tool_file->src.width);
  rel_y = (y - tool_file->src.y) * 65536 / (tool_file->src.height);

  if (rel_x < 0 ||
      rel_y < 0 ||
      rel_x > 65536 ||
      rel_y > 65536)
    { /* outside rectangle */
      view_set_x (view, view_get_x (view) - (x-tool->prev_x));
      view_set_y (view, view_get_y (view) - (y-tool->prev_y));
    }
  else
    { /* inside rectangle */

      if (tool_file->path ||
          (rel_x > 65536 / 3 &&
          rel_y > 65536 / 3 &&
          rel_x < 65536 * 2 / 3 &&
          rel_y < 65536 * 2 / 3))
        { /* in middle of rectangle */
          tool_file->src.x += (x-tool->prev_x);
          tool_file->src.y += (y-tool->prev_y);
        }
      else
        {
          if (rel_x < 65536 / 3)
            {
              tool_file->src.x += (x-tool->prev_x);
              tool_file->src.width += (tool->prev_x - x);
            }
          else if (rel_x > 65536 * 2 / 3)
            {
              tool_file->src.width += (x-tool->prev_x);
            }

          if (rel_y < 65536 / 3)
            {
              tool_file->src.y += (y-tool->prev_y);
              tool_file->src.height += (tool->prev_y - y);
            }
          else if (rel_y > 65536 * 2 / 3)
            {
              tool_file->src.height += (y-tool->prev_y);
            }
        }
      tool->prev_x = x;
      tool->prev_y = y;
    }
  tool->prev_time = time;
}

static int
flush (Tool *tool)
{
  update_src_rect (tool); 
  return 1;
}


static void
tool_file_do_import (Label    *label,
                     gpointer  data)
{
  Tool     *tool      = data;
  ToolFile *tool_file = (ToolFile *) tool;
  gboolean  undo_disabled = FALSE;

  if (tool_file->path == NULL ||
      (tool_file->saved_src.x == tool_file->src.x &&
      tool_file->saved_src.y == tool_file->src.y &&
      tool_file->saved_src.width == tool_file->src.width &&
      tool_file->saved_src.height == tool_file->src.height))
    {
      return;
    }

  {
    gint width=0, height=0;
    if (horizon_query_png (tool_file->path, &width, &height))
        horizon_query_jpg (tool_file->path, &width, &height);

    if (width * height > 1024 * 1024)
      undo_disabled = TRUE;
    undo_stack_freeze (tool->horizon->undo_stack);
  }
  
  horizon_status (tool->horizon, "Importing...");
  action (tool->horizon, "refresh");

  if (strstr (tool_file->path, "jpg"))
    {
      canvas_import_jpg (tool->canvas, tool_file->path, tool_file->src.x, tool_file->src.y);
    }
  else
    {
      canvas_import_png (tool->canvas, tool_file->path, tool_file->src.x, tool_file->src.y);
    }
  if (undo_disabled)
    undo_stack_thaw (tool->horizon->undo_stack);
  tool_file->saved_src=tool_file->src;
  horizon_status (tool->horizon, "done");
}


#include "canvas/png_util.h"

static void
tool_file_select_file_for_import (Label    *label,
                                  gpointer  data)
{
  Tool *tool = data;
  gint  width;
  gint  height;
  gint status;
  ToolFile *tool_file = (ToolFile *)tool;
  const gchar *path = box_get_id ((Box*)label);

  status = horizon_query_png (path, &width, &height);
  if (status == 0)
    {
      gchar buf[256];
      sprintf (buf, "%s is a %ix%i png file,. select where to import it and press select",
       path, width, height);
      horizon_status (tool->horizon, buf);
      if (tool_file->path)
        g_free (tool_file->path);
      tool_file->path = g_strdup (path);

      tool_file->src.width = width;
      tool_file->src.height = height;
      update_src_rect (tool);
    }
  else
    {
      status = horizon_query_jpg (path, &width, &height);

      if (status == 0)
        {
          gchar buf[256];
          sprintf (buf, "%s is a %ix%i jpg file,. select where to import it and press select",
           path, width, height);
          horizon_status (tool->horizon, buf);
          if (tool_file->path)
            g_free (tool_file->path);
          tool_file->path = g_strdup (path);

          tool_file->src.width = width;
          tool_file->src.height = height;
          update_src_rect (tool);
        }
      else
        {
          gchar buf[256];
          sprintf (buf, "%s is not an importable file", path);
          horizon_status (tool->horizon, buf);
        }
    }
}

#include <glob.h>
#include <libgen.h>

static void fill_file_list (Tool *tool)
{
  ToolFile   *tool_file = (ToolFile*)tool;
  Box *file_list = tool_file->file_list;
  glob_t globbuf;
  gint i;

  glob ("/media/mmc1/*.png", GLOB_ERR, NULL, &globbuf);
  glob ("/media/mmc1/*.jpg", GLOB_ERR | GLOB_APPEND, NULL, &globbuf);
  glob ("/tmp/*.png", GLOB_ERR | GLOB_APPEND, NULL, &globbuf);
  glob ("/tmp/*.jpg", GLOB_ERR | GLOB_APPEND, NULL, &globbuf);

  for (i=0; globbuf.gl_pathv[i]!=NULL && i<FILE_ITEMS; i++)
    {
      Label *item = (Label*)box_get_child (file_list, i);
      box_set_id ((Box*)item, globbuf.gl_pathv[i]);
        {
          gchar *tmp = g_strdup (globbuf.gl_pathv[i]);
          label_set_label (item, basename (tmp));
          label_set_event (item, tool_file_select_file_for_import, tool);
          g_free (tmp);
        }
      box_set_child (file_list, i, (Box*)item);
    }
  globfree (&globbuf);

  for (;i<FILE_ITEMS;i++)
    {
      Label *item = (Label*)box_get_child (file_list, i);
      label_set_event (item, NULL, NULL);
    }
}

static void
tool_file_export_mode (Label    *label,
                       gpointer  data)
{
  Tool     *tool      = data;
  ToolFile *tool_file = (ToolFile *)tool;

  if (tool_file->path)
    g_free (tool_file->path);
  tool_file->path = NULL;

  {
    horizon_status (tool->horizon, "Select rectangular region to export.");
  }
  box_set_child (tool_file->bin, 0, tool_file->export_options);
  label_set_active ((Label*)box_id (tool->tool_settings_box, "file_import_tab"), FALSE);
  label_set_active ((Label*)box_id (tool->tool_settings_box, "file_export_tab"), TRUE);
}

static void
tool_file_import_mode (Label    *label,
                       gpointer  data)
{
  Tool     *tool      = data;
  ToolFile *tool_file = (ToolFile *)tool;

  {
    horizon_status (tool->horizon, "Pick one of the files listed in the tool context area (png files from /media/mmc1/");
  }
  box_set_child (tool_file->bin, 0, tool_file->import_options);
  fill_file_list (tool);

  label_set_active ((Label*)box_id (tool->tool_settings_box, "file_import_tab"), TRUE);
  label_set_active ((Label*)box_id (tool->tool_settings_box, "file_export_tab"), FALSE);
}

static void
create_import_box (Tool *tool)
{
  ToolFile   *tool_file = (ToolFile*)tool;
  Canvas     *canvas      = tool->canvas;
  Box        *vbox;
  gint ratios[]={9,1};
  Label      *do_import;
  Box        *file_list = vbox_new (FILE_ITEMS);

  vbox = vbox_new (2);
  tool_file->import_options = vbox;
  tool_file->file_list = file_list;

  box_set_child (vbox, 0, file_list);

  {
    gint i;
    for (i=0; i<FILE_ITEMS; i++)
      {
        Label *item = label_new (canvas);
        box_set_child (file_list, i, (Box*)item);
        label_set_font_size (item, 9);
      }
  }

  do_import = label_new (canvas);
  label_set_label (do_import, "Do import");
  label_set_event (do_import, tool_file_do_import, tool);
  
  box_set_child (vbox, 1, (Box*) do_import);
  box_set_ratios (vbox, ratios);
}

static void
create_export_box (Tool *tool)
{
  ToolFile   *tool_file = (ToolFile*)tool;
  Canvas     *canvas      = tool->canvas;
  Box        *vbox;
  Label      *label;
  gint ratios[]={5,1,1};

  vbox = vbox_new (3);
  
  {
    Navigator *navigator = navigator_new (canvas);
    navigator_set_easel (navigator, tool->easel);
    box_set_child (vbox, 0, (Box*)navigator);
  }
 
  label = label_new (canvas);
  label_set_event (label, tool_file_export, tool);
  tool_file->status1 = label;
  label_set_font_size (label, 10);
  label_set_label (label, "");
  box_set_child (vbox, 1, (Box*) label);

  label = label_new (canvas);
  label_set_event (label, tool_file_export, tool);
  label_set_label (label, "Save file");
  box_set_child (vbox, 2, (Box*) label);

  box_set_ratios (vbox, ratios);

  tool_file->export_options = vbox;
}

static void
tool_file_fill_tool_specific_settings_box (Tool *tool)
{
  ToolFile   *tool_file = (ToolFile*)tool;
  Canvas     *canvas      = tool->canvas;
  Box        *vbox;
  Box        *hbox;
  gint ratios[]={1,10};
  Label      *label;

  tool_file->bin = bin_new ();

  vbox = vbox_new (2);

  tool->tool_settings_box = vbox;
  box_set_ratios (vbox, ratios);

  hbox = hbox_new (2);
  label = label_new (canvas);
  label_set_label (label, "Import");
  box_set_id ((Box*)label, "file_import_tab");
  label_set_event (label, tool_file_import_mode, tool);
  box_set_child (hbox, 0, (Box*) label);
  label_set_font_size (label, 8);

  label = label_new (canvas);
  label_set_label (label, "Export");
  box_set_id ((Box*)label, "file_export_tab");
  label_set_event (label, tool_file_export_mode, tool);
  label_set_font_size (label, 8);
  box_set_child (hbox, 1, (Box*) label);

  box_set_child (vbox, 0, hbox);
  box_set_child (vbox, 1, tool_file->bin);

  create_import_box (tool);
  create_export_box (tool);

  tool_file_export_mode (NULL, tool);
}

static void
process_coordinate (Tool   *tool,
                    gint    x,
                    gint    y,
                    gint    pressure,
                    gulong  time)
{
  if (time - tool->prev_time > 100)
   {
     coordinate_start (tool, x, y, pressure, time);
   }
  coordinate_continue (tool, x, y, pressure, time);
}


static void
selected (Tool *tool)
{
  tool_file_reset (tool);
}

static void
deselected (Tool *tool)
{
  Easel *easel = tool->easel;
  View  *view = (View*)easel;

  view_set_rect_visible (view, FALSE);
}

static void change_mode (Tool *tool)
{
  ToolFile *tool_file = (ToolFile*)tool;

  if (tool_file->path)
    tool_file_do_import (NULL, tool);
  else
    tool_file_export (NULL, tool);
}

Tool *
tool_file_new ()
{
  Tool *tool_file = g_malloc0 (sizeof (ToolFile));
  Tool *tool = (Tool*) tool_file;
  
  tool_init (tool);
  
  tool->flush = flush;
  tool->tool_fill_tool_specific_settings_box = tool_file_fill_tool_specific_settings_box;
  tool->process_coordinate = process_coordinate;
  tool->uses_brush = FALSE;
  tool->short_name = "File";
  tool->change_mode = change_mode;

  tool->select = selected;
  tool->deselect = deselected;
  
  return tool;
}

void
tool_file_destroy (Tool *tool)
{
  g_free ((ToolFile *)tool);
}
