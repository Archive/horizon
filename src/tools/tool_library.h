/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Eero Tanskanen <yendor@nic.fi>
                             Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _TOOL_LIBRARY_H
#define _TOOL_LIBRARY_H

#include "tool.h"
#include "tool_with_brush.h"
#include "horizon.h"

void tool_library_create_tool_uis (Horizon *horizon);
void tool_library_create_tool_box (Canvas *canvas, Box *toolbox_dest, Box *root);

Box *tool_library_get_tool_box (Canvas *canvas);

Tool *lookup_tool (const gchar *name);
void  set_current_tool (Tool *new_tool);
Tool *get_current_tool (void);

void create_tools (void);
void tools_destroy (void);

void tool_library_set_tool (Tool *tool);

void tool_library_set_horizon (Horizon *new_horizon);

#endif
