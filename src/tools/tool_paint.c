/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>

#include "toolkit/label.h"
#include "canvas/canvas.h"
#include "brush.h"
#include "tool_paint.h"
#include "tool.h"

#include "config.h"


static Allocation *palette = NULL;

static gboolean palette_handler (View     *view,
                                 gint      x,
                                 gint      y,
                                 gint      pressure,
                                 gulong    time,
                                 gpointer  data);

#define px_dist(x0,y0,x1,y1) sqrt(((x1)-(x0))*((x1)-(x0))+((y1)-(y0))*((y1)-(y0)))

static void render_palette (Canvas *canvas, 
                            gint    cx,
                            gint    cy,
                            gint    width,
                            gint    height)
{
  guchar *buf;
  gint x,y;
 
  buf = g_malloc (width*height*4);
  if (!buf)
    return;

  height-=15;
  for (y=0;y<height;y++)
    for (x=0;x<width;x++)
      {
        gint red   = 256 * px_dist (x,y, 0,0) / height * 0.75;
        gint green = 256 * px_dist (x,y, width,0) / height * 0.75;
        gint blue  = 256 * px_dist (x,y, width/2,height) / height * 0.75;

        if (red>255)
          red=255;
        if (green>255)
          green=255;
        if (blue>255)
          blue=255;

        red=255-red;
        green=255-green;
        blue=255-blue;

        buf[y * width * 4 + x * 4 + 0] = red;
        buf[y * width * 4 + x * 4 + 1] = green;
        buf[y * width * 4 + x * 4 + 2] = blue;
      }
  height+=15;
  for (y=height-15;y<height;y++)
    for (x=0;x<width;x++)
      {
        gint red=255 * x/width;
        gint green=255 * x/width;
        gint blue=255 * x/width;

        buf[y * width * 4 + x * 4 + 0] = red;
        buf[y * width * 4 + x * 4 + 1] = green;
        buf[y * width * 4 + x * 4 + 2] = blue;
      }
  {
    CanvasRect *rect = canvas_rect_new (canvas, cx, cy, width, height);
    canvas_rect_import_buf (rect, buf);
    g_object_unref (rect);
  }
  g_free (buf);
}

static void palette_render (View     *view,
                            gpointer  data)
{
  Box    *box = (Box *)view;
  Canvas *canvas = view->canvas;

  if (palette == NULL)
    {
      palette = canvas_alloc (canvas, box_get_width (box), box_get_height (box));
      render_palette (canvas,
         CANVAS_RECT(palette)->x, CANVAS_RECT(palette)->y,
         CANVAS_RECT(palette)->width, CANVAS_RECT(palette)->height);

      view_set_x (view, CANVAS_RECT(palette)->x);
      view_set_y (view, CANVAS_RECT(palette)->y);
      view_set_scale (view, 65536);
    }
}

static gboolean palette_handler (View *view,
                                 gint      x,
                                 gint      y,
                                 gint      pressure,
                                 gulong    time,
                                 gpointer  data)
{
  ToolPaint   *tool_paint = data;
  ToolSetting *ts = &((ToolWithBrush *) tool_paint)->ts;

  Canvas *canvas = view->canvas;

  if (tool_paint->pick_mode)
    {
      canvas_stamp (
             canvas, SPP * (CANVAS_RECT (palette)->x + (x>>16)) ,
                     SPP * (CANVAS_RECT (palette)->y + (y>>16)) ,
             ts->radius * MAX_RADIUS / 65536,
             ts->opacity * MAX_OPACITY / 65536,
             ts->hardness,
             ts->red, ts->green, ts->blue);
    }
  else
    {
      guint old_red = ts->red;
      guint old_green = ts->green;
      guint old_blue = ts->blue;

      canvas_pick (canvas, CANVAS_RECT (palette)->x + (x>>16),
                           CANVAS_RECT (palette)->y + (y>>16),
                   &ts->red, &ts->green, &ts->blue);
      
      if ((old_red != ts->red) || (old_green != ts->green) ||  (old_blue != ts->blue))
        tool_setting_changed (ts);
    }

  return TRUE;
}

Box *palette_box_new (Canvas      *canvas,
                      ToolPaint   *tool_paint)
{
  View *palette_view;

  palette_view  = view_new (canvas);

  view_set_event (palette_view, palette_handler, tool_paint);
  view_set_render (palette_view, palette_render, NULL);
  return (Box*)palette_view;
}

static void
tool_paint_start (Tool  *tool,
                  gint   x,
                  gint   y,
                  gint   pressure,
                  gulong time)
{
  ToolSetting *ts;
  ToolPaint *tool_paint = (ToolPaint*)tool;
  
  if (tool->uses_brush == FALSE)
    return;
  ts = &((ToolWithBrush *) tool)->ts;
   
  /* only add a new undostep if there is more than
   * 0.5 second between strokes */
  if (time - tool->prev_time > 500)
    {
      undo_stack_new_step (canvas_get_undo_stack (tool->canvas));
    }

  tool->prev_x = x;
  tool->prev_y = y;
  tool->prev_pressure = pressure;
  tool->prev_time=time;

  tool_paint->need_to_travel = 0;
  tool_paint->traveled_length = 0;
}

typedef struct Point
{
  gint x;
  gint y;
} Point;

static void
lerp (Point *dest,
      Point *a,
      Point *b,
      gint t)
{
  dest->x = a->x + (b->x-a->x) * t / 65536;
  dest->y = a->y + (b->y-a->y) * t / 65536;
}

#define iter1(N) \
    try = root + (1 << (N)); \
    if (n >= try << (N))   \
    {   n -= try << (N);   \
        root |= 2 << (N); \
    }

static inline guint isqrt (guint n)
{
    guint root = 0, try;
    iter1 (15);    iter1 (14);    iter1 (13);    iter1 (12);
    iter1 (11);    iter1 (10);    iter1 ( 9);    iter1 ( 8);
    iter1 ( 7);    iter1 ( 6);    iter1 ( 5);    iter1 ( 4);
    iter1 ( 3);    iter1 ( 2);    iter1 ( 1);    iter1 ( 0);
    return root >> 1;
}

static gint
point_dist (Point *a,
            Point *b)
{
  return isqrt ((a->x-b->x)*(a->x-b->x) +
                (a->y-b->y)*(a->y-b->y));
}

static void
tool_paint_continue (Tool   *tool,
                     gint    x,
                     gint    y,
                     gint    pressure,
                     gulong  time)
{
  Point a,b;
  ToolSetting *ts;
  ToolPaint *tool_paint = (ToolPaint*)tool;
  gint spacing;
  gint local_pos;
  gint distance;
  gint offset;
  gint leftover;
   
  if (tool->uses_brush == FALSE)
    return;
  ts = &((ToolWithBrush *) tool)->ts;

  a.x = tool->prev_x;
  a.y = tool->prev_y;

  b.x = x;
  b.y = y;

  spacing = ts->spacing * (ts->radius * MAX_RADIUS / 65536) / 65536;

  if (spacing < MIN_SPACING * SPP / 65536)
    spacing = MIN_SPACING * SPP / 65536;

  distance = point_dist (&a, &b);

  leftover = tool_paint->need_to_travel - tool_paint->traveled_length;

  offset = spacing - leftover;
  local_pos = offset;

  if (tool_paint->traveled_length == 0 &&
      tool_paint->need_to_travel == 0)
    {
      local_pos=0;
      distance=0;
      spacing=1;
    }

  if (distance<1)
    distance=1;

    for (;
      local_pos <= distance;
      local_pos += spacing)
      {
        Point spot;
        gint ratio = 65536 * local_pos / distance;

        lerp (&spot, &a, &b, ratio);

        canvas_stamp (tool->canvas, spot.x, spot.y,
           ts->radius * MAX_RADIUS / 65536,
           ts->opacity * MAX_OPACITY / 65536,
           ts->hardness,
           ts->red, ts->green, ts->blue);

        tool_paint->traveled_length += spacing;
      }
    tool_paint->need_to_travel += distance;
    
    tool->prev_x = x;
    tool->prev_y = y;
    //tool->prev_pressure = tp;
    tool->prev_time = time;
}

int
tool_paint_flush (Tool *tool)
{
  return 0;
}

static void
toggle_pick_mode (Label    *label,
               gpointer  data)
{
  ToolPaint *tool_paint = data;
  if (tool_paint->pick_mode)
    {
      tool_paint->pick_mode = FALSE;
      label_set_label (tool_paint->pick_state_toggle, "<");
    }
  else
    {
      tool_paint->pick_mode = TRUE;
      label_set_label (tool_paint->pick_state_toggle, ">");
    }
}

static void
restore_palette (Label *label,
                 gpointer data)
{
  Tool* tool = data;

  /* FIXME: just pass the Rect */
  render_palette (tool->canvas,
     CANVAS_RECT (palette)->x, CANVAS_RECT (palette)->y,
     CANVAS_RECT (palette)->width, CANVAS_RECT (palette)->height);
}


void tool_paint_fill_tool_specific_settings_box (Tool *tool)
{
  ToolSetting *ts;
  ToolPaint *tool_paint = (ToolPaint*) tool;
  Canvas *canvas = tool->canvas;

  if (tool->uses_brush == FALSE)
    return;
  ts = &((ToolWithBrush *) tool)->ts;
  ts->canvas = canvas;

  {
    Box *vbox = vbox_new (4);
    gint ratios[]={3,4,3,4};
    box_set_ratios (vbox, ratios);

    box_set_child (vbox, 0, tool_setting_get_brush_sliders (ts, canvas));
    box_set_child (vbox, 1, tool_setting_get_brush_preview (ts, canvas));
    box_set_child (vbox, 2, tool_setting_get_color_sliders (ts, canvas));

    {
      Box *hbox = hbox_new (2);
      gint ratios[]={13,85};
      box_set_ratios (hbox, ratios);
      
      {
        Box *vbox = vbox_new (2);
        Label *toggle = label_new (canvas);
        Label *restore = label_new (canvas);

        tool_paint->pick_state_toggle = toggle;

        label_set_font_size (toggle, 8);
        label_set_font_size (restore, 10);

        label_set_label (toggle, "<");
        label_set_label (restore,  "@");

        label_set_event (toggle, toggle_pick_mode, tool_paint);
        label_set_event (restore, restore_palette, tool_paint);
        
        box_set_child (vbox, 0, (Box*)toggle);
        box_set_child (vbox, 1, (Box*)restore);

        box_set_child (hbox, 0, vbox);
      }

      box_set_child (hbox, 1, palette_box_new (canvas, tool_paint));

      box_set_child (vbox, 3, hbox);
    }

    tool->tool_settings_box = vbox;
  }
}

static void
tool_pick_start (Tool   *tool,
                 gint    x,
                 gint    y,
                 gint    pressure,
                 gulong  time)
{
  ToolSetting *ts = &((ToolWithBrush *) tool)->ts;

  x /= SPP;
  y /= SPP;

  canvas_pick (tool->canvas, x, y, &ts->red, &ts->green, &ts->blue);
  tool_setting_changed (ts);
 
  tool->prev_x = x;  /* prev is used as the /grabbed coordinate/ in pick tool */
  tool->prev_y = y;
  tool->prev_time = time;
}

static void
tool_pick_continue (Tool  *tool,
                    gint   x,
                    gint   y,
                    gint   pressure,
                    gulong time)
{
  Easel *easel = tool->easel;
  View  *view  = (View*)easel;
  x /= SPP;
  y /= SPP;

  view_set_x (view, view_get_x (view) - (x-tool->prev_x));
  view_set_y (view, view_get_y (view) - (y-tool->prev_y));

  tool->prev_pressure=pressure;
  tool->prev_time=time;
}

void
tool_paint_process_coordinate (Tool   *tool,
                               gint    x,
                               gint    y,
                               gint    pressure,
                               gulong  time)
{
  ToolPaint *tool_paint = (ToolPaint*)tool;

  if (tool_paint->pick_mode)
    {
      if (time - tool->prev_time > 100)
       {
         tool_pick_start (tool, x, y, pressure, time);
       }
      tool_pick_continue (tool, x, y, pressure, time);
    }
  else
    {
      if (time - tool->prev_time > 100)
       {
         tool_paint_start (tool, x, y, pressure, time);
       }
      tool_paint_continue (tool, x, y, pressure, time);
    }
}

static void change_mode (Tool *tool)
{
  toggle_pick_mode (NULL, tool);
}

Tool *
tool_paint_new ()
{
  ToolPaint *tool_paint = g_malloc0 (sizeof (ToolPaint));
  Tool *tool = (Tool*) tool_paint;
  
  tool_with_brush_init (tool);
  
  tool->process_coordinate = tool_paint_process_coordinate;
  tool->flush = tool_paint_flush;
  tool->tool_fill_tool_specific_settings_box = tool_paint_fill_tool_specific_settings_box;
  tool->uses_brush = TRUE;
  tool->short_name = "Paint";
  tool->change_mode = change_mode;

  return tool;
}

void
tool_paint_destroy (Tool *tool)
{
  g_free ((ToolPaint *)tool);
}
