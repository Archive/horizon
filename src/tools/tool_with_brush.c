/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "brush.h"
#include "tool.h"
#include "tool_library.h"
#include "toolkit/easel.h"
#include "toolkit/view.h"
#include "toolkit/slider.h"
#include "toolkit/widget.h"
#include "vectors/path.h"
#include "tool_with_brush.h"

#include "config.h"

/* Tool with brush */

void tool_with_brush_init (Tool *tool)
{
  ToolWithBrush *toolwithbrush = (ToolWithBrush*) tool;
  ToolSetting   *ts = &toolwithbrush->ts;
  tool_init (tool);

  ts->canvas      = NULL;
  ts->red         = 65536 * 0.0;
  ts->green       = 65536 * 0.0;
  ts->blue        = 65536 * 0.0;

  ts->radius      = 65536 * 0.02;
  ts->opacity     = 65535 * 0.2;
  ts->hardness    = 65536 * 0.2;

  ts->min_radius  = ts->radius;
  ts->max_radius  = ts->radius;
  ts->min_opacity = ts->opacity;
  ts->max_opacity = ts->opacity;

  ts->spacing     = 65536 * 0.2;
}

ToolSetting *tool_with_brush_get_toolsetting (Tool *tool)
{
  ToolWithBrush *toolwithbrush = (ToolWithBrush*) tool;
  return &toolwithbrush->ts;
}

void tool_with_brush_destroy (Tool *tool)
{
}

#define UI_RADIUS_GAMMA   2.0
#define UI_OPACITY_GAMMA  1.5
#define UI_HARDNESS_GAMMA  1.5

Box *tool_setting_get_brush_preview (ToolSetting *ts, Canvas *canvas)
{
  if (ts->brush_preview == NULL)
    {
      ts->brush_preview = brush_preview_new (canvas);
      brush_preview_set_tool_setting (ts->brush_preview, ts);
    }
  return (Box*)ts->brush_preview;
}

void tool_setting_changed (ToolSetting *ts)
{
  if (ts->brush_preview)
    box_set_dirty ((Box*)ts->brush_preview);

  if (ts->brush_radius_slider)
    slider_set_value ((Slider*)ts->brush_radius_slider, 
       pow (ts->radius/65536.0, (1.0/UI_RADIUS_GAMMA))* 65536
       );
  if (ts->brush_opacity_slider)
    slider_set_value ((Slider*)ts->brush_opacity_slider, 
       pow (ts->opacity/65536.0, (1.0/UI_OPACITY_GAMMA))* 65536
       );
  if (ts->brush_hardness_slider)
    slider_set_value ((Slider*)ts->brush_hardness_slider, 
       pow (ts->hardness/65536.0, (1.0/UI_HARDNESS_GAMMA))* 65536
       );

  if (ts->color_red_slider)
    slider_set_value ((Slider*)ts->color_red_slider, ts->red);
  if (ts->color_green_slider)
    slider_set_value ((Slider*)ts->color_green_slider, ts->green);
  if (ts->color_blue_slider)
    slider_set_value ((Slider*)ts->color_blue_slider, ts->blue);

}

void
tool_setting_slider_handler (Slider *slider, gpointer data)
{
  ToolSetting *ts = (ToolSetting*) data;
  /* Variable changed depends on the slider that changed. */
  if ((gpointer)slider == (gpointer)ts->brush_radius_slider)
    {
      ts->radius = slider_get_value (slider);
      ts->radius = pow (ts->radius / 65536.0, UI_RADIUS_GAMMA) * 65536;
      
      if (ts->radius < 2)
	ts->radius = 2;
    }
  else if ((gpointer)slider == (gpointer)ts->brush_opacity_slider)
    {
      ts->opacity = slider_get_value (slider);
      ts->opacity = pow (ts->opacity / 65536.0, UI_OPACITY_GAMMA) * 65536;
    }
  else if ((gpointer)slider == (gpointer)ts->brush_hardness_slider)
    {
      ts->hardness = slider_get_value (slider);
      ts->hardness = pow (ts->hardness / 65536.0, UI_HARDNESS_GAMMA) * 65536;
    }
  else if ((gpointer)slider == (gpointer)ts->color_red_slider)
    {
      ts->red = slider_get_value (slider);
    }
  else if ((gpointer)slider == (gpointer)ts->color_green_slider)
    {
      ts->green = slider_get_value (slider);
    }
  else if ((gpointer)slider == (gpointer)ts->color_blue_slider)
    {
      ts->blue = slider_get_value (slider);
    }
  else
    g_print ("eep!\n");
 
  tool_setting_changed (ts);
}

Box *tool_setting_get_brush_sliders (ToolSetting *ts, Canvas *canvas)
{
  if (ts->brush_settings_box == NULL)
   {
     Box *vbox = vbox_new (3);
     ts->brush_settings_box = vbox;

     ts->brush_radius_slider = slider_new (canvas);
     ts->brush_opacity_slider = slider_new (canvas);
     ts->brush_hardness_slider = slider_new (canvas);

     slider_set_value_changed_function (ts->brush_radius_slider, tool_setting_slider_handler, ts);
     slider_set_value_changed_function (ts->brush_opacity_slider, tool_setting_slider_handler, ts);
     slider_set_value_changed_function (ts->brush_hardness_slider, tool_setting_slider_handler, ts);

     box_set_child (vbox, 0, (Box*)ts->brush_radius_slider);
     box_set_child (vbox, 1, (Box*)ts->brush_opacity_slider);
     box_set_child (vbox, 2, (Box*)ts->brush_hardness_slider);

     tool_setting_changed (ts);
   }
  return ts->brush_settings_box;
}

Box *tool_setting_get_color_sliders (ToolSetting *ts, Canvas *canvas)
{
  if (ts->color_sliders_box == NULL)
   {
     Box *vbox = vbox_new (3);
     ts->color_sliders_box = vbox;

     ts->color_red_slider = slider_new (canvas);
     ts->color_green_slider = slider_new (canvas);
     ts->color_blue_slider = slider_new (canvas);

     slider_set_value_changed_function (ts->color_red_slider, tool_setting_slider_handler, ts);
     slider_set_value_changed_function (ts->color_green_slider, tool_setting_slider_handler, ts);
     slider_set_value_changed_function (ts->color_blue_slider, tool_setting_slider_handler, ts);

     box_set_child (vbox, 0, (Box*)ts->color_red_slider);
     box_set_child (vbox, 1, (Box*)ts->color_green_slider);
     box_set_child (vbox, 2, (Box*)ts->color_blue_slider);

     widget_set_background_color ((Widget*)ts->color_red_slider, 65536, 0, 0);
     widget_set_background_color ((Widget*)ts->color_green_slider, 0, 65536, 0);
     widget_set_background_color ((Widget*)ts->color_blue_slider, 0, 0, 65536);
#if 0
     slider_set_secondary_color (ts->color_red_slider, 0, 0, ts->red);
     slider_set_secondary_color (ts->color_green_slider, 0, 0, ts->green);
     slider_set_secondary_color (ts->color_blue_slider, 0, 0, ts->blue);
#endif
     tool_setting_changed (ts);
   }
  return ts->color_sliders_box;
}
