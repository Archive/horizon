/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _TOOL_WITH_BRUSH_H
#define _TOOL_WITH_BRUSH_H

#include <glib.h>
typedef struct _ToolWithBrush ToolWithBrush;
typedef struct _ToolSetting ToolSetting;

#include "horizon.h"
#include "toolkit/view.h"
#include "toolkit/label.h"
#include "toolkit/slider.h"
#include "toolkit/easel.h"
#include "toolkit/brush_preview.h"
#include "canvas/canvas.h"
#include "tool.h"

struct _ToolSetting {
  gint  refcount;
  
  Canvas *canvas;

  guint red;
  guint green;
  guint blue;

  guint hardness;
  guint opacity;
  guint min_opacity;
  guint max_opacity;
  guint radius;
  guint min_radius;
  guint max_radius;
  guint spacing;
  
  Box  *brush_settings_box;
  Box  *color_sliders_box;
  
  Slider *brush_radius_slider;
  Slider *brush_opacity_slider;
  Slider *brush_hardness_slider;
  
  Slider *color_red_slider;
  Slider *color_green_slider;
  Slider *color_blue_slider;

  BrushPreview *brush_preview;
};

struct _ToolWithBrush {
  Tool            tool;
  ToolSetting     ts;
};

void         tool_with_brush_init (Tool *tool);
ToolSetting *tool_with_brush_get_toolsetting (Tool *tool);
void         tool_with_brush_destroy (Tool *tool);

View *tool_setting_get_brush_sliders_view (ToolSetting *ts, Canvas *canvas);
View *tool_setting_get_color_sliders_view (ToolSetting *ts, Canvas *canvas);

Box *tool_setting_get_brush_preview (ToolSetting *ts, Canvas *canvas);
Box *tool_setting_get_brush_sliders (ToolSetting *ts, Canvas *canvas);
Box *tool_setting_get_color_palette (ToolSetting *ts, Canvas *canvas);
Box *tool_setting_get_color_sliders (ToolSetting *ts, Canvas *canvas);

void tool_setting_changed (ToolSetting *ts);

#endif
