/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _BOX_H
#define _BOX_H

#include <glib.h>
#include <SDL.h>

typedef struct _Box Box;

typedef enum BoxType
{
  BOX_PRIMITVE,
  BOX_BIN,
  BOX_VSPLIT,
  BOX_HSPLIT,
  BOX_VBOX,
  BOX_HBOX,
  BOX_GRID,
  BOX_VIEW
} BoxType;

struct _Box
{
  BoxType   type;
  gint      children;
  /* the child boxes of a non-leaf box */
  Box      *child[2];

  gint      ratio;         /* 16.16,. and normalized,. so 0..65536 */

  char     *id;

  gboolean  vertical;     

  /* recalculated each time a reclculation request is performed on the root
   * box
   */
  gint      width; /* in pixels */
  gint      height;
};

Box *         box_new          (void);

void          box_init         (Box         *box);

void          box_destroy      (Box         *box);

int           box_get_ratio    (Box         *box);

void          box_set_ratio    (Box         *box,
                                gint         ratio);

Box *         box_get_child    (Box         *box,
                                gint         index);

void          box_set_child    (Box         *box,
                                gint         index,
                                Box         *new_child);

gboolean      box_event        (Box         *root,
                                gint         x,
                                gint         y,
                                gint         pressure,
                                gulong       time);

void          box_render       (Box         *root);


void          box_recalc_sizes (Box         *box,
                                int          width,
                                int          height);

gint          box_get_width    (Box         *box);

gint          box_get_height   (Box         *box);

void          box_set_dirty    (Box         *box);

Box *         bin_new          (void);

Box *         hsplit_new       (void);

Box *         vsplit_new       (void);

Box *         vbox_new         (gint         children);

Box *         hbox_new         (gint         children);

void          box_set_ratios   (Box         *box,
                                gint         sizes[]);

Box *         grid_new         (gint         cols,
                                gint         rows);

Box *         box_id           (Box         *box,
                                const gchar *id);

void          box_set_id       (Box         *box,
                                const gchar *id);

const gchar * box_get_id       (Box         *box);

#endif
