/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _VIEW_H
#define _VIEW_H

typedef struct _View View;
typedef struct _ViewRect ViewRect;

#include "box.h"
#include "canvas/canvas.h"

struct _View
{
  Box        box;
  
  Canvas    *canvas;

  gint       tile_width;  /* cached from canvas to avoid function */
  gint       tile_height; /* call overhead for blits */

  gint       x;
  gint       y;
  gint       scale;

  gboolean (*event) (View     *view,
                     gint      x,
                     gint      y,
                     gint      pressure,
                     gulong    time,
                     gpointer  data);
  
  /* extra data member passed on as usage data for the event handler */
  gpointer   event_data;

  void     (*render) (View     *view,
                      gpointer  data);
  gpointer   render_data;

  gint       last_x;
  gint       last_y;
  gint       last_scale;

  ViewRect  *rect;
  gboolean   dirty;
};

struct _ViewRect
{
  gint x;
  gint y;
  gint width;
  gint height;
  gint border_px;
  gint surround_color[4];
  gint border_color[4];
  gint color[4];
};

void view_lookat                     (View   *view,
                                      Canvas *canvas,
                                      gint    x,
                                      gint    y,
                                      gint    scale);

void
view_init                            (View      *view,
                                      Canvas    *canvas);

void     view_set_event              (View      *view,
                                      gboolean (*event) (View *view,
                                                         gint     x,
                                                         gint     y,
                                                         gint     pressure,
                                                         gulong   time,
                                                         gpointer data),
                                      gpointer   data);

void     view_set_render             (View      *view,
                                      void     (*render) (View    *view,
                                                          gpointer data),
                                      gpointer   data);

View   * view_new                    (Canvas    *canvas);

void     view_destroy                (View      *view);

void     view_set_x                  (View      *view,
                                      glong      x);

glong    view_get_x                  (View      *view);

void     view_set_y                  (View      *view,
                                      glong      y);

glong    view_get_y                  (View      *view);

void     view_set_scale              (View      *view,
                                      gint       scale);
gint     view_get_scale              (View      *view);

Canvas * view_get_canvas             (View      *view);

void     view_refresh                (View      *view,
                                      gint       x0,
                                      gint       y0,
                                      gint       x1,
                                      gint       y1);

void     views_flush                  (void);

void     view_set_dirty               (View     *view);

gboolean view_is_dirty                (View     *view);

void     view_set_center_x            (View     *view,
                                       glong     x);

glong    view_get_center_x            (View     *view);

glong    view_get_center_y            (View     *view);

void     view_set_center_y            (View     *view,
                                       glong     y);

void     view_set_center_scale        (View     *view,
                                       gint      scale);

void     view_set_rect_visible        (View     *view,
                                       gboolean  visible);

void     view_set_rect_x              (View     *view,
                                       gint      x);

void     view_set_rect_y              (View     *view,
                                       gint      y);

void     view_set_rect_width          (View     *view,
                                       gint      width);

void     view_set_rect_height         (View     *view,
                                       gint      height);

void     view_set_rect_border_px      (View     *view,
                                       gint      border_px);

void     view_set_rect_color          (View     *view,
                                       gint      color[4]);

void     view_set_rect_border_color   (View     *view,
                                       gint      color[4]);

void     view_set_rect_surround_color (View     *view,
                                       gint      color[4]);

#endif
