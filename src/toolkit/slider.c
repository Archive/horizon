/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Eero Tanskanen <yendor@nic.fi>
                       Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "canvas/canvas.h"
#include "view.h"
#include "widget.h"
#include "slider.h"

#include "config.h"

/* Changing this value doesn't work at this point. */
#define SLIDER_MAX  65536

struct _Slider
{
  Widget   widget;
  
  /* Always return value between min 0 and SLIDER_MAX */
  
  guint    value;
  guint    old_value;
  gpointer data;
  
  Allocation *allocation;
  
  void (*value_changed_function) (Slider   *slider,
		                  gpointer  data);
};

static void
render (View     *view,
        gpointer  data)
{
  Box    *box = (Box*) view;
  Slider *slider = (Slider*) view;
  Widget *widget = (Widget*) view;

  gint x = CANVAS_RECT (slider->allocation)->x;
  gint y = CANVAS_RECT (slider->allocation)->y;
  gint width = box_get_width (box);
  gint height = box_get_height (box);
  gint mini_box_size = (height / 4) | 1;
  gint bar_height = (height - (mini_box_size * 2)) * 6 / 10;
  gint value = slider->value;
   
  if (value == slider->old_value)
    {
      return;
    }
  slider->old_value = value;

  x = x + value * width / SLIDER_MAX;
  y = y + height / 2;

  canvas_fill_rect (view->canvas,
     CANVAS_RECT (slider->allocation)->x, CANVAS_RECT (slider->allocation)->y,
     width, height,
     widget->background_color[0],
     widget->background_color[1],
     widget->background_color[2],
     65536);
  canvas_fill_rect (view->canvas,
    x, y-bar_height/2,
    1, bar_height,
    widget->primary_color[0],
    widget->primary_color[1],  
    widget->primary_color[2],
    65536);
  canvas_fill_rect (view->canvas,
    x-mini_box_size/2, y-(bar_height/2+(mini_box_size)),
    mini_box_size, mini_box_size,
    widget->primary_color[0],
    widget->primary_color[1],  
    widget->primary_color[2],
    65536);
  canvas_fill_rect (view->canvas,
    x-(mini_box_size-2)/2, y-(bar_height/2+(mini_box_size-1)),
    mini_box_size-2, mini_box_size-2,
    widget->secondary_color[0],
    widget->secondary_color[1],  
    widget->secondary_color[2],
    65536);
  canvas_fill_rect (view->canvas,
    x-mini_box_size/2, y+(bar_height/2),
    mini_box_size, mini_box_size,
    widget->primary_color[0],
    widget->primary_color[1],  
    widget->primary_color[2],
    65536);
  canvas_fill_rect (view->canvas,
    x-(mini_box_size-2)/2, y+(bar_height/2)+1,
    mini_box_size-2, mini_box_size-2,
    widget->secondary_color[0],
    widget->secondary_color[1],  
    widget->secondary_color[2],
    65536);
}

static gboolean
event (View *view,
       gint x,
       gint y,
       gint pressure,
       gulong time,
       gpointer data)
{
  Slider *slider = (Slider*) view;
  Box *box = (Box*) view;

  gint old_value = slider->value;
  gint length = box_get_width (box);

  gint fraction = x;
  slider->value = MAX (0, MIN ((fraction / length), SLIDER_MAX));
  
  /* value can change in below func. */
  if (slider->value != old_value)
    {
      if (slider->value_changed_function != NULL)
        slider->value_changed_function (slider, slider->data);
    }
  
   
  return TRUE;
}


void
slider_set_value_changed_function (Slider *slider,
                                   void  (*value_changed_function) (Slider    *slider,
	                                                            gpointer  data),
	                           gpointer  data)
{
  slider->value_changed_function = value_changed_function;
  slider->data = data;
}

Slider *
slider_new (Canvas   *canvas)
{
  Slider *slider = g_malloc0 (sizeof (Slider));
  Widget *widget = (Widget*) slider;
  View   *view   = (View*) widget;

  widget_init (widget, canvas);
  
  view_set_event (view, event, slider);
  view_set_render (view, render, NULL);
 
  slider->old_value = -1; 
  
  slider->allocation = canvas_alloc (canvas, 250, 250);
  
  view_lookat (view, view->canvas,
     CANVAS_RECT (slider->allocation)->x, CANVAS_RECT (slider->allocation)->y, 65536);
  return slider;
}

void 
slider_set_value (Slider *slider,
		  guint   value)
{
  slider->value = value;
}

guint
slider_get_value (Slider *slider)
{
  return slider->value; 
}
