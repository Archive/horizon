/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _BRUSH_PREVIEW_H
#define _BRUSH_PREVIEW_H

typedef struct _BrushPreview BrushPreview;

#include "canvas/canvas.h"
#include "tools/tool_with_brush.h"


BrushPreview *
brush_preview_new (Canvas *canvas);

void
brush_preview_set_tool_setting (BrushPreview *brush_preview,
                                ToolSetting  *tool_setting);

#endif
