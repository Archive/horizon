/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include <string.h>
#include "canvas/canvas.h"
#include "toolkit/box.h"
//#include "view.h"
#include "tools/tool_library.h"
#include "config.h"
#include "blit.h"

#define H_LABEL_OFFSET  20


static gboolean
test_event (View *view,
            gint      x,
            gint      y,
            gint      pressure,
            gulong    time,
            gpointer  data)
{
  fprintf (stderr, "view=%p x=%i y=%i pressure=%i data=%p\n", view, x, y, pressure, data);
  return FALSE;
}

void
view_init (View   *view,
           Canvas *canvas)
{
  Box      *box = (Box*) view;
  box_init (box);

  view->canvas  = canvas;

  view->x = 0;
  view->y = 0;
  view->scale = 65536 * 1.0;

  view->last_x = -1000000;
  view->last_y = -1000000;
  view->last_scale = 65536 * 2;

  view->tile_width  = canvas_get_tile_width  (canvas);
  view->tile_height = canvas_get_tile_height (canvas);

  view->dirty = FALSE;

  view->rect = NULL;

  view_set_event (view, test_event, NULL);
  view_set_render (view, NULL, NULL);

  box->type = BOX_VIEW;
}


View *
view_new (Canvas *canvas)
{
  View *view = g_malloc0 (sizeof (View));

  view_init (view, canvas);
  
  return view;
}


/** this is the code to maintain the globally shared accumulated bounding box
 *  of the region that is reblitted upon a views_flush
 */
static gint dx0=65536;
static gint dy0=65536;
static gint dx1=0;
static gint dy1=0;

static void
views_clear_dirty (void)
{
  dx0=65536;
  dy0=65536;
  dx1=0;
  dy1=0;
}

void
views_flush (void)
{
  SDL_Surface *screen = SDL_GetVideoSurface ();
  if (dx0<dx1 && dy0<dy1)
    {
      if (dx0<0)
        dx0=0;
      if (dy0<0)
        dy0=0;
      if (dx1>=screen->w) 
        dx1=screen->w;
      if (dy1>=screen->h)
        dy1=screen->h;

      SDL_UpdateRect (screen, dx0, dy0, dx1-dx0, dy1-dy0);
    }
  views_clear_dirty ();
}

/**************/

void
view_set_x (View  *view,
            glong  x)
{
  view->x=x;
}

void
view_set_y (View  *view,
            glong  y)
{
  view->y=y;
}

glong
view_get_x (View *view)
{
  return view->x;
}

glong
view_get_y (View *view)
{
  return view->y;
}


static void pre_zoom (View *view)
{
  Box *box = (Box*)view;
  gint x, y;
  x = view->x;
  y = view->y;
  x = x + (box_get_width (box) / 2) * 65536 / view->scale;
  y = y + (box_get_height (box) / 2) * 65536 / view->scale;
  view->x = x;
  view->y = y;
}

static void post_zoom (View *view)
{
  Box *box = (Box*)view;
  gint x, y;
  x = view->x;
  y = view->y;
  x = x - (box_get_width (box) / 2) * 65536 / view->scale;
  y = y - (box_get_height (box) / 2) * 65536 / view->scale;
  view->x = x;
  view->y = y;
}



void
view_set_center_x (View  *view,
                   glong  x)
{
  Box *box = (Box*)view;
  view->x=x - box_get_width (box)/2 * 65536 / view->scale;
}

glong
view_get_center_x (View *view)
{
  Box *box = (Box*)view;
  return view->x + box_get_width (box)/2 * 65536 / view->scale;
}

glong
view_get_center_y (View *view)
{
  Box *box = (Box*)view;
  return view->y + box_get_height (box)/2 * 65536 / view->scale;
}

void
view_set_center_y (View  *view,
                   glong  y)
{
  Box *box = (Box*)view;
  view->y=y - box_get_height (box)/2 * 65536 / view->scale;
}

void
view_set_center_scale (View *view,
                       gint  scale)
{
  pre_zoom (view);
  view->scale=scale;
  post_zoom (view);
}

void
view_set_scale (View *view,
                gint  scale)
{
  view->scale=scale;
}

gint
view_get_scale (View *view)
{
  return view->scale;
}

void
view_lookat (View   *view,
             Canvas *canvas,
             gint    x,
             gint    y,
             gint    scale)
{
  view->canvas = canvas;
  view->x = x;
  view->y = y;
  view->scale = scale;
}

void
view_set_event (View      *view,
                gboolean (*event) (View     *view,
                                   gint      x,
                                   gint      y,
                                   gint      pressure,
                                   gulong    time,
                                   gpointer  data),
                gpointer data)
{
  view->event = event;
  view->event_data = data;
}

void
view_set_render (View    *view,
                 void   (*render) (View     *view,
                                   gpointer  data),
                 gpointer data)
{
  view->render = render;
  view->render_data = data;
}

void
view_destroy (View *view)
{
  if (view->rect)
    g_free (view->rect);
  g_free (view);
}

#include "SDL.h"

static void
dim_tile (ViewRect    *rect,
          SDL_Surface *surface,
          gint         tx0,
          gint         ty0,
          gint         tx1,
          gint         ty1,
          gint         rx0,
          gint         ry0,
          gint         rx1,
          gint         ry1)
{
  guchar *data = surface->pixels;
 
  gint y;

  data += ty0 * surface->pitch + tx0 * 2;

  for (y=ty0; y<ty1; y++)
    {
      gint x;
      guchar *d = data;
      d += surface->pitch * (y-ty0);

      for (x=tx0; x<tx1; x++)
        {
          guint val = *(unsigned short *)d;
          guint red   = ((val >> 11) & 0x1f) << 11;
          guint green = ((val >> 5)  & 0x3f) << 10;
          guint blue  = ((val >> 0)  & 0x1f) << 11;

          if (x > rx0 &&
              y > ry0 &&
              x < rx1 &&
              y < ry1)
            {
              red   = (red   * (65536-rect->color[3]) + rect->color[0] * rect->color[3]) / 65536;
              green = (green * (65536-rect->color[3]) + rect->color[1] * rect->color[3]) / 65536;
              blue  = (blue  * (65536-rect->color[3]) + rect->color[2] * rect->color[3]) / 65536;
            }
          else if (x > rx0-rect->border_px &&
                   y > ry0-rect->border_px &&
                   x < rx1+rect->border_px &&
                   y < ry1+rect->border_px)
            {
              red   = (red   * (65536-rect->border_color[3]) + rect->border_color[0] * rect->border_color[3]) / 65536;
              green = (green * (65536-rect->border_color[3]) + rect->border_color[1] * rect->border_color[3]) / 65536;
              blue  = (blue  * (65536-rect->border_color[3]) + rect->border_color[2] * rect->border_color[3]) / 65536;
            }
          else
            {
              red   = (red   * (65536-rect->surround_color[3]) + rect->surround_color[0] * rect->surround_color[3]) / 65536;
              green = (green * (65536-rect->surround_color[3]) + rect->surround_color[1] * rect->surround_color[3]) / 65536;
              blue  = (blue  * (65536-rect->surround_color[3]) + rect->surround_color[2] * rect->surround_color[3]) / 65536;
            }

          red >>= 11;
          green >>= 10;
          blue >>= 11;
         
          *(unsigned short *)d  = (red << 11) + (green << 5) + blue;
          d+=2;
        }
    }
}

/* scaling sdl view blitter that uses varying level of detail
 * for tiles */
static void
view_render_sdl (View        *view,
                 SDL_Surface *surface,
                 gint         x0,
                 gint         y0,
                 gint         x1,
                 gint         y1,
                 gint         mx0,
                 gint         my0,
                 gint         mx1,
                 gint         my1,
                 gboolean     refresh)
{
  gint scaled_tile_width;
  gint scaled_tile_height;

  gint offsetx, offsety;
  gint x, y;

  glong sx, sy;
  gint sscale;

  sx = view->x;
  sy = view->y;
  sscale = view->scale;

  {
    while (sscale < 65536 * MIN_TILE_SCALE)
      {
        sx = (sx + (TILE_ZOOM_START*2))/2;
        sy = (sy + (TILE_ZOOM_START*2))/2;
        sscale = sscale * 2;
      }
    //g_printf ("req: %f used: %f\n", view->scale / 65536.0, sscale/65536.0);
  }

  scaled_tile_width  = view->tile_width * sscale / 65536;
  scaled_tile_height = view->tile_height * sscale / 65536;

  offsetx = tile_local (sx) * sscale / 65536;
  offsety = tile_local (sy) * sscale / 65536;

  for (y=0;
      (y-1) * scaled_tile_height < y1-y0;
      y++)
    for (x=0;
        (x-1) * scaled_tile_width < x1-x0;
        x++)
      {
        Tile *tile = NULL;
        if (x0 + (x+1) * scaled_tile_width  - offsetx < mx0 ||
            y0 + (y+1) * scaled_tile_height - offsety < my0 ||
            y0 + (y)   * scaled_tile_height - offsety > my1 ||
            x0 + (x)   * scaled_tile_width  - offsetx > mx1)
          {
            continue;
          }

        {
          gint tile_x = (tile_tile (sx) + x) & 0xffffff;
          gint tile_y = (tile_tile (sy) + y) & 0xffffff;

          if (!refresh ||
              (refresh && canvas_is_dirty (view->canvas, tile_x, tile_y)))
            tile = canvas_get_tile (view->canvas, tile_x, tile_y);
        }
        
        if (tile )
          {
            gint bx = x0 + x * scaled_tile_width  - offsetx;
            gint by = y0 + y * scaled_tile_height - offsety;

            g_object_ref (G_OBJECT (tile));

            /* update dirty region of screen */
            if (bx < dx0)
              dx0=bx;
            if (bx +  scaled_tile_width >= dx1 )
              dx1=bx + scaled_tile_width;
            if (by < dy0)
              dy0=by;
            if (by +  scaled_tile_height >= dy1)
              dy1=by + scaled_tile_height; 

            tile_blit (tile, surface, bx,by, sscale, mx0, my0, mx1-mx0, my1-my0);
            g_object_unref (G_OBJECT (tile));

            /* draw rectangle */
            if (view->rect != NULL)
              {
                gint rx0=x0+(view->rect->x - view->x) * (view->scale/4) / 16384;
                gint ry0=y0+(view->rect->y - view->y) * (view->scale/4) / 16384;
                gint rx1=rx0 + view->rect->width * (view->scale/4) / 16384;
                gint ry1=ry0 + view->rect->height * (view->scale/4) / 16384;

                gint tx0, ty0, tx1, ty1;

                tx0 = bx;
                ty0 = by;
                tx1 = bx + scaled_tile_width;
                ty1 = by + scaled_tile_height;

                tx0=CLAMP(tx0,mx0,mx1);
                ty0=CLAMP(ty0,my0,my1);
                tx1=CLAMP(tx1,mx0,mx1);
                ty1=CLAMP(ty1,my0,my1);

                dim_tile (view->rect, surface, tx0, ty0, tx1, ty1, rx0, ry0, rx1, ry1);
              }
          }
      }
}

/* FIXME: do not issue refreshes when using pan tool and no move has occured
 * (it seems to lead to a full repaint)
 */

void
view_refresh (View *view,
              gint  x0,
              gint  y0,
              gint  x1,
              gint  y1)
{
  Box         *box     = (Box*)view;
  SDL_Surface *surface = SDL_GetVideoSurface ();
  SDL_Surface *vsurface;
  gint deltax;
  gint deltay;

  gint width = box_get_width (box);
  gint height = box_get_height (box);

  deltax = (view->last_x * view->scale) / 65536 - 
           (view->x      * view->scale) / 65536;
  deltay = (view->last_y * view->scale) / 65536 - 
           (view->y      * view->scale) / 65536;
    
  if ( (deltax == 0 &&   /* for painting operations */
      deltay == 0 &&
      view->scale == view->last_scale) ||
      view->dirty)
    {
       gboolean refresh;
       if (view->dirty == TRUE)
	 refresh = FALSE;
       else
	 refresh = TRUE;
       
       view_render_sdl (view, surface, x0, y0, x1,y1, 
                            x0, y0, x1, y1, refresh);
       view->last_x = view->x;
       view->last_y = view->y;
       view->last_scale = view->scale;

       view->dirty = FALSE;
       return;
    }

  /* note another scale check is performed in the real blitter */
  if (view->scale != view->last_scale ||
      deltax >  width ||
      deltax < -width ||
      deltay >  height ||
      deltay < -height)
    {
       view_render_sdl (view, surface, x0, y0, x1,y1, 
                            x0, y0, x1, y1, FALSE);
       view->last_x = view->x;
       view->last_y = view->y;
       view->last_scale = view->scale;
       return;
    }

  vsurface = SDL_CreateRGBSurfaceFrom (
      surface->pixels + y0 * surface->pitch + x0 * 2,
      width, height, 16, surface->pitch,
      surface->format->Rmask, surface->format->Gmask,
      surface->format->Bmask, surface->format->Amask);

  if (deltax >= 0 &&
      deltay >= 0)
    {
      SDL_Rect src = {0,0, width - deltax, height - deltay};
      SDL_Rect dst = {deltax, deltay, 0,0};
      SDL_BlitSurface (vsurface, &src, vsurface, &dst);

      view_render_sdl (view, surface, x0, y0, x1, y1,
                           x0, y0, x0+deltax, y0+height, FALSE);
      view_render_sdl (view, surface, x0, y0, x1, y1,
                           x0, y0, x0+width, y0+deltay, FALSE);
    }
  else if (deltax <=0 &&
           deltay >=0)
    {
      SDL_Rect src = {-deltax, 0, width + deltax, height - deltay};
      SDL_Rect dst = {0, deltay, 0,0};
      SDL_BlitSurface (vsurface, &src, vsurface, &dst);

      view_render_sdl (view, surface, x0, y0, x1, y1,
                           x0+width + deltax, y0, x0+width, y0+height, FALSE);
      view_render_sdl (view, surface, x0,y0,x1,y1,
                           x0, y0,x0+width, y0+deltay, FALSE);
    }
  else if (deltax >=0 &&
           deltay <=0)
    {
      SDL_Rect src = {0, -deltay, width-deltax, height + deltay};
      SDL_Rect dst = {deltax, 0, 0,0};
      SDL_BlitSurface (vsurface, &src, vsurface, &dst);

      view_render_sdl (view, surface, x0,y0,x1,y1,
                           x0,y0,x0+deltax,y0+height, FALSE);
      view_render_sdl (view, surface, x0,y0,x1,y1,
                           x0, y0+height+deltay, x0+width, y0+height, FALSE);
    }
  else if (deltax <=0 &&
           deltay <=0)
    {
      SDL_Rect src = {-deltax, -deltay, width+deltax, height + deltay};
      SDL_Rect dst = {0, 0, 0,0};
      SDL_BlitSurface (vsurface, &src, vsurface, &dst);

      view_render_sdl (view, surface, x0,y0,x1,y1,
                           x0+width+deltax,y0,x0+width,y0+height, FALSE);
      view_render_sdl (view, surface, x0,y0,x1,y1,
                           x0, y0+height+deltay, x0+width, y0+height, FALSE);
    }
  SDL_FreeSurface (vsurface);
  //SDL_UpdateRect (surface, x0,y0,x1,y1);
  view->last_x = view->x;
  view->last_y = view->y;
  view->last_scale = view->scale;
  
  view->dirty = FALSE;
}

Canvas *
view_get_canvas (View *view)
{
  return view->canvas;
}

void
view_set_dirty (View *view)
{
  view->dirty = TRUE; 
}

gboolean
view_is_dirty (View *view)
{
  return view->dirty;
}

static void
view_add_rect (View *view)
{
  if (view->rect != NULL)
    return;
  view->rect = g_malloc0 (sizeof (ViewRect));
  view->rect->x = 65536;
  view->rect->y = 65536;
  view->rect->width = 1000;
  view->rect->height = 1000;

  view->rect->border_px = 4;
  view->rect->color[0] = 65536*0.0;
  view->rect->color[1] = 65536*0.0;
  view->rect->color[2] = 65536*0.5;
  view->rect->color[3] = 65536*0.2;
  view->rect->border_color[0] = 65535*1.0;
  view->rect->border_color[1] = 65535*1.0;
  view->rect->border_color[2] = 65535*1.0;
  view->rect->border_color[3] = 65535*0.5;
  view->rect->surround_color[0] = 65536*0.0;
  view->rect->surround_color[1] = 65536*0.0;
  view->rect->surround_color[2] = 65536*0.0;
  view->rect->surround_color[3] = 65536*0.3;
}

void
view_set_rect_visible (View    *view,
                       gboolean visible)
{
  if (visible)
    {
      view_add_rect (view);
    }
  else
    {
      if (view->rect != NULL)
        g_free (view->rect);
      view->rect = NULL;
    }
  view->dirty=TRUE;
}

void
view_set_rect_x (View *view,
                 gint  x)
{
  if (!view->rect)
    view_add_rect (view);
  view->rect->x = x;
  view->dirty=TRUE;
}

void
view_set_rect_y (View *view,
                 gint  y)
{
  if (!view->rect)
    view_add_rect (view);
  view->rect->y = y;
  view->dirty=TRUE;
}

void
view_set_rect_width (View *view,
                     gint  width)
{
  if (!view->rect)
    view_add_rect (view);
  view->rect->width = width;
  view->dirty=TRUE;
}

void
view_set_rect_height (View *view,
                      gint  height)
{
  if (!view->rect)
    view_add_rect (view);
  view->rect->height = height;
  view->dirty=TRUE;
}

void
view_set_rect_border_px (View *view,
                         gint  border_px)
{
  if (!view->rect)
    view_add_rect (view);
  view->rect->border_px = border_px;
  view->dirty=TRUE;
}

void
view_set_rect_color (View *view,
                     gint  color[4])
{
  gint i;
  if (!view->rect)
    view_add_rect (view);
  for (i=0; i<4; i++)
    view->rect->color[i] = color[i];
  view->dirty=TRUE;
}

void
view_set_rect_border_color (View *view,
                            gint  color[4])
{
  gint i;
  if (!view->rect)
    view_add_rect (view);
  for (i=0; i<4; i++)
    view->rect->border_color[i] = color[i];
  view->dirty=TRUE;
}

void
view_set_rect_surround_color (View *view,
                              gint  color[4])
{
  gint i;
  if (!view->rect)
    view_add_rect (view);
  for (i=0; i<4; i++)
    view->rect->surround_color[i] = color[i];
  view->dirty=TRUE;
}

