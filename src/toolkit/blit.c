/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>
                             Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include "SDL.h"

#include "canvas/tile.h"

/*  blitting function to blit a rgbP u8  tile to a 16bpp SDL Surface
 *  should work for all tile sizes, if the mask coordinates passed in
 *  are all 0, the entire surface is blittable.
 */
static void
tile_blit_native (Tile        *tile,
                  SDL_Surface *surface,
                  gint         dest_x,
                  gint         dest_y,
                  gint         mask_x,
                  gint         mask_y,
                  gint         mask_width,
                  gint         mask_height)
{
  gint tile_width   = tile_get_width  (tile);
  gint tile_height  = tile_get_height (tile);
  
  gint y;
  
  gint start_x = MAX (dest_x, mask_x);
  gint end_x = MIN (dest_x + tile_width, mask_x + mask_width);
  gint row_length = end_x - start_x;
  gint start_y = MAX (dest_y, mask_y);
  gint end_y = MIN (dest_y + tile_height, mask_y + mask_height);

  gint src_row_increment = tile_width * 4;
  guchar *src_row_start = tile_get_data (tile) +
    ((start_y - dest_y) * src_row_increment) + 
    (start_x - dest_x) * 4;  
  gint dst_row_increment = surface->pitch;
  guchar *dst_row_start = surface->pixels +
    start_y * dst_row_increment +
    start_x * 2;
  
  for (y = start_y; y < end_y; y++)
    {
      guchar *src = src_row_start;
      guchar *dst;
      guchar *dst_row_end = dst_row_start + row_length * 2;
      for (dst = dst_row_start; dst < dst_row_end; dst += 2)
        {
/*        For debug purposes for poor people without efence. :-) */
/*	  if ((src < tile_get_data (tile) || (src >= ((guchar *)tile_get_data (tile)) + tile_width * tile_height * 4)))
              {
		g_print ("BEEP!\n");
		*((gint*) 0) = 0;
	      }
	  if ((dst < ((guchar *)surface->pixels)) || (dst >= ((guchar *)surface->pixels) + SCREEN_WIDTH*SCREEN_HEIGHT*2))
              {
		g_print ("BEEP2!\n"); 
		*((gint*) 0) = 0;
		return;
	      }*/
     
          #define red   (src[0] >> 3)
          #define green (src[1] >> 2)
          #define blue  (src[2] >> 3)
          *((unsigned short*) dst) = (red << 11) + (green << 5) + blue;
          #undef red
          #undef green
          #undef blue
	  
	  src += 4;
	}
      src_row_start += src_row_increment;
      dst_row_start += dst_row_increment;
    }
}

/* one possible quality improvement is to use an accumulation scanline, in parallel
 * with the accumulation scanline a array with a sample count per location */
static void
tile_blit_decimate (Tile        *tile,
                    SDL_Surface *surface,
                    gint         dest_x,
                    gint         dest_y,
                    gint         scale,
                    gint         mask_x,
                    gint         mask_y,
                    gint         mask_width,
                    gint         mask_height)
{
  gint tile_width   = tile_get_width  (tile);
  gint tile_height  = tile_get_height (tile);
  
  gint y;
  
  /* below is a hack. 65536 * 65536 / scale wouldn't work on 32bit value. */
  guint dst_pixel_increment = 16384 * scale / 65536 * 4;
  guint src_pixel_increment = (256 * 65536) / scale * 65536;

  gint start_x = MAX (dest_x, mask_x);
  gint end_x = MIN (dest_x + tile_width * dst_pixel_increment / 65536, mask_x + mask_width);
  gint row_length = end_x - start_x;
  gint start_y = MAX (dest_y, mask_y);
  gint end_y = MIN (dest_y + tile_height * dst_pixel_increment / 65536, mask_y + mask_height);

  gint src_row_increment = tile_width * 4;
  guchar *src_row_start = tile_get_data (tile) +
    ((((start_y - dest_y) * src_pixel_increment) >> 24) * src_row_increment) + 
    (((start_x - dest_x) * src_pixel_increment) >> 24) * 4;
  gint dst_row_increment = surface->pitch;
  guchar *dst_row_start = surface->pixels +
    start_y * dst_row_increment +
    start_x * 2;
  
  gint x_accu_start = (((start_x - dest_x) * src_pixel_increment) & 0xffffff);
  gint y_accu = (((start_y - dest_y) * src_pixel_increment) & 0xffffff);
  
  for (y = start_y; y < end_y; y++)
    {
      guchar *src = src_row_start;
      guchar *dst;
      guchar *dst_row_end = dst_row_start + row_length * 2;;
      gint x_accu = x_accu_start;

      for (dst = dst_row_start; dst < dst_row_end; dst += 2)
        {
/*        For debug purposes for poor people without efence.*/
/*	  if ((src < tile_get_data (tile) || (src >= ((guchar *)tile_get_data (tile)) + tile_width * tile_height * 4)))
              {
		g_print ("BEEP!\n");
		*((gint*) 0) = 0;
	      }
	  if ((dst < ((guchar *)surface->pixels)) || (dst >= ((guchar *)surface->pixels) + SCREEN_WIDTH*SCREEN_HEIGHT*2))
              {
		g_print ("BEEP2!\n"); 
		*((gint*) 0) = 0;
		return;
	      }*/
     
          #define red   (src[0] >> 3)
          #define green (src[1] >> 2)
          #define blue  (src[2] >> 3)
          *((unsigned short*) dst) = (red << 11) + (green << 5) + blue;
          #undef red
          #undef green
          #undef blue
	  
	  x_accu += src_pixel_increment;
	  src += 4*(x_accu >> 24);
	  x_accu &= 0xffffff;
	}
      y_accu += src_pixel_increment;
      src_row_start += src_row_increment * (y_accu >> 24);
      y_accu &= 0xffffff;
      dst_row_start += dst_row_increment;
    }
}

static void
tile_blit_interpolate (Tile        *tile,
                       SDL_Surface *surface,
                       gint         dest_x,
                       gint         dest_y,
                       gint         scale,
                       gint         mask_x,
                       gint         mask_y,
                       gint         mask_width,
                       gint         mask_height)
{
  /* Until we get a real interpolating function, use decimate. */
  tile_blit_decimate (tile, surface, dest_x, dest_y, scale, mask_x, mask_y, mask_width, mask_height);
}

/*
 * scale is a 16.16 value
 *
 */
void
tile_blit (Tile        *tile,
           SDL_Surface *surface,
           gint         dest_x,
           gint         dest_y,
           gint         scale,
           gint         mask_x,
           gint         mask_y,
           gint         mask_width,
           gint         mask_height)
{
  gint tile_width  = tile_get_width  (tile);
  gint tile_height = tile_get_height (tile);

  if (scale == 0)
    scale = 65536;

  if (mask_x == 0 &&
      mask_y == 0 &&
      mask_width == 0 &&
      mask_height == 0)
    {
      mask_width  = surface->w;
      mask_height = surface->h;
    }

  if (dest_x + tile_width < mask_x + mask_width &&
      dest_x + tile_width >= surface->w)
    {
       mask_width = surface->w - mask_x;
    }

  if (dest_y + tile_height < mask_y + mask_width &&
      dest_y + tile_height >= surface->h)
    {
      mask_height = surface->h - mask_y;
    }

  if (scale > 65536-16 &&
      scale < 65536+16)
    {
      tile_blit_native (tile, surface,
        dest_x, dest_y,
        mask_x, mask_y, mask_width, mask_height);
    }
  else if (scale < 65536)
    {
      tile_blit_decimate (tile, surface,
        dest_x, dest_y, scale,
        mask_x, mask_y, mask_width, mask_height);
    }
  else if (scale > 65536)
    {
      tile_blit_interpolate (tile, surface,
        dest_x, dest_y, scale,
        mask_x, mask_y, mask_width, mask_height);
    }
}
