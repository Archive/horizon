/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include "canvas/canvas.h"
#include "toolkit/view.h"
#include "brush_preview.h"
#include "tools/tool_library.h"
#include "vectors/path.h"
#include "config.h"

struct _BrushPreview
{
  View         view;
  Allocation  *allocation;
  ToolSetting *tool_setting;
};

gboolean
brush_preview_event (View     *view,
                     gint      x,
                     gint      y,
                     gint      pressure,
                     gulong    time,
                     gpointer  data)
{
  return FALSE;
}

static void
render (View     *view,
        gpointer  data)
{
  Box *box                    = (Box*) view;
  BrushPreview *brush_preview = (BrushPreview*)view;
  ToolSetting *ts = brush_preview->tool_setting;
  gint opacity;

  gint x = CANVAS_RECT (brush_preview->allocation)->x + box_get_width (box)/2;
  gint y = CANVAS_RECT (brush_preview->allocation)->y + box_get_height (box)/2;

  opacity = ts->opacity * MAX_OPACITY / 65536;

  if (!view_is_dirty (view) || !ts)
    return;

  canvas_fill_rect (view->canvas, x-64, y-100, 128, 200, 65535,65535,65535,65535);
  canvas_fill_rect (view->canvas, x, y-100, 128, 200, 0,0,0,65535);

  {
    Path *line = path_new ();

    path_move_to (line, x * SPP, y * SPP);
    path_rel_line_to (line, 0, 128 * SPP);
    path_stroke_tool (line, view->canvas, get_current_tool()); /* this is a hack,
                                                           but passing a full
                                                           tool to path_stroke
                                                           is better than just
                                                           settings.*/
    path_destroy (line);
  }
  canvas_fill_rect (view->canvas, x-64, y-64, 128, 20, ts->red, ts->green, ts->blue, 65536);
}

void
brush_preview_set_tool_setting (BrushPreview *brush_preview,
                                ToolSetting  *tool_setting)
{
  brush_preview->tool_setting = tool_setting;
}

BrushPreview *
brush_preview_new (Canvas   *canvas)
{
  BrushPreview *brush_preview = g_malloc0 (sizeof (BrushPreview));
  View  *view  = (View*) brush_preview;
  
  view_init (view, canvas);
  view_set_event (view, brush_preview_event, NULL);

  brush_preview->allocation = canvas_alloc (canvas,
    canvas_get_tile_width (canvas), canvas_get_tile_height (canvas));

  view_set_x (view, CANVAS_RECT (brush_preview->allocation)->x);
  view_set_y (view, CANVAS_RECT (brush_preview->allocation)->y);
  view_set_render (view, render, NULL);
  view_set_scale (view, 65536);

  return brush_preview;
}

