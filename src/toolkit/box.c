/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include "string.h"
#include "box.h"
#include "view.h"

void
box_init (Box *box)
{
  box->type = BOX_PRIMITVE;
  box_set_ratio (box, 65536*0.5);
  box->children = 0;
}

Box *
box_new (void)
{
  Box *box;

  box = g_malloc0 (sizeof (Box));
  box_init (box);
  return box;
}

void
box_destroy (Box *box)
{
  if (box->child[0])
      box_destroy (box->child[0]);
  if (box->child[1])
      box_destroy (box->child[1]);
  /* FIXME: destruction of derived types doesn't work properly yet */

  box_set_id (box, NULL);
  g_free (box);
}

static void
box_split (Box *box)
{
  if (box->children!=0)
    {
      g_error ("attempted to split non-leaf box");
      return;
    }
  box->children = 2;
  box->child[0] = NULL;
  box->child[1] = NULL;
}

int
box_get_ratio (Box *box)
{
  return box->ratio;
}

void
box_set_ratio (Box *box,
               int  ratio)
{
  box->ratio = ratio;
  box_recalc_sizes (box, box->width, box->height);
}

void
box_recalc_sizes (Box  *box,
                  gint  width,
                  gint  height)
{
    if (!box)
      return;
    box->width  = width;
    box->height = height;
    if (box->child[0]==NULL &&
        box->child[1]==NULL)
      {
	if (box->type == BOX_VIEW)
	  view_set_dirty ((View*)box);
      }
    else
      {
        if (box->vertical)
          {
            gint a = height * box->ratio/65536;
            gint b = height - a;

            box_recalc_sizes (box->child[0], width, a);
            box_recalc_sizes (box->child[1], width, b);
          }
        else
          {
            gint a = width * box->ratio/65536;
            gint b = width - a;

            box_recalc_sizes (box->child[0], a, height);
            box_recalc_sizes (box->child[1], b, height);
          }
      }
}

void
box_hsplit (Box *box)
{
  box_split (box);
}

void
box_vsplit (Box *box)
{
  box_split (box);
  box->vertical = TRUE;
}

static void
box_render_internal (Box         *box,
                     gint         x0,
                     gint         y0)
{
  if (!box)
    return;

  if (box->children)
    {
      if (box->vertical)
        {
          gint offset = box->height * box->ratio/65536;

          if (box->ratio > 0)
            {
              box_render_internal (box->child[0], x0, y0);
            }
          if (box->ratio < 65536)
            {
              box_render_internal (box->child[1], x0, y0 + offset);
            }
        }
      else
        {
          gint offset = box->width * box->ratio/65536;

          if (box->ratio > 0)
            {
              box_render_internal (box->child[0], x0, y0);
            }
          if (box->ratio < 65536)
            {
              box_render_internal (box->child[1], x0 + offset, y0);
            }
        }
    }
  else
    {
      if (box->type == BOX_VIEW)
        {
          View *view = (View*)box;
          if (view->render)
              view->render (view, view->render_data);
          view_refresh (view, x0, y0, x0 + box->width, y0 + box->height);
        }
      else
        {
          //g_warning ("trying to render box without view");
        }
    }
}

void
box_render (Box *root)
{
  box_render_internal (root, 0, 0);
}


static Box *
box_resolve (Box  *box,
             gint  x,
             gint  y,
             gint *ret_x,
             gint *ret_y)
{

  if (!box)
    return NULL;

  if (box->child[0] == NULL &&
      box->child[1] == NULL)
    {
      if (ret_x)
        *ret_x = x;
      if (ret_y)
        *ret_y = y;
      return box;
    }

  if (box->vertical)
    {
      if (y < box->height * box->ratio)
        return box_resolve (box->child[0],
                            x, y,
                            ret_x, ret_y);
      else if (y>= box->height * box->ratio)
        return box_resolve (box->child[1],
                            x, y - box->height * box->ratio,
                            ret_x, ret_y);
      else
        {
          if (ret_x)
            *ret_x = 0;
          if (ret_y)
            *ret_y = 0;
          return NULL;
        }
    }
  else
    {
      if (x < box->width*box->ratio)
        return box_resolve (box->child[0],
                            x, y,
                            ret_x, ret_y);
      else if (x >= box->width*box->ratio)
        return box_resolve (box->child[1],
                            x - box->width * box->ratio, y,
                            ret_x,ret_y);
      else
        {
          *ret_x = 0;
          *ret_y = 0;
          return NULL;
        }
    }
  return NULL;
}

/* simplistic stylus grab with 200ms timeout */

static Box *prev_target = NULL;
static gulong prev_time = 0;

gboolean
box_event (Box    *root,
           gint    x,
           gint    y,
           gint    pressure,
           gulong  time)
{
  gint lx, ly;
  Box *target = box_resolve (root, x * root->width, y * root->height, &lx, &ly);

  if (time - prev_time > 200)
    prev_target = target;
  prev_time = time;

  if (target &&
      target == prev_target &&
      target->type == BOX_VIEW)
    {
      /* FIXME: move more of this logic into view */
      View *view = (View*)target;
      return view->event (view, lx, ly, pressure, time, view->event_data);
    }
  return FALSE;
}

Box *
bin_new (void)
{
  Box *box = box_new ();
  box_hsplit (box);
  box->ratio = 65536;
  box->type = BOX_BIN;
  return box;
}

Box *
hsplit_new (void)
{
  Box *box = box_new ();
  box_hsplit (box);
  box->type = BOX_HSPLIT;
  return box;
}

Box *
vsplit_new (void)
{
  Box *box = box_new ();
  box_vsplit (box);
  box->type = BOX_VSPLIT;
  return box;
}

static void box_set_ratios_even (Box *box)
{
  gint i;
  gint children = box->children;
  Box *iter = box; 
  for (i=0; i< children-1; i++)
    {
      box_set_ratio (iter, 65536-65536 * (children-i-1) / (children-i));
      iter = iter->child[1];
    }
}

void
box_set_ratios (Box  *box,
                gint  sizes[])
{
  gint i;
  gint children = box->children;
  Box *iter = box;
  
  for (i=0; i< children-1; i++)
    {
      gint j;
      gint sum = 0;
      for (j=i; j< children; j++)
        {
          sum+=sizes[j];
        }
      box_set_ratio (iter, 65536*sizes[i]/sum);
      iter = iter->child[1];
    }
}


Box *
vbox_new (gint children)
{
  Box *box = NULL; 
  Box *iter; 
  gint i;

  if (children==1)
    {
      box = bin_new ();
      box->type = BOX_VBOX;
      return box;
    }

  box = vsplit_new ();
  iter = box;

  for (i=1;i<children-1;i++)
    {
      Box *vsplit = vsplit_new ();
      box_set_child (iter, 1, vsplit);
      iter = vsplit;
    }
  
  box->type = BOX_VBOX;
  box->children = children;

  box_set_ratios_even (box);
  return box;
}

Box *
hbox_new (gint children)
{
  Box *box = NULL; 
  Box *iter; 
  gint i;

  if (children==1)
    {
      box = bin_new ();
      box->type = BOX_HBOX;
      return box;
    }

  box = hsplit_new ();
  iter = box;

  for (i=1;i<children-1;i++)
    {
      Box *hsplit = hsplit_new ();
      box_set_child (iter, 1, hsplit);
      iter = hsplit;
    }
  
  box->type = BOX_HBOX;
  box->children = children;

  box_set_ratios_even (box);
  return box;
}

Box *
grid_new (gint cols,
          gint rows)
{
  Box *vbox;
  gint row;
  if (cols==0 || rows==0)
    {
      g_error ("cols=0 row=0");
    }
  vbox = vbox_new (rows);
  for (row=0; row<rows; row++)
    {
      box_set_child (vbox, row, hbox_new (cols));
    }
  vbox->type = BOX_GRID;
  return vbox;
}


gint
box_get_width (Box *box)
{
  return box->width;
}

gint
box_get_height (Box *box)
{
  return box->height;
}

void
box_set_child (Box  *parent,
               gint  index,
               Box  *new_child)
{
  switch (parent->type)
    {
      case BOX_PRIMITVE:
        g_error ("setting child of a primitive box");
        break;
      case BOX_BIN:
      case BOX_VSPLIT:
      case BOX_HSPLIT:
        parent->child[index] = new_child;
        box_set_dirty (new_child);
        break;
      case BOX_VBOX:
      case BOX_HBOX:
        {
          gint i;
          Box *iter = parent;
          for (i=0; i<index; i++)
            {
              if (i < parent->children-2)
                iter = iter->child[1];
            }
          if (index == parent->children-1)
            {
              iter->child[1] = new_child;
            }
          else
            {
              iter->child[0] = new_child;
            }
          box_set_dirty (new_child);
        }
        break;
      case BOX_GRID:
        {
          gint i;
          Box *iter = parent;
          gint rows = parent->children;
          gint cols = parent->child[0]->children;
          gint row  = index / cols;
          gint col  = index % cols;

          for (i=0; i<row; i++)
            {
              if (i < rows-2)
                iter = iter->child[1];
            }
          if (row == rows-1)
            {
              box_set_child (iter->child[1], col, new_child);
            }
          else
            {
              box_set_child (iter->child[0], col, new_child);
            }
        }
        break;
      default:
        break;
    }
  box_recalc_sizes (parent, parent->width, parent->height);
}


Box *
box_get_child (Box *parent,
               gint index)
{
  switch (parent->type)
    {
      case BOX_PRIMITVE:
        g_error ("getting child of a primitive box");
        return NULL;
      case BOX_BIN:
      case BOX_VSPLIT:
      case BOX_HSPLIT:
        return parent->child[index];
      case BOX_VBOX:
      case BOX_HBOX:
        {
          gint i;
          Box *iter = parent;
          for (i=0; i<index; i++)
            {
              if (i < parent->children-2)
                iter = iter->child[1];
            }
          if (index == parent->children-1)
            {
              return iter->child[1];
            }
          else
            {
              return iter->child[0];
            }
        }
      case BOX_GRID:
        {
          gint i;
          Box *iter = parent;
          gint rows = parent->children;
          gint cols = parent->child[0]->children;
          gint row  = index / cols;
          gint col  = index % cols;

          for (i=0; i<row; i++)
            {
              if (i < rows-2)
                iter = iter->child[1];
            }
          if (row == rows-1)
            {
              return box_get_child (iter->child[1], col);
            }
          else
            {
              return box_get_child (iter->child[0], col);
            }
        }
        break;
      default:
        return NULL;
    }
  return NULL;
}

void
box_set_dirty (Box *box)
{
  if (!box)
    return;

  if (box->type == BOX_VIEW)
    {
       view_set_dirty ((View*)box);
       return;
    }
  else
    {
      box_set_dirty (box->child[0]);
      box_set_dirty (box->child[1]);
    }
}

const char *
box_get_id (Box *box)
{
  if (box)
    return box->id;
  return NULL;
}

void
box_set_id (Box        *box,
            const char *id)
{
  if (box->id)
    g_free (box->id);
  box->id = NULL;
  if (id)
    box->id = g_strdup (id);
}

static Box *
box_id_internal (Box        *box,
                 const char *id)
{
  if (!box)
    return NULL;
  
  if (box->id &&
      !strcmp (box->id, id))
    {
      return box;
    }
  
  {
    Box *ret;
    if (box->child[0] &&
      (ret=box_id_internal (box->child[0], id))!=NULL)
      return ret;
    if (box->child[1] && 
        (ret=box_id_internal (box->child[1], id))!=NULL)
      return ret;
  }
  return NULL;
}

Box *
box_id (Box        *box,
        const char *id)
{
  Box *ret = box_id_internal (box, id);

  if (!ret)
    g_warning ("box_id (%p, '%s') failed", box, id);
  return ret;
}
