/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include "canvas/canvas.h"
#include "view.h"
#include "widget.h"


void
widget_init (Widget *widget,
             Canvas *canvas)
{
  View   *view   = (View*) widget;
  view_init (view, canvas);

  widget->primary_color[0] = 65536 * 0.0;
  widget->primary_color[1] = 65536 * 0.0;
  widget->primary_color[2] = 65536 * 0.0;
  
  widget->secondary_color[0] = 65536 * 1.0;
  widget->secondary_color[1] = 65536 * 1.0;
  widget->secondary_color[2] = 65536 * 1.0;

  widget->background_color[0] = 65536 * 0.75;
  widget->background_color[1] = 65536 * 0.75;
  widget->background_color[2] = 65536 * 0.75;
}

Widget *
widget_new (Canvas   *canvas)
{
  Widget *widget = g_malloc0 (sizeof (Widget));

  widget_init (widget, canvas);
  return widget;
}

void
widget_set_primary_color (Widget *widget,
			  guint   red,
			  guint   green,
			  guint   blue)
{
  widget->primary_color[0] = red;
  widget->primary_color[1] = green;
  widget->primary_color[2] = blue;
  
  box_set_dirty ((Box*) widget);
}

void
widget_set_secondary_color (Widget *widget,
			    guint   red,
			    guint   green,
			    guint   blue)
{
  widget->secondary_color[0] = red;
  widget->secondary_color[1] = green;
  widget->secondary_color[2] = blue;
  
  box_set_dirty ((Box*) widget);
}

void
widget_set_background_color (Widget *widget,
			     guint   red,
			     guint   green,
			     guint   blue)
{
  widget->background_color[0] = red;
  widget->background_color[1] = green;
  widget->background_color[2] = blue;

  box_set_dirty ((Box*) widget);
}
