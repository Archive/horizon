/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Eero Tanskanen <yendor@nic.fi>
                       Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _SLIDER_H
#define _SLIDER_H

#include "canvas/canvas.h"
#include "toolkit/box.h"

typedef struct _Slider Slider;

Slider * slider_new                        (Canvas   *canvas);

void     slider_set_value_changed_function (Slider   *slider,
                                            void    (*value_changed_function) (Slider    *slider,
	                                                                       gpointer  data),
	                                    gpointer  data);

void     slider_set_value                  (Slider   *slider,
                                            guint     value);

guint    slider_get_value                  (Slider   *slider);



#endif
