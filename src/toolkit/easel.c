/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include "canvas/canvas.h"
#include "toolkit/view.h"
#include "easel.h"
#include "tools/tool_library.h"
#include "config.h"

struct _Easel
{
  View view;
};

gboolean
easel_event (View     *view,
             gint      x,
             gint      y,
             gint      pressure,
             gulong    time,
             gpointer  data)
{
  Easel *easel = (Easel*)view;
  tool_set_easel (get_current_tool (), easel);

  /* transform coordinates from 16.16 view pixel coordinates to
   * canvas coordinates with SPP (subpixel precision)
   */

  x = x / (view_get_scale (view) / SPP) + view_get_x (view) * SPP;
  y = y / (view_get_scale (view) / SPP) + view_get_y (view) * SPP;
  
  if (get_current_tool ()->uses_brush == TRUE)
    {
      ToolSetting *ts = &((ToolWithBrush*) get_current_tool())->ts;
      gint backup_radius = ts->radius;
      ts->radius = ts->radius * 256 / (view_get_scale (view)/256);
      tool_process_coordinate (get_current_tool (), x, y, pressure, time);
      ts->radius = backup_radius;
    }
  else 
    tool_process_coordinate (get_current_tool (), x, y, pressure, time);
  return TRUE;
}

Easel *
easel_new (Canvas   *canvas)
{
  Easel *easel = g_malloc0 (sizeof (Easel));
  View  *view  = (View*) easel;
  
  view_init (view, canvas);
  view_set_event (view, easel_event, NULL);
  
  return easel;
}

