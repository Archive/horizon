/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>
                       Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include "canvas/canvas.h"
#include "toolkit/view.h"
#include "navigator.h"
#include "config.h"

struct _Navigator
{
  View   view;
  Easel *easel;
  gint   ox;
  gint   oy;
  gint   prev_x;
  gint   prev_y;
  gulong prev_time;

  gint   zoom;
};

static gboolean event (View     *view,
                       gint      x,
                       gint      y,
                       gint      pressure,
                       gulong    time,
                       gpointer  data)
{
  Navigator *navigator = (Navigator*)view;

  if (!navigator->easel)
    return FALSE;
  
  if (time - navigator->prev_time > 500)
    {
      navigator->prev_x = x;
      navigator->prev_y = y;
      navigator->ox = view_get_x ((View*)navigator->easel);
      navigator->oy = view_get_y ((View*)navigator->easel);
    }
  else
    {
       view_set_x ((View*)navigator->easel, navigator->ox + (navigator->prev_x - x) /
          view_get_scale (view));
       view_set_y ((View*)navigator->easel, navigator->oy + (navigator->prev_y - y) /
          view_get_scale (view));
    }
  navigator->prev_time = time;
  return TRUE;
}

static void render (View     *view,
                    gpointer  data)
{
  Navigator *navigator = (Navigator*)view;
  gint       scale;

  if (!navigator->easel)
    return;
  scale  = view_get_scale ((View*)navigator->easel);
  
  /* scale the thumbnail view */
  scale = (scale/256) * (navigator->zoom/256);
  if (scale < ZOOM_MIN)
    scale = ZOOM_MIN;

  view_set_center_x (view, view_get_center_x ((View*)navigator->easel));
  view_set_center_y (view, view_get_center_y ((View*)navigator->easel));
  view_set_center_scale (view, scale);

  view_set_rect_visible (view, TRUE);
  view_set_rect_x (view, view_get_x ((View*)navigator->easel));
  view_set_rect_y (view, view_get_y ((View*)navigator->easel));
  view_set_rect_width (view,
     box_get_width ((Box*)(View*)navigator->easel) * 65536 /
     view_get_scale ((View*)navigator->easel) );
  view_set_rect_height (view,
     box_get_height ((Box*)(View*)navigator->easel) * 65536 /
     view_get_scale ((View*)navigator->easel) );
  view_set_rect_border_px (view, 1);
}

Navigator *
navigator_new (Canvas   *canvas)
{
  Navigator *navigator = g_malloc0 (sizeof (Navigator));
  View  *view  = (View*) navigator;
  
  view_init (view, canvas);
  view_set_event  (view, event, NULL);
  view_set_render (view, render, NULL);

  navigator->zoom = 65536 * 0.1;
  
  return navigator;
}

void
navigator_set_easel (Navigator *navigator,
                     Easel     *easel)
{
  navigator->easel = easel;
}

void
navigator_set_zoom (Navigator *navigator,
                    gint       zoom)
{
  navigator->zoom = zoom;
}
