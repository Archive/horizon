/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <math.h>
#include <string.h>
#include "brush.h"
#include "canvas/canvas.h"
#include "view.h"
#include "label.h"
#include "widget.h"
#include "vectors/font.h"
#include "tools/tool_library.h"

#include "config.h"

#define H_LABEL_OFFSET  20

struct _Label
{
  Widget widget;
  
  gpointer data;

  gchar *label;
  gboolean active;

  gchar    *old_label;
  gboolean  old_active;

  gint     font_size;
  
  Allocation *allocation;
  Allocation *active_allocation;
  
  void (*label_pressed_function) (Label   *label,
		                  gpointer  data);
};

static void
label_render (View *view,
              gpointer  data)
{
  Box      *box      = (Box*) view;
  Widget   *widget   = (Widget*) view;
  Label    *label    = (Label*) widget;

  if (label &&
      ((!label->old_label) ||
       strcmp (label->old_label, label->label) ||
      (label->active && !label->active_allocation)))
    {
      gint width = box_get_width (box);
      gint height = box_get_height (box);

      if (!label->allocation)
        label->allocation = canvas_alloc (view->canvas, width, height);

      {
        gint x = CANVAS_RECT (label->allocation)->x;
        gint y = CANVAS_RECT (label->allocation)->y;
        gint width = box_get_width (box);
        gint height = box_get_height (box);
        HorizonFont *font = font_new ();

        Path *path = NULL;

        canvas_fill_rect (view->canvas, x,y,width, height, widget->background_color[0],
                                                           widget->background_color[1],
                                                           widget->background_color[2],
                                                           65536);

        font_set_size (font, label->font_size);
        path = font_path (font, NULL, (x + 5) * SPP, (y + 20) * SPP, width * SPP, label->label);

        path_set_rgba (path, widget->primary_color[0],
                             widget->primary_color[1],
                             widget->primary_color[2],
                             65536 * 0.2);
        path_set_hardness (path, 65536 * 0.4);

        if (label->font_size < 8)
           path_set_line_width (path, 65536 * 0.01875 * 2 * 0.75);
        else
           path_set_line_width (path, 65536 * 0.01875 * 2);

        path_stroke (path, view->canvas);

        path=path_destroy (path);
        font_destroy (font);
      } 

      if (label->active && !label->active_allocation)
        {
          label->active_allocation = canvas_alloc (view->canvas, width, height);
        }

      if (label->active_allocation)
        {
          gint x = CANVAS_RECT (label->active_allocation)->x;
          gint y = CANVAS_RECT (label->active_allocation)->y;
          gint width = box_get_width (box);
          gint height = box_get_height (box);

          HorizonFont *font = font_new ();

          Path *path = NULL;

          canvas_fill_rect (view->canvas, x,y,width, height, widget->background_color[0],
                                                             widget->background_color[1],
                                                             widget->background_color[2],
                                                             65536);

          font_set_size (font, label->font_size);
          path = font_path (font, NULL, (x + 5) * SPP, (y + 20) * SPP, width * SPP, label->label);

          path_set_rgba (path, widget->secondary_color[0],
                               widget->secondary_color[1],
                               widget->secondary_color[2],
                               65536 * 0.15);
          path_set_hardness (path, 65536 * 0.08);
          path_set_line_width (path, 65536 * 0.15 * 2);
          path_stroke (path, view->canvas);

          path_set_rgba (path, widget->primary_color[0],
                               widget->primary_color[1],
                               widget->primary_color[2],
                               65536 * 0.3);
          path_set_hardness (path, 65536 * 0.4);
          path_set_line_width (path, 65536 * 0.02875 * 2);
          path_stroke (path, view->canvas);

          path=path_destroy (path);
          font_destroy (font);
        }

      if (label->old_label)
        {
          g_free (label->old_label);
        }

      label->old_label  = g_strdup (label->label);
    }

  if (label->active)
    {
      view_lookat (view, view->canvas,
        CANVAS_RECT (label->active_allocation)->x,
        CANVAS_RECT (label->active_allocation)->y, 65536);
    }
  else
    {
      view_lookat (view, view->canvas,
        CANVAS_RECT (label->allocation)->x,
        CANVAS_RECT (label->allocation)->y, 65536);
    }

  label->old_active = label->active;
}

static gboolean
label_event (View *view,
             gint      x,
             gint      y,
             gint      pressure,
             gulong    time,
             gpointer  data)
{
  Label *label = (Label*) data;


  if (label->label_pressed_function)
    {
      label->label_pressed_function (label, label->data);
    }
   
  return TRUE;
}

void
label_set_event (Label *label,
                 void (*label_pressed) (Label    *label,
	                                gpointer  data),
	         gpointer  data)
{
  label->label_pressed_function = label_pressed;
  label->data = data;
}

Label *
label_new (Canvas   *canvas)
{
  Label  *label = g_malloc0 (sizeof (Label));
  Widget *widget = (Widget*) label;
  View  *view  = (View*) widget;
  
  widget_init (widget, canvas);
  
  view_set_event (view, label_event, label);
  view_set_render (view, label_render, NULL);
 
  label->font_size = 12;

  label->label = g_strdup ("");
  view_lookat (view, view->canvas, 0,0, 65536);
  
  return label;
}

void
label_set_label (Label      *label,
                 const char *new_label)
{
  if (label->label)
    g_free (label->label);
  label->label = g_strdup (new_label);
  box_set_dirty ((Box*)label);
}

void
label_set_active (Label    *label,
                  gboolean  active)
{
  label->active = active;
  box_set_dirty ((Box*)label);
}

void
label_set_font_size (Label *label,
                     gint   font_size)
{
  label->font_size = font_size;
  box_set_dirty ((Box*)label);
}
