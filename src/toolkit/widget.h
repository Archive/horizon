/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _WIDGET_H
#define _WIDGET_H

#include "canvas/canvas.h"
#include "toolkit/box.h"

typedef struct _Widget Widget;

struct _Widget
{
  View     view;
  
  guint    primary_color[3];
  guint    secondary_color[3];
  guint    background_color[3];
};

void                 widget_init (Widget *widget,
                                  Canvas *canvas);


void widget_set_primary_color    (Widget *widget,
                                  guint   red,
                                  guint   green,
                                  guint   blue);

void widget_set_secondary_color  (Widget *widget,
                                  guint   red,
                                  guint   green,
                                  guint   blue);

void widget_set_background_color (Widget *widget,
                                  guint   red,
                                  guint   green,
                                  guint   blue);


#endif
