/*_license(*/
/* license automatically inserted by boilc
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Copyright 2004 Øyvind Kolås
 *
 */

/*)*/

#include <sys/time.h>
#include <time.h>
#define msecs(time) ((time.tv_sec-self->init_time.tv_sec)*1000 + time.tv_usec/1000)


/*_def(*/
#include <stdlib.h>
#include <string.h>
#include "timer.h"

struct _Timer
{
  double    elapsed_seconds;
  long      elapsed_ms;
/*)*/

  struct timeval init_time;
  struct timeval start_time;
  struct timeval end_time;
  int       stopped;

/*_enddef(*/
};
/*)*/

/*_static_functions(*/
static void timer_set_elapsed_seconds (Timer * self, double elapsed_seconds);

static void timer_set_elapsed_ms (Timer * self, long elapsed_ms);

static void
boilc_ref_statics (void)
{
  timer_set_elapsed_seconds (0, 0);
  timer_set_elapsed_ms (0, 0);
  boilc_ref_statics ();
}

/*)*/

/*_timer_start(*/
void
timer_start (Timer * self)
{
/*)*/
  gettimeofday (&(self->start_time), NULL);
  self->stopped = 0;
/*__timer_start(*/
}

/*)*/

/*_timer_stop(*/
void
timer_stop (Timer * self)
{
/*)*/
  gettimeofday (&(self->end_time), NULL);
  self->stopped = 1;

/*__timer_stop(*/
}

/*)*/

/*_timer_new(*/
Timer    *
timer_new (void)
{
  Timer    *self = calloc (1, sizeof (Timer));
/*)*/
  gettimeofday (&(self->init_time), NULL);
  timer_start (self);

/*__timer_new(*/
  return self;
}

/*)*/

/*_timer_free(*/
void
timer_free (Timer * self)
{
/*)*/

/*__timer_free(*/
  free (self);
}

/*)*/
/*_timer_get_elapsed_seconds(*/
double
timer_get_elapsed_seconds (Timer * self)
{
/*)*/
  self->elapsed_seconds = timer_get_elapsed_ms (self) / 1000.0;
/*__timer_get_elapsed_seconds(*/
  return self->elapsed_seconds;
}

/*)*/

/*_timer_get_elapsed_ms(*/
long
timer_get_elapsed_ms (Timer * self)
{
/*)*/
  if (self->stopped)
    {
      self->elapsed_ms = (msecs (self->end_time) - msecs (self->start_time));
    }
  else
    {
      struct timeval current_time;
      gettimeofday (&current_time, NULL);
      self->elapsed_ms = (msecs (current_time) - msecs (self->start_time));
    }

/*__timer_get_elapsed_ms(*/
  return self->elapsed_ms;
}

/*)*/



/*_timer_set_elapsed_seconds(*/
static void
timer_set_elapsed_seconds (Timer * self, double elapsed_seconds)
{
/*)*/

/*__timer_set_elapsed_seconds(*/
  self->elapsed_seconds = elapsed_seconds;
}                               /*) */

/*_timer_set_elapsed_ms(*/
static void
timer_set_elapsed_ms (Timer * self, long elapsed_ms)
{
/*)*/

/*__timer_set_elapsed_ms(*/
  self->elapsed_ms = elapsed_ms;
}                               /*) */
