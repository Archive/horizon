/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005, 2006 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#ifndef _HORIZON_H
#define _HORIZON_H

#include "canvas/canvas.h"
#include "canvas/undo.h"
#include "toolkit/box.h"
#include "toolkit/easel.h"
#include "stylus.h"

typedef struct _Horizon Horizon;

struct _Horizon
{
  Canvas    *canvas;
  Easel     *easel;
  UndoStack *undo_stack;

  Box       *root;
  Stylus    *stylus;

  Box       *prefs;

  gchar     *storage_path;
  gboolean   quit;

  gboolean   fullscreen;
  gboolean   first_run;

  gint       screen_width;
  gint       screen_height;

  Display   *dpy;

  gboolean   prefetch;
  gint       cache_mem;
  gint       wash_threshold;
};

void horizon_status (Horizon     *horizon,
                     const gchar *message);


void horizon_show_prefs (Horizon *horizon);

Horizon * get_global_horizon (void);

#endif
