/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#ifndef _ACTIONS_H
#define _ACTIONS_H

#include <glib.h>
#include <string.h>
#include "config.h"
#include "horizon.h"

typedef struct _Action Action;

struct _Action
{
  char *keysym;
  char *name;
  gboolean (*action)(Horizon *horizon);
};

extern Action actions[];

gboolean
action (Horizon *horizon,
        gchar   *action);

#endif
