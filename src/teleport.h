#ifndef TELEPORT_H
#define TELEPORT_H

#include <glib.h>
#include "horizon.h"
#include "toolkit/view.h"

void
horizon_teleport (Horizon     *horizon,
                  gint         dest_x,
                  gint         dest_y,
                  gint         dest_scale,
                  gint         time);

#endif
