/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _CONFIG_H
#define _CONFIG_H

#include "canvas/map.h"

/*  where is the data for the canvas stored, (unless horizon is started with
 *  a commandline parameter specifying the canvas 
 */
#define CANVAS_PATH         "/tmp/horizon"


#define USER_POSITION_PATH  CANVAS_PATH "/position"

/*  location to place a screenshot when quitting
 */
#define SCREENSHOT_FILE     CANVAS_PATH "/last.bmp"


/*  amount of memory set aside for cached image data (more makes panning
 *  faster)
 */
#define CANVAS_SYSTEM_CACHE_MB        2
#define CANVAS_CACHE_MB               11

#define CANVAS_CACHE_WASH_PERCENTAGE  30

/*  amount of memory set aside for undo steps, more = larger undo history
 */
#define UNDO_MB                       8

#define ZOOM_FACTOR   (0.33)

#define ZOOM_IN  (1.0 + ZOOM_FACTOR)
#define ZOOM_OUT (1.0 / ZOOM_IN)

#define ZOOM_MAX   (65536 * 3.0)
#define ZOOM_MIN   (65536 * 0.0175)

/* The on-screen brush radius achieved by a radius setting of 65536 */
#define MAX_RADIUS         (64 * SPP)

/* maximum size of radius drawn,. in 1:1 pixels*/
#define HARD_MAX_RADIUS    (256 * SPP)

#define MIN_SPACING        (65536*0.5)

#define MAX_OPACITY        65536 

/*  constants calculated based on the preceding values
 */
#define TILE_SIZE_BYTES     ((TILE_SIZE * TILE_SIZE * 4) + 64)
#define CANVAS_CACHE_TILES  ((CANVAS_CACHE_MB * 1024*1024) / TILE_SIZE_BYTES)
#define CANVAS_SYSTEM_CACHE_TILES  ((CANVAS_SYSTEM_CACHE_MB * 1024*1024) / TILE_SIZE_BYTES)
#define UNDO_TILES          ((UNDO_MB         * 1024*1024) / TILE_SIZE_BYTES)

#define SUBDIVIDE_LEVELS 5

/*   affects which scale of tiles is used for rendering
 *   0.5 = correct
 *   1.0 = faster
 *   2.0 = double pixels to quadruple pixels
 */
#define MIN_TILE_SCALE     0.5

#define BEZIER_SEGMENTS 10


#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 480

//#define DO_TELEPORT
//#define TILE_IO_LOG
//#define TILE_IO_SLOW

#define TILE_IO_SLOW_SAVE_DELAY 3000
#define TILE_IO_SLOW_LOAD_DELAY 2000

#endif
