/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include "tile.h"
#include <stdio.h>
#include "canvas.h"
#include "tile_cache.h"

static GObjectClass *parent_class = NULL;

static void
tile_cache_finalize (GObject *object)
{
  TileCache *cache;
  GSList *list;

  cache = (TileCache*) object;

  fprintf (stderr, "hits: %i misses: %i  hit percentage:%f (size=%i)\n", cache->hits, cache->misses,
     cache->hits * 100.0 / (cache->hits+cache->misses), cache->size);

  while ((list = cache->list))
    {
      Tile *tile = list->data;
      cache->list = g_slist_remove (cache->list, tile);
      g_object_unref (G_OBJECT (tile));
    }

  (* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
tile_cache_class_init (TileCacheClass *class)
{
  GObjectClass *gobject_class = (GObjectClass*) class;

  parent_class = g_type_class_peek_parent (class);

  gobject_class->finalize = tile_cache_finalize;
}

static void
tile_cache_init (TileCache *cache)
{
  cache->wash_percentage = 20;
  cache->list = NULL;
}

GType
tile_cache_get_type (void)
{
  static GType type = 0;

  if (type == 0)
  {
    static const GTypeInfo info =
    {
      sizeof (TileCacheClass),
      (GBaseInitFunc)     NULL,
      (GBaseFinalizeFunc) NULL,
      (GClassInitFunc)    tile_cache_class_init,
      NULL,               /* class_finalize */
      NULL,               /* class_data */
      sizeof (TileCache),
      0,                  /* n_preallocs */
      (GInstanceInitFunc) tile_cache_init
    };

    type = g_type_register_static (G_TYPE_OBJECT,
                                   "TileCache", &info, 0);
  }

  return type;
}

/* create a new tile cache, associated with the given canvas and capable of holding size tiles */
TileCache *
tile_cache_new (void)
{
  TileCache *cache = g_object_new (tile_cache_get_type(), NULL);

  tile_cache_set_wash_percentage (cache, 20);
  tile_cache_set_size (cache, 16*4);

  return cache;
}

void
tile_cache_set_size (TileCache *cache,
                     gint       size)
{
  cache->size = size;
}

gint
tile_cache_get_size (TileCache *cache)
{
  return cache->size;
}

void
tile_cache_set_wash_percentage (TileCache *cache,
                                gint       wash_percentage)
{
  cache->wash_percentage = wash_percentage;
}

gint
tile_cache_get_wash_percentage (TileCache *cache)
{
  return cache->wash_percentage;
}

/* write the least recently used dirty tile to disk if it
 * is in the wash_percentage (20%) least recently used tiles,
 * calling this function in an idle handler distributes the
 * tile flushing overhead over time.
 */
gboolean
tile_cache_wash (TileCache *cache)
{
  Tile *last_dirty = NULL;
  guint count = 0;

  gint wash_tiles = cache->wash_percentage * cache->size / 100;

  GSList *list = cache->list;
  while (list)
    {
      Tile *tile = list->data;
      count++;
      if (!tile_is_stored (tile))
        {
          if (count > cache->size-wash_tiles)
            {
              last_dirty = tile;
            }
        }
      list = list->next;
    }
  if (last_dirty != NULL)
    {
      canvas_store_tile (last_dirty->canvas, last_dirty);
      return TRUE;
    }
  return FALSE;
}


gboolean
tile_cache_has_tile (TileCache *cache,
                     gint       x,
                     gint       y)
{
  Tile *tile = tile_cache_get_tile (cache, x, y);
  if (tile)
    {
      g_object_unref (G_OBJECT (tile));
      cache->hits--;
      return TRUE;
    }
  else
    {
      cache->misses--;
    }
  return FALSE;
}

/* returns the requested Tile * if it is in the cache NULL
 * otherwize.
 */
Tile *
tile_cache_get_tile (TileCache *cache,
                     gint       x,
                     gint       y)
{
  Tile  *tile = NULL;
  if (cache->size > 0)
    {
      GSList *list = cache->list;
      GSList *prev = NULL;

      while (list)
        {
          Tile *cur_tile = list->data;
            
          if (cur_tile != NULL &&
              cur_tile->x == x &&
              cur_tile->y == y)
            {
              tile = list->data;
              break;
            }
          prev = list;
          list = list->next;
        }

      if (tile)
        {
          cache->hits++;
          
          if (prev)
            {
              if (prev->next)
                {
                  prev->next=list->next;
                }
              if (cache->list)
                list->next=cache->list;
              cache->list=list;
            }
          g_object_ref (G_OBJECT (tile));
        }
      else
        {
          cache->misses++;
        }
    }
  return tile;
}

/* Insert a tile into a cache. (warning no check is done
 * to make sure it isn't already in the cache)
 */
void
tile_cache_insert (TileCache *cache,
                   Tile      *tile)
{
  guint count = g_slist_length (cache->list);
  g_object_ref (G_OBJECT (tile));
  cache->list = g_slist_prepend (cache->list, tile);

  if (count > cache->size)
    {
      gint to_remove = count - cache->size;

      while (--to_remove)
        {
          GSList *last = g_slist_last (cache->list);
          Tile   *tile = last->data;
          
          /* XXX: FIXME: avoiding eviction of tiles with
           * a refcount of >1, should probably spend some more time
           * here finding a replacement victim tile.
           *
           * evicting easy to regenerate data (e.g. blank tiles)
           * should also be a priority
           */

          if (G_OBJECT(tile)->ref_count <= 1)
            {
              g_object_unref (G_OBJECT (tile));
              cache->list = g_slist_remove (cache->list, tile);
            }
        }
    }
}
