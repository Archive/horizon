#ifndef _PNG_UTIL_H
#define _PNG_UTIL_H

#include <glib.h>
#include "canvas.h"

gint
canvas_import_png (Canvas      *canvas,
                   const gchar *path,
                   gint         x,
                   gint         y);


gint
canvas_export_png (Canvas      *canvas,
                   const gchar *path,
                   gint         x,
                   gint         y,
                   gint         width,
                   gint         height);


/* utility functions */

gint
horizon_load_png (const gchar  *path,
                  gint         *width,
                  gint         *height,
                  gint         *row_stride,
                  guchar      **pixels);


gint
horizon_save_png (const gchar *path,
                  gint         width,
                  gint         height,
                  gint         row_stride,
                  guchar      *data);

gint
horizon_query_png (const gchar *path,
                   gint        *width,
                   gint        *height);

gint
canvas_import_jpg (Canvas      *canvas,
                   const gchar *path,
                   gint         x,
                   gint         y);

gint
horizon_query_jpg (const gchar *path,
                   gint        *width,
                   gint        *height);

#endif
