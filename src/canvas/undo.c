/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include "undo.h"
#include "canvas.h"
#include "tile.h"
#include "config.h"

typedef struct _UndoItem UndoItem;
typedef struct _UndoTile UndoTile;

struct _UndoTile
{
  Canvas *canvas;   /* the canvas where the original data comes from */
  gint  original_x; /* location in the canvas this undo tile is a reference for */
  gint  original_y;
  
  gint  undo_x;     /* location of this undo tile in the canvas */
  gint  undo_y;
};

UndoTile *
undo_tile_new ()
{
  UndoTile *undo_tile = g_malloc0 (sizeof (UndoTile));
  return undo_tile;
}

void undo_tile_destroy (UndoTile *undo_tile)
{
/*  if (undo_tile->tile)
    g_object_unref (G_OBJECT (undo_tile->tile));
*/
  g_free (undo_tile);
}

struct _UndoItem {
  GSList *tiles;
};

static UndoItem *
undo_item_new ()
{
  UndoItem *item = g_malloc0 (sizeof (UndoItem));
  return item;
}

static void
undo_item_destroy (UndoItem *item)
{
  GSList *list;
  while ((list = item->tiles))
    {
      UndoTile *undo_tile = list->data;
      item->tiles = g_slist_remove (item->tiles, undo_tile);
      undo_tile_destroy (undo_tile);
    }
  g_free (item);
}

static gint
undo_item_tile_count (UndoItem *item)
{
  return g_slist_length (item->tiles);
}

/****************************************************************************/

struct _UndoStack
{
  gint    frozen;      /* < 0 means undo disabled */
  Canvas *canvas;      /* canvas where undo items are stored */
  GSList *undo_items;
  GSList *redo_items;
  gint    tile_no;     /* counter to keep track of the current undo tile location */
};

/* create a undo_stack storing undo tiles in specified canvas
 */
UndoStack *
undo_stack_new (Canvas    *canvas)
{
  UndoStack *stack = g_malloc0 (sizeof (UndoStack));
  stack->canvas = canvas;
  stack->tile_no = 0;
  return stack;
}

static void
clear_redo (UndoStack *stack)
{
  GSList *list;
  while ((list = stack->redo_items))
    {
      UndoItem *item = list->data;
      stack->redo_items = g_slist_remove (stack->redo_items, item);
      undo_item_destroy (item);
    }
}

void
undo_stack_destroy (UndoStack *stack)
{
  GSList *list;
  while ((list = stack->undo_items))
    {
      UndoItem *item = list->data;
      stack->undo_items = g_slist_remove (stack->undo_items, item);
      undo_item_destroy (item);
    }
  clear_redo (stack);

  g_free (stack);
}

static gint
undo_stack_tile_count (UndoStack *stack)
{
  GSList *iter;
  gint sum=0;

  iter = stack->undo_items;

  while (iter)
    {
      UndoItem *item = iter->data;
      sum += undo_item_tile_count (item);
      iter = g_slist_next (iter); 
    }
  return sum;
}

void
undo_stack_new_step (UndoStack *stack)
{
  UndoItem *item = undo_item_new ();
  clear_redo (stack);
  stack->undo_items = g_slist_prepend (stack->undo_items, item);
}

gint
undo_stack_get_undos (UndoStack *stack)
{
  return g_slist_length (stack->undo_items);
}

gint
undo_stack_get_redos (UndoStack *stack)
{
  return g_slist_length (stack->redo_items);
}

void
undo_stack_undo (UndoStack *stack)
{
  GSList *iter;
  UndoItem *item;

  if (!undo_stack_get_undos (stack))
    return;

  undo_stack_freeze (stack);

  item = stack->undo_items->data;
  g_assert (item);

  iter = item->tiles;

  while (iter)
    {
      UndoTile *undo_tile;
      Tile *image_tile;
      Tile *stored_tile;

      undo_tile = iter->data;
      image_tile = canvas_get_tile (undo_tile->canvas,
          undo_tile->original_x, undo_tile->original_y);

      stored_tile = canvas_get_tile (stack->canvas,
                                     undo_tile->undo_x, undo_tile->undo_y);

      tile_lock (image_tile);
      tile_swp (stored_tile, image_tile);
      tile_unlock (image_tile);

      g_object_unref (G_OBJECT (stored_tile));
      g_object_unref (G_OBJECT (image_tile));


      stack->tile_no--;
      if (stack->tile_no < 0)
        stack->tile_no= UNDO_TILES - 1;
      
      iter = g_slist_next (iter); 
    }
  stack->redo_items = g_slist_prepend (stack->redo_items, item);
  stack->undo_items = g_slist_remove (stack->undo_items, item);
  undo_stack_thaw (stack);
}

static void
undo_stack_expunge_oldest (UndoStack *stack)
{
  UndoItem *oldest = g_slist_last (stack->undo_items)->data;
  stack->undo_items = g_slist_remove (stack->undo_items, oldest);
  undo_item_destroy (oldest);
}

void
undo_stack_redo (UndoStack *stack)
{
  GSList *iter;
  UndoItem *item;

  if (!undo_stack_get_redos (stack))
    return;
  undo_stack_freeze (stack);

  item = stack->redo_items->data;
  g_assert (item);

  iter = item->tiles;

  while (iter)
    {
      UndoTile *redo_tile;
      Tile     *image_tile;
      Tile     *stored_tile;

      redo_tile = iter->data;
      image_tile = canvas_get_tile (redo_tile->canvas,
          redo_tile->original_x, redo_tile->original_y);

      stored_tile = canvas_get_tile (stack->canvas,
                                     redo_tile->undo_x, redo_tile->undo_y);

      tile_lock (image_tile);
      tile_swp (stored_tile, image_tile);
      tile_unlock (image_tile);

      g_object_unref (G_OBJECT (stored_tile));
      g_object_unref (G_OBJECT (image_tile));
      iter = g_slist_next (iter); 

      stack->tile_no++;
      if (stack->tile_no >= UNDO_TILES)
        stack->tile_no = 0;
      
    }
  stack->undo_items = g_slist_prepend (stack->undo_items, item);
  stack->redo_items = g_slist_remove (stack->redo_items, item);
  undo_stack_thaw (stack);
}

static gint compare (gconstpointer a,
                     gconstpointer b)
{
  const UndoTile *tile_a = a;
  const Tile     *tile_b = b;
  
  return !(tile_a != NULL &&
           tile_b != NULL &&
           tile_a->original_x == tile_b->x &&
           tile_a->original_y == tile_b->y);
}

/* Add a tile to the current undo step
 * this creates a copy of the tile involved.
 *
 * If the tile is already added to the current
 * undo step it is a noop, since the state of that
 * tile before the current stroke is already stored.
 */
void undo_stack_add_tile        (UndoStack *stack,
                                 Tile      *tile)
{
  UndoItem *item;
  GSList   *stored_tile;

  if (stack->frozen < 0)
    return;

  while (undo_stack_tile_count (stack) >= UNDO_TILES)
    {
      undo_stack_expunge_oldest (stack);
    }

  if (!stack->undo_items)
    {
      g_warning ("no current undo item (no danger)");
      undo_stack_new_step (stack);
    }

  item = stack->undo_items->data;
  stored_tile = g_slist_find_custom (item->tiles, tile, compare);
  if (stored_tile)
    {
      //g_warning ("tile was already in undo pack\n");
      return;
    }

  g_object_ref (G_OBJECT (tile));
  {
    UndoTile *undo_tile = undo_tile_new ();
    Tile *stored_tile;

    undo_tile->canvas = tile_get_canvas (tile);
    undo_tile->original_x = tile_get_x (tile);
    undo_tile->original_y = tile_get_y (tile);
    undo_tile->undo_x = stack->tile_no;
    undo_tile->undo_y = tile_tile (MIDDLE);

    stored_tile = canvas_get_tile (stack->canvas,
                                   undo_tile->undo_x, undo_tile->undo_y);

    tile_cpy (tile, stored_tile);

    g_object_unref (G_OBJECT (stored_tile));

    stack->tile_no++;
    if (stack->tile_no > UNDO_TILES)
      stack->tile_no=0;

    item->tiles = g_slist_prepend (item->tiles, undo_tile);
  }
  g_object_unref (G_OBJECT (tile));
}


void
undo_stack_freeze    (UndoStack *stack)
{
  stack->frozen--;
}

void
undo_stack_thaw      (UndoStack *stack)
{
  stack->frozen++;
}
