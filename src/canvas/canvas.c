/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

#include <string.h>  /* memset */
#include "canvas.h"
#include "map.h"
#include "tile.h"
#include "tile_cache.h"
#include "config.h"

#define COMFORT_MAX  256

/* dirty */

typedef struct _CachePriv CachePriv;

struct _CachePriv
{
  TileCache *cache;//  <- should be implemented as alayer for the canvas
  TileCache *system_cache;//  <- should be implemented as a layer for the canvas
};

static void
cache_init (Canvas *canvas)
{
  CachePriv *priv = canvas->cache = g_malloc (sizeof (CachePriv));

  priv->cache = tile_cache_new ();
  tile_cache_set_size (priv->cache, CANVAS_CACHE_TILES);
  priv->system_cache = tile_cache_new ();
  tile_cache_set_wash_percentage (priv->cache, CANVAS_CACHE_WASH_PERCENTAGE);
  tile_cache_set_size (priv->system_cache, CANVAS_SYSTEM_CACHE_TILES);
}

static void
cache_destroy (Canvas *canvas)
{
  CachePriv *priv = canvas->cache;
  if (!canvas->cache)
    return;

  g_object_unref (G_OBJECT (priv->cache));
  g_object_unref (G_OBJECT (priv->system_cache));

  g_free (priv);
}

gboolean
canvas_has_tile_in_memory (Canvas *canvas,
                           gint    x,
                           gint    y)
{
  CachePriv *cache = canvas->cache;

  if (!cache)
    return FALSE;

  if (y >= tile_tile (ALLOCATOR_Y) &&
      x <= tile_tile (MIDDLE))
    {
      return tile_cache_has_tile (cache->system_cache, x, y);
    }
  else
    {
      return tile_cache_has_tile (cache->cache, x, y);
    }
  return FALSE;
}

/*
 * Prefetch a region of the canvas:
 * params: 
 *   x0     upperleft horizontal coordinate in canvas pixel units
 *   y0     upperleft vertical coordinate in canvas pixel units
 *   width  width in canvas pixel units
 *   height height in canvas pixel units
 *   scale  the scale level a view would be set to to render this
 *          region. (set to 65536 for 1:1) 
 *
 * returns true if a tile was fetched.
 */
gboolean
canvas_prefetch (Canvas    *canvas,
                 gint       x0,
                 gint       y0,
                 gint       width,
                 gint       height,
                 gint       scale,
                 gboolean   do_context)
{
  gint  x=0,y;
  gint  sscale;
  glong sx, sy;
  gint  scaled_tile_width;
  gint  scaled_tile_height;
  gint  context_tiles = do_context?1:0;

  sx = x0;
  sy = y0;
  sscale = scale;

  {
    while (sscale < 65536 * MIN_TILE_SCALE)
      {
        sx = (sx + (TILE_ZOOM_START*2))/2;
        sy = (sy + (TILE_ZOOM_START*2))/2;
        sscale = sscale * 2;
      }
  }

  scaled_tile_width  = canvas->tile_width  * sscale / 65536;
  scaled_tile_height = canvas->tile_height * sscale / 65536;

  for (y=-context_tiles;
       (y-1-context_tiles) * scaled_tile_height < height * scale / 65536;
       y++)
         for (x=-context_tiles;
              (x-1-context_tiles) * scaled_tile_width < width * scale / 65536;
              x++)
           {
             Tile *tile;
             gint tile_x = (tile_tile (sx) + x) & 0xffffff;
             gint tile_y = (tile_tile (sy) + y) & 0xffffff;

             if (!canvas_has_tile_in_memory (canvas, tile_x, tile_y))
               {
                 tile = canvas_get_tile (canvas, tile_x, tile_y);
                 return TRUE;
               }
           }
  return FALSE;
}

/**************/

typedef struct _DirtyPriv DirtyPriv;

struct _DirtyPriv
{
  gint       size;   //  <- dirtying is dynamic.
  gint       count;  //     /* number of dirty tiles kept track of */
  gint      *tile;  //     /* storage for dirty tiles */
};

static void
dirty_init (Canvas *canvas)
{
  DirtyPriv *priv = canvas->dirty = g_malloc (sizeof (DirtyPriv));
  priv->count = 0;
  priv->size = COMFORT_MAX;
  priv->tile = g_malloc (sizeof (gint) * 2 * priv->size);
}

static void
dirty_destroy (Canvas *canvas)
{
  DirtyPriv *priv = canvas->dirty;
  if (!canvas->dirty)
    return;
  if (priv->tile)
    g_free (priv->tile);
  g_free (priv);
}

/**************/

typedef struct _EmptyPriv EmptyPriv;

struct _EmptyPriv
{
  Tile *tile;
};

static void
empty_init (Canvas *canvas)
{
  EmptyPriv *priv = canvas->empty = g_malloc (sizeof (EmptyPriv));
  priv->tile = tile_new (canvas->tile_width,
                         canvas->tile_height,
                         NULL);
  memset (tile_get_data (priv->tile), 0xff, canvas->tile_width * canvas->tile_height *4);
}

static void
empty_destroy (Canvas *canvas)
{
  EmptyPriv *priv = canvas->empty;
  if (!canvas->empty)
    return;
  if (priv->tile)
    ;/*FIXME: g_free (priv->tile);  */
  g_free (priv);
}

/**************/

typedef struct _FetcherPriv FetcherPriv;

struct _FetcherPriv
{
  gchar *path;
};

static void
fetcher_init (Canvas *canvas)
{
  FetcherPriv *priv = canvas->fetcher = g_malloc (sizeof (FetcherPriv));
  priv->path = NULL;
  canvas_set_path (canvas, "/tmp/fnord");
}

static void
fetcher_destroy (Canvas *canvas)
{
  FetcherPriv *priv = canvas->fetcher;
  if (!canvas->fetcher)
    return;
  if (priv->path)
    g_free (priv->path);
  g_free (priv);
}

static gchar hex_digits[17]="0123456789ABCDEF";

static char *
make_id (gchar *buf,
         gint   x,
         gint   y)
{
  snprintf (buf, 256, "%c/%c/%c/%06X_%06X", hex_digits[x%16], hex_digits[y%16], hex_digits[(x/16)%16], x, y);
  return buf;
}


static gint prepare_storage (Canvas *canvas)
{
  if (!g_file_test (((FetcherPriv*)canvas->fetcher)->path, G_FILE_TEST_IS_DIR))
    {
      if (g_mkdir (((FetcherPriv*)canvas->fetcher)->path, 0777))
        {
          g_error ("failed to created canvas storage dir %s", ((FetcherPriv*)canvas->fetcher)->path);
          return -1;
        }
    }
  return 0;
}

void
canvas_set_path (Canvas      *canvas,
                 const gchar *path)
{
  if (((FetcherPriv*)canvas->fetcher)->path)
    g_free (((FetcherPriv*)canvas->fetcher)->path);
  ((FetcherPriv*)canvas->fetcher)->path = NULL;

  if (path)
    ((FetcherPriv*)canvas->fetcher)->path = g_strdup (path);

  prepare_storage (canvas);
}

const gchar *
canvas_get_path (Canvas *canvas)
{
  return ((FetcherPriv*)canvas->fetcher)->path;
}


/* this is the only place that actually should
 * instantiate tiles, when the cache is large enough
 * that should make sure we don't hit this function
 * too often.
 */
static Tile *fetcher_get_tile (Canvas      *canvas,
                               gint         x,
                               gint         y)
{
  Tile *tile;
  gchar path[512];
  gchar id[128];

  make_id (id, x, y);
  sprintf (path, "%s/%s.png", ((FetcherPriv*)canvas->fetcher)->path, id);
  tile = tile_load (path);

  if (!tile)
    {
      tile = tile_dup (((EmptyPriv*)canvas->empty)->tile);
    }

  tile_set_canvas (tile, canvas);
  tile_set_x (tile, x);
  tile_set_y (tile, y);
  return tile;
}


/*****************/

static void
undo_init (Canvas *canvas)
{
}

static void
undo_destroy (Canvas *canvas)
{
}


static GObjectClass *parent_class = NULL;

static void
canvas_finalize (GObject *object)
{
  Canvas *canvas = (Canvas*) object;

  empty_destroy (canvas);
  cache_destroy (canvas);
  fetcher_destroy (canvas);
  dirty_destroy (canvas);
  undo_destroy (canvas);

  (* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
canvas_class_init (CanvasClass *class)
{
  GObjectClass *gobject_class;

  gobject_class = (GObjectClass*) class;

  parent_class = g_type_class_peek_parent (class);
  gobject_class->finalize = canvas_finalize;
}

static void
canvas_init (Canvas *canvas)
{
  canvas->format = NULL;
  canvas->tile_width  = TILE_SIZE;
  canvas->tile_height = TILE_SIZE;

  empty_init (canvas); 
  fetcher_init (canvas); 
  cache_init (canvas);
  undo_init (canvas);
  dirty_init (canvas);
}

GType
canvas_get_type (void)
{
  static GType type = 0;

  if (type == 0)
  {
    static const GTypeInfo info =
    {
      sizeof (CanvasClass),
      (GBaseInitFunc)     NULL,
      (GBaseFinalizeFunc) NULL,
      (GClassInitFunc)    canvas_class_init,
      NULL,               /* class_finalize */
      NULL,               /* class_data */
      sizeof (Canvas),
      0,                  /* n_preallocs */
      (GInstanceInitFunc) canvas_init
    };

    type = g_type_register_static (G_TYPE_OBJECT,
                                   "Canvas", &info, 0);
  }

  return type;
}

Canvas *canvas_new ()
{
  Canvas *canvas = g_object_new (canvas_get_type(), NULL);

  return canvas;
}

void
canvas_set_undo_stack (Canvas    *canvas,
                       UndoStack *undo_stack)
{
  canvas->undo = undo_stack;
}

UndoStack *
canvas_get_undo_stack (Canvas *canvas)
{
  return canvas->undo;
}


void
canvas_add_dirty (Canvas *canvas,
                  gint    x,
                  gint    y)
{
  DirtyPriv *priv = canvas->dirty;

  if (canvas_is_dirty (canvas, x,y))
    return;
  
  if (priv->count+1 >= priv->size)
    {
      priv->size*=2;
      priv->tile = g_realloc (priv->tile, sizeof (gint) * 2 * priv->size);
      canvas_add_dirty (canvas, x, y);
      return;
    }
  else
    {
      gint c = priv->count++;
      priv->tile[c*2+0]=x;
      priv->tile[c*2+1]=y;
    }

    /*  add undo */
    if (canvas->undo       && 
        y < tile_tile (MIDDLE)   &&
        x < tile_tile (MIDDLE))
      {
        Tile * tile = canvas_get_tile (canvas, x, y);
        undo_stack_add_tile (canvas->undo, tile);
        g_object_unref (G_OBJECT (tile));
      }
}

void
canvas_flush_dirty (Canvas *canvas)
{
  DirtyPriv *priv = canvas->dirty;

  priv->count = 0;
  if (priv->size > COMFORT_MAX)
    {
      priv->size=COMFORT_MAX;
      priv->tile = g_realloc (priv->tile, sizeof (gint) * 2 * priv->size);
    }
}

static gboolean contains (gint array[], gint n, gint x, gint y)
{
  gint i;
  for (i=0; i<n; i++)
    {
      if (array[i*2+0] == x &&
          array[i*2+1] == y)
        {
          return TRUE;
        }
    }
  return FALSE;
}

gboolean
canvas_is_dirty (Canvas *canvas,
                 gint    x,
                 gint    y)
{
  DirtyPriv *priv = canvas->dirty;

  if (priv->count+1 >= priv->size)
    return FALSE;
  return contains (priv->tile, priv->count, x, y);
}


gboolean coord_is_user (gint x, gint y)
{
  gint tmiddle = tile_tile(MIDDLE);

  while (tmiddle < x && tmiddle < y)
    {
      tmiddle += tmiddle/2;
    }
  return !(x > tmiddle || y > tmiddle);
}



/* returns TRUE if something was done */
gboolean
canvas_idle (Canvas *canvas)
{
  CachePriv *cache_priv = canvas->cache;
  gboolean did_something = FALSE;

  did_something |= tile_cache_wash (cache_priv->cache);
  did_something |= tile_cache_wash (cache_priv->system_cache);

  return did_something;
}

gint
canvas_store_tile (Canvas *canvas,
                   Tile   *tile)
{
  gchar buf[512];
  gchar id[128];

  g_assert (canvas); 
  if (!canvas || !tile)
    return -1;

  make_id (id, tile_get_x (tile), tile_get_y (tile));

  sprintf (buf, "%s/%s.png", ((FetcherPriv*)canvas->fetcher)->path, id);
  tile_save (tile, buf);
  return 0;
}

Tile *
cacher_get_tile (Canvas *canvas,
                 gint    x,
                 gint    y)
{
  Tile      *tile = NULL;
  CachePriv *cache = canvas->cache;

  g_assert (canvas);


  if (y >= tile_tile (ALLOCATOR_Y) &&
      x <= tile_tile (MIDDLE))
    {
      tile = tile_cache_get_tile (cache->system_cache, x, y);
    }
  else
    {
      tile = tile_cache_get_tile (cache->cache, x, y);
    }
  
  if (tile)
    return tile;
  
  tile = fetcher_get_tile (canvas, x, y);

  if (tile)
    {
      if (y >= tile_tile(ALLOCATOR_Y) &&
          x <= tile_tile(MIDDLE))
        {
          tile_cache_insert (cache->system_cache, tile);
          /* transfer ownership to cache */
          /* g_object_unref (G_OBJECT (tile)); */
        }
      else
        {
          tile_cache_insert (cache->cache, tile);
          /* transfer ownership to cache */
          /* g_object_unref (G_OBJECT (tile)); */
        }
    }
  return tile;
}

Tile *
canvas_get_tile (Canvas *canvas,
                 gint    x,
                 gint    y)
{
  Tile      *tile = NULL;
  //return fetcher_get_tile (canvas, x, y);
  tile = cacher_get_tile (canvas, x, y);
  return tile;
}



#include "scale.h"

void
canvas_update_zoom (Canvas *canvas)
{
  DirtyPriv *dirty_priv = canvas->dirty;

  if (!dirty_priv)
    return;

  gint levels = SUBDIVIDE_LEVELS;
  gint i;
  gint *zoom = g_alloca (sizeof (gint) * 2 * dirty_priv->size);
  gint n_zoom = 0;
  gint *dirty = g_alloca (sizeof (gint) * 2 * dirty_priv->size);
  gint n_dirty = 0;

  for (i=0; i<dirty_priv->count; i++)
    {
      gint x = dirty_priv->tile[i*2+0];
      gint y = dirty_priv->tile[i*2+1];

      /* do not create scaled down version of system region of canvas */
      if (y>=MIDDLE)
        continue;

      gint zx = (tile_tile(TILE_ZOOM_START)*2 + x)/2;
      gint zy = (tile_tile(TILE_ZOOM_START)*2 + y)/2;
      
      if (!contains (zoom, n_zoom, zx, zy))
        {
          zoom[n_zoom*2+0]=zx;
          zoom[n_zoom*2+1]=zy;
          n_zoom++;
        }
    }

  memcpy (dirty, dirty_priv->tile, dirty_priv->count * sizeof(gint) * 2);
  n_dirty = dirty_priv->count;

  while(levels--)
    {
      gint *zoomB = g_alloca (sizeof (gint) * 2 * dirty_priv->size);
      gint n_zoomB = 0;

      for (i=0; i<n_zoom; i++)
        {
          Tile *dst;
         
          Tile *src[4]={NULL,NULL,NULL,NULL};
          gint x,y;
          gint sx,sy;
         
          x = zoom[i*2+0]; 
          y = zoom[i*2+1];

          sx = x * 2 - tile_tile(TILE_ZOOM_START)*2;
          sy = y * 2 - tile_tile(TILE_ZOOM_START)*2;

          dst = canvas_get_tile (canvas, x,y);

          if (contains (dirty, n_dirty, sx+0, sy+0))
           {
             src[0] = canvas_get_tile (canvas, sx+0, sy+0);
           }
          if (contains (dirty, n_dirty, sx+1, sy+0))
           {
             src[1] = canvas_get_tile (canvas, sx+1, sy+0);
           }
          if (contains (dirty, n_dirty, sx+0, sy+1))
           {
             src[2] = canvas_get_tile (canvas, sx+0, sy+1);
           }
          if (contains (dirty, n_dirty, sx+1, sy+1))
           {
             src[3] = canvas_get_tile (canvas, sx+1, sy+1);
           }

          scale (src, dst);
          {
            gint i;
            for (i=0; i<4; i++)
              if (src[i])
                g_object_unref (G_OBJECT (src[i]));
          }
          g_object_unref (G_OBJECT (dst));

          {
            gint zx = (tile_tile(TILE_ZOOM_START)*2 + x)/2;
            gint zy = (tile_tile(TILE_ZOOM_START)*2 + y)/2;

            if (!contains(zoomB, n_zoomB, zx,zy))
              {
                zoomB[n_zoomB*2+0]=zx;
                zoomB[n_zoomB*2+1]=zy;
                n_zoomB++;
              }
          }
        }
      memcpy (dirty, zoom, sizeof(gint) * 2 * n_zoom);
      n_dirty = n_zoom;
      memcpy (zoom, zoomB, sizeof(gint) * 2 * n_zoomB);
      n_zoom = n_zoomB;
    }
}



void
canvas_pick (Canvas *canvas,
             gint    x,
             gint    y,
             guint  *r,
             guint  *g,
             guint  *b)
{
  Tile *tile;

  gint tile_width = canvas_get_tile_width (canvas);
  gint tile_height = canvas_get_tile_height (canvas);
  
  gint tile_x = x / tile_width;
  gint tile_y = y / tile_height;

  if (x<0)
    tile_x -=1;
  if (y<0)
    tile_y -=1;

    {
      gint tx,ty;
      guchar *data;

      tx = x % tile_width;
      ty = y % tile_height;

      if (x<0)
        tx += tile_width;
      if (y<0)
        ty += tile_height;

      /* note: we are not referencing the tile, this should nevertheless work
       * since we assume a single thread of operation and that the canvas will
       * have a cached entry for this tile */
      tile = canvas_get_tile (canvas, tile_x, tile_y);

      data = tile_get_data (tile) + tile_width * ty * 4 + tx * 4;
      *r = data[0] * 256;
      *g = data[1] * 256;
      *b = data[2] * 256;
    }
}



void
canvas_outline_box (Canvas *canvas,
                    gint    x0,
                    gint    y0,
                    gint    width,
                    gint    height,
                    gint    red,
                    gint    green,
                    gint    blue,
                    gint    alpha)
{
  canvas_fill_rect (canvas, x0, y0, width, 1, red, green, blue, alpha);
  canvas_fill_rect (canvas, x0, y0 + height, width, 1, red, green, blue, alpha);
  canvas_fill_rect (canvas, x0, y0, 1, height, red, green, blue, alpha);
  canvas_fill_rect (canvas, x0 + width, y0, 1, height, red, green, blue, alpha);
}


void
canvas_fill_rect (Canvas *canvas,
                  gint    x,
                  gint    y,
                  gint    width,
                  gint    height,
                  gint    red,
                  gint    green,
                  gint    blue,
                  gint    alpha)
{
  guchar r = red*255/65536;
  guchar g = green*255/65536;
  guchar b = blue*255/65536;
  guchar a = alpha*255/65536;
  guchar *buf = g_malloc (4 * width * height);
  if (buf)
    {
      CanvasRect *rect = canvas_rect_new (canvas, x, y, width, height);
      gint i;
      for (i=0;i<width*height;i++)
        {
          buf[i*4 + 0] = r;
          buf[i*4 + 1] = g;
          buf[i*4 + 2] = b;
          buf[i*4 + 3] = a;
        }
      canvas_rect_import_buf (rect, buf);
      g_object_unref (rect); 
      g_free (buf);
    }
}

