/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#ifndef _TILE_CACHE_H
#define _TILE_CACHE_H

#include "types.h"

struct _TileCache
{
  GObject parent_object;
  GSList *list;
  gint    size;
  gint    hits;
  gint    misses;

  gint    wash_percentage;
};

struct _TileCacheClass
{
  GObjectClass parent_class;
};

GType       tile_cache_get_type            (void) G_GNUC_CONST;

TileCache * tile_cache_new                 (void);

void        tile_cache_set_size            (TileCache *cache,
                                            gint       size);

gint        tile_cache_get_size            (TileCache *cache);


void        tile_cache_set_wash_percentage (TileCache *cache,
                                            gint       wash_percentage);

gint        tile_cache_get_wash_percentage (TileCache *cache);

gboolean    tile_cache_wash                (TileCache *cache);

Tile      * tile_cache_get_tile            (TileCache *cache,
                                            gint       x,
                                            gint       y);

gboolean    tile_cache_has_tile            (TileCache *cache,
                                            gint       x,
                                            gint       y);

void        tile_cache_insert              (TileCache *cache,
                                            Tile      *tile);

#endif
