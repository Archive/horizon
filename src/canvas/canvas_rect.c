/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#define _CANVAS_RECT_C

#include <glib.h>
#include "config.h"
#include "canvas.h"
#include "canvas_rect.h"

static void finalize (GObject *object);

G_DEFINE_TYPE(CanvasRect, canvas_rect, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
  (* G_OBJECT_CLASS (canvas_rect_parent_class)->finalize) (object);
}

enum 
{
  PROP_0,
  PROP_X,
  PROP_Y,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_CANVAS,
  PROP_LAST
};

static void
get_property (GObject    *gobject,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
  CanvasRect *canvas_rect = CANVAS_RECT (gobject);
  switch(property_id)
    {
      case PROP_X:
        g_value_set_int (value, canvas_rect->x);
        break;
      case PROP_Y:
        g_value_set_int (value, canvas_rect->y);
        break;
      case PROP_WIDTH:
        g_value_set_int (value, canvas_rect->width);
        break;
      case PROP_HEIGHT:
        g_value_set_int (value, canvas_rect->height);
        break;
      case PROP_CANVAS:
        g_value_set_object (value, canvas_rect->canvas);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, property_id, pspec);
        break;
    }
}

static void
set_property (GObject      *gobject,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
  CanvasRect *canvas_rect = CANVAS_RECT (gobject);
  switch(property_id)
    {
      case PROP_X:
        canvas_rect->x = g_value_get_int (value);
        break;
      case PROP_Y:
        canvas_rect->y = g_value_get_int (value);
        break;
      case PROP_WIDTH:
        canvas_rect->width = g_value_get_int (value);
        break;
      case PROP_HEIGHT:
        canvas_rect->height = g_value_get_int (value);
        break;
      case PROP_CANVAS:
        canvas_rect->canvas = g_value_get_object (value);
        return;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, property_id, pspec);
        break;
    }
}

static void
canvas_rect_class_init (CanvasRectClass *klass)
{
  GObjectClass  *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = finalize;
  gobject_class->set_property = set_property;
  gobject_class->get_property = get_property;

   g_object_class_install_property (gobject_class, PROP_X,
                                    g_param_spec_int ("x",
                                                      "x",
                                                      "x" " of " "CanvasRect",
                                                      0, G_MAXINT, 0,
                                                      G_PARAM_READWRITE));
   g_object_class_install_property (gobject_class, PROP_Y,
                                    g_param_spec_int ("y",
                                                      "y",
                                                      "y" " of " "CanvasRect",
                                                      0, G_MAXINT, 0,
                                                      G_PARAM_READWRITE));
   g_object_class_install_property (gobject_class, PROP_WIDTH,
                                    g_param_spec_int ("width",
                                                      "width",
                                                      "width" " of " "CanvasRect",
                                                      0, G_MAXINT, 0,
                                                      G_PARAM_READWRITE));
   g_object_class_install_property (gobject_class, PROP_HEIGHT,
                                    g_param_spec_int ("height",
                                                      "height",
                                                      "height" " of " "CanvasRect",
                                                      0, G_MAXINT, 0,
                                                      G_PARAM_READWRITE));
  
   g_object_class_install_property (gobject_class, PROP_CANVAS,
                                   g_param_spec_object ("canvas",
                                                        "Canvas",
                                                        "The canvas this rect is a portion of",
                                                        G_TYPE_OBJECT,
                                                        G_PARAM_CONSTRUCT |
                                                        G_PARAM_READWRITE));
                                      
}

static void
canvas_rect_init (CanvasRect* canvas_rect)
{
  canvas_rect->x      = 0;
  canvas_rect->y      = 0;
  canvas_rect->height = 0;
  canvas_rect->height = 0;
  canvas_rect->canvas = NULL;
}

CanvasRect *
canvas_rect_new (Canvas *canvas,
                 gint    x,
                 gint    y,
                 gint    width,
                 gint    height)
{
  CanvasRect *canvas_rect;
 
  canvas_rect = g_object_new (CANVAS_TYPE_RECT, NULL);
  canvas_rect->x      = x;
  canvas_rect->y      = y;
  canvas_rect->width  = width;
  canvas_rect->height = height;
  canvas_rect->canvas = canvas;
  return canvas_rect; 
}


/* NB! keep changes in sync with export method */
void
canvas_rect_import_buf (CanvasRect *rect,
                        guchar     *src)
{
  Canvas *canvas = rect->canvas;
  gint    width  = rect->width;
  gint    height = rect->height;
  gint    x      = rect->x;
  gint    y      = rect->y;

  gint tile_width  = canvas_get_tile_width (canvas);
  gint tile_height = canvas_get_tile_height (canvas);
  gint srcy = 0;

  while (srcy < height)
    {
      gint srcx = 0;
      gint dsty = srcy + y;
      gint offsety = tile_local (dsty);

      while (srcx < width)
        {
          Tile *tile;
          gint dstx = x + srcx;

          gint offsetx = tile_local (dstx);

          if (offsetx<0)
            offsetx += tile_width;

          /** **/

          tile = canvas_get_tile (canvas, tile_tile (dstx), tile_tile (dsty));

          tile_lock (tile);

          guchar *dst = tile_get_data (tile);
            {
              gint row;
              for (row = offsety; row<tile_height &&
                                  row+srcy-offsety < height; row++)
                {
                  guint *sp = (guint*)src + ((srcy + (row - offsety)) * width + srcx);
                  guint *dp = (guint*)dst + (row * tile_width + offsetx);

                  gint i;
                  for (i = offsetx; i<tile_width &&
                                    i+srcx-offsetx < width; i++)
                    {
                      *dp++=*sp++;
                    }
                }
            }
          tile_unlock (tile);
          g_object_unref (G_OBJECT (tile));
          
          /** **/

          if (offsetx)
            {
              srcx += tile_width - offsetx;
            }
          else
            {
              srcx += tile_width;
            }
        }

      if (offsety)
        {
          srcy += tile_height - offsety;
        }
      else
        {
          srcy += tile_height;
        }
    }

}

/* NB! keep changes in sync with write method */
void
canvas_rect_export_buf (CanvasRect *rect,
                        guchar     *dst)
{
  Canvas *canvas = rect->canvas;
  gint    width  = rect->width;
  gint    height = rect->height;
  gint    x      = rect->x;
  gint    y      = rect->y;

  gint tile_width  = canvas_get_tile_width (canvas);
  gint tile_height = canvas_get_tile_height (canvas);
  gint dsty = 0;

  while (dsty < height)
    {
      gint dstx = 0;
      gint srcy = dsty + y;
      gint offsety = tile_local (srcy);

      while (dstx < width)
        {
          Tile *tile;
          gint srcx = x + dstx;

          gint offsetx = tile_local (srcx);

          /** **/

          tile = canvas_get_tile (canvas, tile_tile (srcx), tile_tile (srcy));

          guchar *src = tile_get_data (tile);
            {
              gint row;
              for (row = offsety; row<tile_height &&
                                  row+dsty-offsety < height; row++)
                {
                  guint *dp = (guint*)dst + ((dsty + (row - offsety)) * width + dstx);
                  guint *sp = (guint*)src + (row * tile_width + offsetx);

                  gint i;
                  for (i = offsetx; i<tile_width &&
                                    i+dstx-offsetx < width; i++)
                    {
                      *dp++=*sp++;
                    }
                }
            }
          g_object_unref (G_OBJECT (tile));
          
          /** **/

          if (offsetx)
            {
              dstx += tile_width - offsetx;
            }
          else
            {
              dstx += tile_width;
            }
        }

      if (offsety)
        {
          dsty += tile_height - offsety;
        }
      else
        {
          dsty += tile_height;
        }
    }
}
