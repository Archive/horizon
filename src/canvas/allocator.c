/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2006 Eero Tanskanen <yendor@nic.fi>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <png.h>
#include <glib.h>
#include <glib/gprintf.h>
#include "allocator.h"
#include "tile.h"
#include "canvas.h"
#include "map.h"
#include "canvas_rect.h"

struct _CanvasAllocation
{
  CanvasRect  canvas_rect;
};

typedef struct _CanvasAllocationClass CanvasAllocationClass;

struct _CanvasAllocationClass
{
  CanvasRectClass parent_class;
};

G_DEFINE_TYPE (CanvasAllocation, canvas_allocation, CANVAS_TYPE_RECT);


static void finalize (GObject *object)
{
  CanvasRect *rect = CANVAS_RECT (object);
  Canvas     *canvas = rect->canvas;

  gint row, col;
  gint tile_x = tile_tile (rect->x);
  gint tile_y = tile_tile (rect->y);

  /* upon finalization of an allocation there is no longer any point in
   * writing the tiles to disk
   */
  
  for (row = 0; row * canvas_get_tile_height (canvas) < rect->height; row ++)
    for (col = 0; col * canvas_get_tile_width (canvas) < rect->height; col ++)
      {
        if (canvas_has_tile_in_memory (canvas, tile_x + col, tile_y + row))
          {
            Tile *tile = canvas_get_tile (canvas, tile_x + col, tile_y + row);
            tile_void (tile);
          }
      }

  G_OBJECT_CLASS (canvas_allocation_parent_class)->finalize (object);
}

static void
canvas_allocation_class_init (CanvasAllocationClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->finalize = finalize;
}

static void
canvas_allocation_init (CanvasAllocation *canvas_allocation)
{
}

static gint requested_tiles = 0;

Allocation *canvas_alloc (Canvas  *canvas,
                          gint     width,
                          gint     height)
{
  gint tile_x = tile_tile (ALLOCATOR_X) + requested_tiles;
  gint tile_y = tile_tile (ALLOCATOR_Y + 256); /* offset since some widgets
                                                  draw outside allocation,
                                                  making sure caches are not
                                                  mixed */
  
  gint x_tiles_allocated = width / canvas_get_tile_width (canvas) + 1;
  
  requested_tiles += x_tiles_allocated;

  return g_object_new (CANVAS_TYPE_ALLOCATION,
                       "canvas", canvas,
                       "width", width,
                       "height", height,
                       "x", tile_x * canvas_get_tile_width (canvas),
                       "y", tile_y * canvas_get_tile_width (canvas),
                       NULL);
}
