#include "tile.h"
#include "config.h"


static inline void
downscale (guchar *src_data,
           gint    src_width,
           gint    src_height,
           gint    src_rowstride,
           guchar *dst_data,
           gint    dst_rowstride)
{
  gint y;
  if (!src_data || !dst_data)
    return;
  for (y=0; y<src_height/2; y++)
    {
       gint x;
       guchar *dst = dst_data + y*dst_rowstride;
       guchar *src = src_data + y*2*src_rowstride;
       
       for (x=0; x<src_width/2; x++)
         {
            int i;
            for (i=0; i<4; i++)
              dst[i] = (src[i] + src[i+4] + src[i + src_rowstride] + src[i + src_rowstride + 4]) / 4;
            
            dst+=4;
            src+=4*2;
         }
    }
}

#define tile_width   TILE_SIZE
#define tile_height  TILE_SIZE
#define rowstride    (tile_width*4)

void
scale (Tile *src[4], Tile *dst)
{
  guchar *dst_data;

  tile_lock (dst);

  dst_data = tile_get_data (dst);
  if (src[0])
    {
      downscale (tile_get_data(src[0]), tile_width, tile_height, rowstride,
                 dst_data, rowstride);
    }
  if (src[1])
    {
      downscale (tile_get_data(src[1]), tile_width, tile_height, rowstride,
                 dst_data + (tile_width/2)*4, rowstride);
    }
  if (src[2])
    {
      downscale (tile_get_data(src[2]), tile_width, tile_height, rowstride,
                 dst_data + rowstride * tile_height/2, rowstride);
    }
  if (src[3])
    {
      downscale (tile_get_data(src[3]), tile_width, tile_height, rowstride,
                 dst_data + rowstride * tile_height/2 + (tile_width/2*4), rowstride);
    }
  tile_unlock (dst);
  return;
}
