/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef _UNDO_H
#define _UNDO_H

typedef struct _UndoStack UndoStack;

#include <glib.h>
#include "canvas.h"
#include "tile.h"

/* create a undo_stack storing undo tiles in
 * specified canvas
 */
UndoStack *undo_stack_new       (Canvas    *canvas);

void       undo_stack_destroy   (UndoStack *stack);
void       undo_stack_new_step  (UndoStack *stack);

gint       undo_stack_get_undos (UndoStack *stack);
gint       undo_stack_get_redos (UndoStack *stack);

void       undo_stack_undo      (UndoStack *stack);
void       undo_stack_redo      (UndoStack *stack);

void       undo_stack_add_tile  (UndoStack *stack,
                                 Tile      *tile);

void       undo_stack_freeze    (UndoStack *stack);
void       undo_stack_thaw      (UndoStack *stack);

#endif
