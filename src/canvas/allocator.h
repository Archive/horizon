/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef SYSTEM_TILE_ALLOCATOR_H
#define SYSTEM_TILE_ALLOCATOR_H

#include <glib-object.h>

G_BEGIN_DECLS

#define CANVAS_TYPE_ALLOCATION            (canvas_allocation_get_type ())
#define CANVAS_ALLOCATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CANVAS_TYPE_ALLOCATION, CanvasAllocation))
#define CANVAS_ALLOCATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CANVAS_TYPE_ALLOCATION, CanvasAllocationClass))
#define CANVAS_IS_ALLOCATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CANVAS_TYPE_ALLOCATION))
#define CANVAS_IS_ALLOCATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CANVAS_TYPE_ALLOCATION))
#define CANVAS_ALLOCATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CANVAS_TYPE_ALLOCATION, CanvasAllocationClass))

#include "canvas.h"

GType canvas_allocation_get_type (void) G_GNUC_CONST;

typedef struct _CanvasAllocation Allocation;
typedef struct _CanvasAllocation CanvasAllocation;

Allocation *canvas_alloc (Canvas  *canvas,
                          gint     width,
                          gint     height);

G_END_DECLS

#endif
