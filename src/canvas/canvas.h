/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <glib-object.h>

#include "types.h"
#include "tile.h"

#ifndef _CANVAS_H
#define _CANVAS_H

#include "map.h"
#include "undo.h"

/*
CanvasSwap (path) -> blank=NULL
  |
CanvasBuffered (canvas) -> blank = NULL
  |
CanvasSubrect (canvas, x, y, width, height) blank = blank
  |

*/


struct _Canvas
{
  GObject    parent_object;

  /* private */

  gshort     tile_width;
  gshort     tile_height;
  void      *format;
  
  void      *fetcher;
  void      *cache;
  void      *undo;
  void      *dirty;
  void      *empty;
  void      *abyss;

  /*Tile      *blank_tile;//     <- private (sparse_fun)
                        //               abyss_fun*/
};

struct _CanvasClass
{
  GObjectClass parent_class;
};

GType   canvas_get_type (void) G_GNUC_CONST;

Canvas *canvas_new ();

void canvas_set_path (Canvas     *canvas,
                      const char *path);
const char * canvas_get_path (Canvas     *canvas);


gint canvas_store_tile (Canvas *canvas,
                        Tile   *tile);

Tile *
canvas_get_tile (Canvas *canvas,
                 gint    x,
                 gint    y);

#define canvas_get_tile_width(canvas) ((canvas)->tile_width)
#define canvas_get_tile_height(canvas) ((canvas)->tile_height)
#define canvas_get_format(canvas) ((canvas)->format)


void
canvas_flush_dirty (Canvas *canvas);

void
canvas_add_dirty (Canvas *canvas,
                  gint    x,
                  gint    y);

gboolean
canvas_is_dirty (Canvas *canvas,
                 gint    x,
                 gint    y);

void
canvas_set_undo_stack (Canvas    *canvas,
                       UndoStack *undo_stack);

UndoStack *
canvas_get_undo_stack (Canvas    *canvas);

gboolean
canvas_idle (Canvas *canvas);

gboolean
canvas_has_tile_in_memory (Canvas *canvas,
                           gint    x,
                           gint    y);

void
canvas_update_zoom (Canvas *canvas);

gboolean
canvas_prefetch (Canvas    *canvas,
                 gint       x0,
                 gint       y0,
                 gint       width,
                 gint       height,
                 gint       scale,
                 gboolean   do_context);


void
canvas_pick (Canvas *canvas,
             gint    x,
             gint    y,
             guint  *r,
             guint  *g,
             guint  *b);

void
canvas_fill_rect (Canvas *canvas,
                  gint    x,
                  gint    y,
                  gint    width,
                  gint    height,
                  gint    red,
                  gint    green,
                  gint    blue,
                  gint    alpha);

void
canvas_outline_box (Canvas *canvas,
                    gint    x0,
                    gint    y0,
                    gint    width,
                    gint    height,
                    gint    red,
                    gint    green,
                    gint    blue,
                    gint    alpha);


#include "allocator.h"
#include "canvas_rect.h"

#endif
