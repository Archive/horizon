/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef TILE_H
#define TILE_H

#include <glib-object.h>

#include "types.h"

struct _Tile
{
  GObject        parent_object;
  gshort         width, height;
  void          *format;
  guchar        *data;

  gint           lock;        /* number of times the tile is write locked
                               * should in theory just have the values 0/1
                               */

  Canvas        *canvas;
  gint           x, y;        /* tile-coordinates, IMPORTANT: must be first in struct */

  glong          rev;         /* this tile revision */
  glong          stored_rev;  /* what revision was we when we from storage?
                                 (currently set to 1 when loaded from disk */

  /* the shared list is a doubly linked circular list */
  Tile          *next_shared;
  Tile          *prev_shared;
};

struct _TileClass
{
  GObjectClass   parent_class;
};

GType  tile_get_type (void) G_GNUC_CONST;

Tile   * tile_new          (gint          width,
                            gint          height,
                            void         *babl_format);

guchar * tile_get_data      (Tile        *tile);

gint     tile_get_width     (Tile        *tile);

gint     tile_get_height    (Tile        *tile);

void   * tile_get_format    (Tile        *tile);

gint     tile_save          (Tile        *tile,
                             const gchar *path);

Tile   * tile_load          (const gchar *path);

gint     tile_get_x         (const Tile *tile);

gint     tile_get_y         (const Tile *tile);

void     tile_set_x         (Tile       *tile,
                             gint        x);

void     tile_set_y         (Tile       *tile,
                             gint        x);

Canvas * tile_get_canvas    (Tile       *tile);

void     tile_set_canvas    (Tile       *tile,
                             Canvas     *canvas);

void     tile_lock          (Tile       *tile);
void     tile_unlock        (Tile       *tile);


gboolean tile_is_stored     (Tile       *tile);
void     tile_void          (Tile       *tile);


Tile    *tile_dup           (Tile       *tile);

/* utility functions used by undo system */
void    tile_swp            (Tile       *a,
                             Tile       *b);

void    tile_cpy            (Tile       *src,
                             Tile       *dst);

#endif
