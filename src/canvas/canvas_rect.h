/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#ifndef CANVAS_RECT_H
#define CANVAS_RECT_H

#include <glib-object.h>

#include "types.h"

G_BEGIN_DECLS

#define CANVAS_TYPE_RECT            (canvas_rect_get_type ())
#define CANVAS_RECT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CANVAS_TYPE_RECT, CanvasRect))
#define CANVAS_RECT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CANVAS_TYPE_RECT, CanvasRectClass))
#define CANVAS_IS_RECT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CANVAS_TYPE_RECT))
#define CANVAS_IS_RECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CANVAS_TYPE_RECT))
#define CANVAS_RECT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CANVAS_TYPE_RECT, CanvasRectClass))

struct _CanvasRect
{
  GObject        parent_object;
  Canvas        *canvas;
  gint           x, y; 
  gint           width, height;
};

struct _CanvasRectClass
{
  GObjectClass   parent_class;
};

GType canvas_rect_get_type (void) G_GNUC_CONST;

CanvasRect * canvas_rect_new (Canvas *canvas,
                              gint    x,
                              gint    y,
                              gint    width,
                              gint    height);

void canvas_rect_import_buf (CanvasRect *rect,
                             guchar     *src);

void canvas_rect_export_buf (CanvasRect *rect,
                             guchar     *dst);

G_END_DECLS

#endif
