/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/
#include <glib.h>
#include <glib/gprintf.h>
#include <unistd.h>
#include "string.h" /* memcpy */
#include "config.h"
#include "tile.h"
#include "canvas.h"
#include "png_util.h"

/*

The infinite canvas is not really infinite, the canvas is created
by tiles:

*/

/* macros for coordinate transforms */
#define local_to_global(local_x,local_y)                   \
          do{                                              \
             local_x= tile->x * tile->width  + local_x;    \
             local_y= tile->y * tile->height + local_y;    \
          }
#define global_to_local(global_x,global_y)                 \
          do{                                              \
             global_x = global_x - tile->x * tile->width;  \
             global_y = global_y - tile->y * tile->height; \
          }

static GObjectClass *parent_class = NULL;

static void
tile_finalize (GObject *object)
{
  Tile *tile = (Tile *) object;

  if (tile->stored_rev != tile->rev)
  {
    canvas_store_tile (tile->canvas, tile);
  }

  if (tile->next_shared == tile)
    {
      if (tile->data)  /* no clones */
        g_free (tile->data);
    }
  else
    {
      tile->prev_shared->next_shared = tile->next_shared;
      tile->next_shared->prev_shared = tile->prev_shared;
    }

  (* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
tile_class_init (TileClass *class)
{
  GObjectClass *gobject_class;

  gobject_class = (GObjectClass*) class;

  parent_class = g_type_class_peek_parent (class);

  gobject_class->finalize = tile_finalize;
}

static void
tile_init (Tile* tile)
{
  tile->rev = 1;
  tile->lock = 0;
  tile->width  = 0;
  tile->height = 0;
  tile->canvas = NULL;
  tile->format = NULL;
  tile->data   = NULL;

  tile->next_shared = tile;
  tile->prev_shared = tile;
}

GType
tile_get_type (void)
{
  static GType type = 0;

  if (type == 0)
  {
    static const GTypeInfo info =
    {
      sizeof (TileClass),
      (GBaseInitFunc)     NULL,
      (GBaseFinalizeFunc) NULL,
      (GClassInitFunc)    tile_class_init,
      NULL,               /* class_finalize */
      NULL,               /* class_data */
      sizeof (Tile),
      0,                  /* n_preallocs */
      (GInstanceInitFunc) tile_init
    };

    type = g_type_register_static (G_TYPE_OBJECT,
                                   "Tile", &info, 0);
  }

  return type;
}

static Tile *
tile_new_from_data (int width,
                    int height,
                    void *format,
                    unsigned char *data
                   )
{
  Tile *tile = g_object_new (tile_get_type(), NULL);

  tile->width = width;
  tile->height = height;
  tile->data = data;

  return tile;
}

Tile *
tile_dup (Tile *src)
{
  Tile *tile = g_object_new (tile_get_type(), NULL);

  tile->width      = src->width;
  tile->height     = src->height;
  tile->rev        = 1;
  tile->stored_rev = 1;
  tile->canvas     = src->canvas;
  tile->format     = src->format;
  tile->data       = src->data;
  
  tile->next_shared = src->next_shared;
  src->next_shared = tile;
  tile->prev_shared = src;
  tile->next_shared->prev_shared = tile;

  return tile;
}

Tile *
tile_new (int   width,
          int   height,
          void *babl_format)
{
  Tile *tile;
  unsigned char *data = g_malloc0 (width * 4 * height);
  tile= tile_new_from_data (width, height, babl_format, data);

  tile->stored_rev = 1;
  return tile;
}

static void
tile_unclone (Tile *tile)
{
  if (tile->next_shared != tile)
    {
      gint buflen = tile->width * tile->height * 4;
      /* the tile data is shared with other tiles,
       * create a local copy
       */
      guchar *data = g_malloc (buflen);
      memcpy (data, tile->data, buflen);
      tile->data = data;
      tile->prev_shared->next_shared = tile->next_shared;
      tile->next_shared->prev_shared = tile->prev_shared;
      tile->prev_shared = tile;
      tile->next_shared = tile;
      
      canvas_add_dirty (tile->canvas, tile->x, tile->y);
    }
}

static gint total_locks=0;
static gint total_unlocks=0;

void
tile_lock (Tile *tile)
{
  if (tile->lock != 0)
    {
      g_warning ("locking a tile for the second time");
    }
  total_locks++;
  tile->lock++;
  /*fprintf (stderr, "global tile locking: %i %i\n", locks, unlocks);*/

  tile_unclone (tile);
}

void
tile_unlock (Tile *tile)
{
  total_unlocks++;
  tile->lock--;
  if (tile->lock < 0)
    {
      g_warning ("unlocked a tile with lock count < 0");
    }
  if (tile->lock == 0)
    {
      canvas_add_dirty (tile->canvas, tile->x, tile->y);
      tile->rev++;
    }
}


gboolean
tile_is_stored (Tile *tile)
{
  return tile->stored_rev == tile->rev;
}

void
tile_void (Tile       *tile)
{
  tile->stored_rev = tile->rev;
}

void
tile_cpy (Tile *src,
          Tile *dst)
{
  tile_lock (dst);
  
  g_free (dst->data);
  dst->data = NULL;
  
  dst->next_shared = src->next_shared;
  src->next_shared = dst;
  dst->prev_shared = src;
  dst->next_shared->prev_shared = dst;

  dst->data = src->data;

  tile_unlock (dst);
}

void
tile_swp (Tile *a,
          Tile *b)
{
  guchar *tmp;
 
  tile_unclone (a);
  tile_unclone (b);

  g_assert (a->width == b->width &&
            a->height == b->height);

  tmp = a->data;
  a->data = b->data;
  b->data = tmp;
}

unsigned char *
tile_get_data (Tile *tile)
{
  return tile->data;
}

int
tile_get_width (Tile *tile)
{
  return tile->width;
}

int
tile_get_height (Tile *tile)
{
  return tile->height;
}

void *
tile_get_format (Tile *tile)
{
  return tile->format;
}

#include <stdio.h>
#include <glib/gstdio.h>

static void build_paths (const gchar *file_path,
                         gint         levels)
{
  gint i;
  gchar *path[10];

  path[levels] = (gchar *)file_path;
  for (i=levels-1;i>=0;i--)
    {
      path[i] = g_strdup (path[i+1]);
      *strrchr(path[i], '/')='\0';
    } 

  for (i=0;i<levels;i++)
    {
      if (!g_file_test (path[i], G_FILE_TEST_IS_DIR))
       {
        if (g_mkdir (path[i], 0777))
          {
            g_warning ("failed to create tile storage dir %s", path[i]);
            return;
          }
       }
      g_free (path[i]);
    }
}

int
tile_save (Tile        *tile,
           const gchar *path)
{
  int error;
#ifdef TILE_IO_NO_SAVE
  return 0;
#endif

  
  error  = horizon_save_png (path, tile->width, tile->height, tile->width * 4, tile->data);
#ifdef TILE_IO_LOG
  fprintf (stderr, "S:%s\n", path);
#endif

  if (error)
    {
      build_paths (path, 3);
      error  = horizon_save_png (path, tile->width, tile->height, tile->width * 4, tile->data);
    }
  
#ifdef TILE_IO_SLOW
  usleep (TILE_IO_SLOW_SAVE_DELAY);
#endif

  if (error)
    {
      g_warning ("failed writing tile %s", path);
    }

  if (!error)
    {
       tile->stored_rev = tile->rev;
    }
  return error;
}

Tile *
tile_load (const gchar *path)
{
  Tile *tile = NULL;
  int width, height, row_stride;
  unsigned char *data;
  if (horizon_load_png (path, &width, &height, &row_stride, &data))
    return NULL;
#ifdef TILE_IO_LOG
  fprintf (stderr, "L:%s\n", path);
#endif
#ifdef TILE_IO_SLOW
  usleep (TILE_IO_SLOW_LOAD_DELAY);
#endif

  tile = tile_new_from_data (width, height, NULL, data);
  tile->stored_rev = 1;
  tile->rev = 1;
  return tile;
}

gint
tile_get_x (const Tile *tile)
{
  return tile->x;
}
            
gint
tile_get_y (const Tile *tile)
{
  return tile->y;
}

void
tile_set_x (Tile *tile,
            gint  x)
{
  tile->x=x;
}

void
tile_set_y (Tile *tile,
            gint  y)
{
  tile->y=y;
}

Canvas *
tile_get_canvas (Tile *tile)
{
  return tile->canvas;
}

void
tile_set_canvas (Tile   *tile,
                 Canvas *canvas)
{
  tile->canvas=canvas;
}
