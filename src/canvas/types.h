#ifndef _TYPES_H
#define _TYPES_H

typedef struct _Tile            Tile;
typedef struct _TileClass       TileClass;
typedef struct _Canvas          Canvas;
typedef struct _CanvasClass     CanvasClass;
typedef struct _TileCache       TileCache;
typedef struct _TileCacheClass  TileCacheClass;
typedef struct _CanvasRect      CanvasRect;
typedef struct _CanvasRectClass CanvasRectClass;

#endif
