#ifndef _SCALE_H
#define _SCALE_H

#include "tile.h"

void
scale (Tile *src[4], Tile *dst);

#endif
