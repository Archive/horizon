/*  horizon, a sketch, note and scribbling application.
    Copyright (C) 2005 Øyvind Kolås <pippin@gimp.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
*/

/*
A coordinate 64 bits, 32 bits are used for the X coordinate and 32 bits are
used for the Y coordinate.

bit usage:

32                            1  
·······························
│├──────────────────┘├─────┘├─┘                                                     
││                   │      │
││                   │      └──  3 bit  subpixel precision
││                   └─────────  7 bit  tile pixel coordinates
│└───────────────────────────── 21 bit  tile index
└──────────────────────────────  1 bit  unused (sign bit)

Fig.2: region usage:

home, zoom and undo share the global largest tile
cache. Whilst the allocator (used for ui elements)
is a seperate cache to make tool switching faster.

0 --X----->
  ·  ·  ·  ·  ·  ·(home)┆  ·  ·  ·  ·  ·(reserved)
| ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  ·   
Y ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  ·   
| ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  · 
| ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  ·   
V ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  ·   
  ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  ·   
  ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·  ·  ·  ·  ·   
┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┼┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄
undo ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·| ·  ·(zoom)   
  ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·| ·  ·  ·  ·   
  ·  .  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·| ·  ·  ·  ·   
__·__·__·__·__·__·__·__·┆ _·__·__·__·|_·__·__·__·   
  ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·| ·  ·  ·  ·   
allocator  ·  ·  ·  ·  ·┆  ·  ·  ·  ·| ·  ·  ·  ·   
  ·  ·  ·  ·  ·  ·  ·  ·┆  ·  ·  ·  ·| ·  ·  ·  ·   
  ·  ·  ·  ·  ·  ·  ·  ·   ·  ·  ·  ·| ·  ·  ·  ·   


*/

#ifndef _MAP_H
#define _MAP_H

/* this is the only location where bit usage should be defined,
 * changing these values will make a stored canvas incompatible
 * with the newly compiled version
 */
#define TILE_SUB_PIXEL_BITS   3
#define TILE_LOCAL_BITS       7
#define TILE_INDEX_BITS      21

#define TILE_VALID_BITS      (TILE_LOCAL_BITS + TILE_INDEX_BITS)

#define TILE_LOCAL_MASK      ((1<<TILE_LOCAL_BITS)-1)
#define TILE_VALID_MASK      ((1<<TILE_VALID_BITS)-1)
#define TILE_SIZE            (1<<TILE_LOCAL_BITS)

#define tile_local(x) ((x)&TILE_LOCAL_MASK)
#define tile_tile(x)  (((x)>>TILE_LOCAL_BITS)&0xffffff)

#define SPP (1<<(TILE_SUB_PIXEL_BITS))
#define MIDDLE (1<< (TILE_VALID_BITS-1))

/* start position for user */
#define USER_X 65536
#define USER_Y 65536
#define USER_ZOOM 65536*1.0

#define TILE_ZOOM_START  MIDDLE

#define ALLOCATOR_X   (0)
#define ALLOCATOR_Y   (MIDDLE * 3/2)  /* offset to counter stampings that "overshoot",
                                          this avoids false undo steps to be recorded,
                                        and forces the data to be within the system
                                        allocator */

#endif /* _MAP_H */
