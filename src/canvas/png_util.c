#include <png.h>
#include <glib.h>
#include "png_util.h"

gint
canvas_import_png (Canvas      *canvas,
                   const gchar *path,
                   gint         dest_x,
                   gint         dest_y)
{
  gint           width;
  gint           height;
  gint           row_stride;
  png_uint_32    w;
  png_uint_32    h;
  FILE          *infile;
  png_structp    load_png_ptr;
  png_infop      load_info_ptr;
  unsigned char  header[8];
  guchar        *pixels;

  unsigned   int i;
  png_bytep  *row_p = NULL;

  infile = fopen (path, "rb");
  if (!infile)
    {
      return -1;
    }

  fread (header, 1, 8, infile);
  if (png_sig_cmp (header, 0, 8))
    {
      fclose (infile);
      g_warning ("%s is not a png file", path);
      return -1;
    }

  load_png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!load_png_ptr)
    {
      fclose (infile);
      return -1;
    }

  load_info_ptr = png_create_info_struct (load_png_ptr);
  if (!load_info_ptr)
    {
      png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
      fclose (infile);
      return -1;
    }

  if (setjmp (png_jmpbuf (load_png_ptr)))
    {
      png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
     if (row_p)
        g_free (row_p);
      fclose (infile);
      return -1;
    }

  png_init_io (load_png_ptr, infile);
  png_set_sig_bytes (load_png_ptr, 8);
  png_read_info (load_png_ptr, load_info_ptr);
  {
    int bit_depth;
    int color_type;

    png_get_IHDR (load_png_ptr, load_info_ptr, &w, &h, &bit_depth, &color_type, NULL, NULL, NULL);
    width = w;
    height = h;
    if ((bit_depth == 8) && (color_type == PNG_COLOR_TYPE_RGB))
      {
	png_set_add_alpha (load_png_ptr, 0xffffffff, 1);
      }
    else if (bit_depth != 8 || !(color_type == PNG_COLOR_TYPE_RGBA))
      {
        g_warning ("bpp or type mismatch");
        png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
        fclose (infile);
        return -1;
      }

    if (png_get_interlace_type (load_png_ptr, load_info_ptr) == PNG_INTERLACE_ADAM7)
      {
        g_warning ("not supporting interlaced pngs");
        png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
        fclose (infile);
        return -1;
      }
  }

  row_stride = w * 4;
  pixels = g_malloc0 (row_stride);

  for (i=0; i< h; i++)
    {
      CanvasRect *rect = canvas_rect_new (canvas, dest_x, dest_y + i, width, 1);
      png_read_rows (load_png_ptr, &pixels, NULL, 1);
      canvas_rect_import_buf (rect, pixels);
      g_object_unref (rect);
    }

  png_read_end (load_png_ptr, NULL);
  png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);

  fclose (infile);

  return 0;
}

gint
canvas_export_png (Canvas      *canvas,
                   const gchar *path,
                   gint         src_x,
                   gint         src_y,
                   gint         width,
                   gint         height)
{
  gint           row_stride = width * 4;
  FILE          *fp;
  gint           i;
  png_struct    *png;
  png_info      *info;
  guchar        *pixels;
  png_color_16   white;

  fp = fopen (path, "wb");
  if (!fp)
    {
      return -1;
    }
  
  png = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (png == NULL)
    return -1;

  info = png_create_info_struct (png);

  if (setjmp (png_jmpbuf (png)))
    return -1;
  png_set_compression_level (png, 1);
  png_init_io (png, fp);

  png_set_IHDR (png, info,
     width, height, 8, PNG_COLOR_TYPE_RGB_ALPHA,
     PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_DEFAULT);

  white.red = 0xff;
  white.blue = 0xff;
  white.green = 0xff;
  png_set_bKGD (png, info, &white);

  png_write_info (png, info);
  pixels = g_malloc0 (row_stride);

  for (i=0; i< height; i++)
    {
      CanvasRect *rect = canvas_rect_new (canvas, src_x, src_y + i, width, 1);
      canvas_rect_export_buf (rect, pixels);
      png_write_rows (png, &pixels, 1);
      g_object_unref (rect);
    }

  png_write_end (png, info);

  png_destroy_write_struct (&png, &info);
  g_free (pixels);
  fclose (fp);

  return 0;
}

gint
horizon_query_png (const gchar *path,
                   gint        *width,
                   gint        *height)
{
  png_uint_32   w;
  png_uint_32   h;
  FILE         *infile;
  png_structp   load_png_ptr;
  png_infop     load_info_ptr;
  unsigned char header[8];

  png_bytep  *row_p = NULL;

  infile = fopen (path, "rb");
  if (!infile)
    {
      return -1;
    }

  fread (header, 1, 8, infile);
  if (png_sig_cmp (header, 0, 8))
    {
      fclose (infile);
      g_warning ("%s is not a png file", path);
      return -1;
    }

  load_png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!load_png_ptr)
    {
      fclose (infile);
      return -1;
    }

  load_info_ptr = png_create_info_struct (load_png_ptr);
  if (!load_info_ptr)
    {
      png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
      fclose (infile);
      return -1;
    }

  if (setjmp (png_jmpbuf (load_png_ptr)))
    {
     png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
     if (row_p)
        g_free (row_p);
      fclose (infile);
      return -1;
    }

  png_init_io (load_png_ptr, infile);
  png_set_sig_bytes (load_png_ptr, 8);
  png_read_info (load_png_ptr, load_info_ptr);
  {
    int bit_depth;
    int color_type;

    png_get_IHDR (load_png_ptr, load_info_ptr, &w, &h, &bit_depth, &color_type, NULL, NULL, NULL);
    *width = w;
    *height = h;
    if ((bit_depth == 8) && (color_type == PNG_COLOR_TYPE_RGB))
      {
	png_set_add_alpha (load_png_ptr, 0xffffffff, 1);
      }
    else if (bit_depth != 8 || !(color_type == PNG_COLOR_TYPE_RGBA))
      {
        g_warning ("bpp or type mismatch");
        png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
        fclose (infile);
        return -1;
      }
  }
  png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
  return 0;
}

gint
horizon_load_png (const gchar  *path,
                  gint         *width,
                  gint         *height,
                  gint         *row_stride,
                  guchar      **pixels)
{
  png_uint_32   w;
  png_uint_32   h;
  FILE         *infile;
  png_structp   load_png_ptr;
  png_infop     load_info_ptr;
  unsigned char header[8];

  unsigned    int i;
  png_bytep  *row_p = NULL;

  infile = fopen (path, "rb");
  if (!infile)
    {
      return -1;
    }

  fread (header, 1, 8, infile);
  if (png_sig_cmp (header, 0, 8))
    {
      fclose (infile);
      g_warning ("%s is not a png file", path);
      return -1;
    }

  load_png_ptr =
    png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!load_png_ptr)
    {
      fclose (infile);
      return -1;
    }

  load_info_ptr = png_create_info_struct (load_png_ptr);
  if (!load_info_ptr)
    {
      png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
      fclose (infile);
      return -1;
    }

  if (setjmp (png_jmpbuf (load_png_ptr)))
    {
      png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
     if (row_p)
        g_free (row_p);
      fclose (infile);
      return -1;
    }

  png_init_io (load_png_ptr, infile);
  png_set_sig_bytes (load_png_ptr, 8);
  png_read_info (load_png_ptr, load_info_ptr);
  {
    int bit_depth;
    int color_type;

    png_get_IHDR (load_png_ptr, load_info_ptr, &w, &h, &bit_depth, &color_type, NULL, NULL, NULL);
    *width = w;
    *height = h;
    if ((bit_depth == 8) && (color_type == PNG_COLOR_TYPE_RGB))
      {
	png_set_add_alpha (load_png_ptr, 0xffffffff, 1);
      }
    else if (bit_depth != 8 || !(color_type == PNG_COLOR_TYPE_RGBA))
      {
        g_warning ("bpp or type mismatch");
        png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);
        fclose (infile);
        return -1;
      }
    /*if (color_type == PNG_COLOR_TYPE_RGBA)
      png_set_strip_alpha (load_png_ptr);
     */  
  }

  *row_stride = w * 4;
  *pixels = g_malloc0 (*row_stride * h);

  row_p = (png_bytep *) g_malloc (sizeof (png_bytep) * h);

  for (i=0; i< h; i++)
    row_p[i] = *pixels + *row_stride * i;

  png_read_image (load_png_ptr, row_p);
  png_read_end (load_png_ptr, NULL);

  png_destroy_read_struct (&load_png_ptr, &load_info_ptr, NULL);

  if (row_p)
    g_free (row_p);
  fclose (infile);

  return 0;
}


gint
horizon_save_png (const gchar *path,
                  gint         width,
                  gint         height,
                  gint         row_stride,
                  guchar      *data)
{
  FILE          *fp;
  gint           i;
  png_struct    *png;
  png_info      *info;
  png_byte     **rows;
  png_color_16   white;

  fp = fopen (path, "wb");
  if (!fp)
    {
      return -1;
    }
  
  rows = g_malloc (height * sizeof (png_byte*));
  if (rows == NULL)
    return -1;

  for (i=0; i<height; i++)
    rows[i] = (png_byte *) data + i * row_stride;

  png = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (png == NULL)
    return -1;

  info = png_create_info_struct (png);

  if (setjmp (png_jmpbuf (png)))
    return -1;
  png_set_compression_level (png, 1);
  png_init_io (png, fp);

  png_set_IHDR (png, info,
     width, height, 8, PNG_COLOR_TYPE_RGB_ALPHA,
     PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_DEFAULT);

  white.red = 0xff;
  white.blue = 0xff;
  white.green = 0xff;
  png_set_bKGD (png, info, &white);

  png_write_info (png, info);
  png_write_image (png, rows);
  png_write_end (png, info);

  png_destroy_write_struct (&png, &info);
  g_free (rows);
  fclose (fp);

  return 0;
}

#if 0
/* Disabled code that allocates a buffer the size of the image */

gint
canvas_import_png (Canvas      *canvas,
                       const gchar *path,
                       gint         x,
                       gint         y)
{
  CanvasRect *rect;
  gint        width;
  gint        height;
  gint        result;
  gint        row_stride;
  guchar     *src;

  result = horizon_load_png (path, &width, &height, &row_stride, &src);
  if (result || !src)
    {
      g_warning ("failed to open png file %s for reading", path);
      return -1;
    }

  rect = canvas_rect_new (canvas, x, y, width, height);
  canvas_rect_import_buf (rect, src);
  g_object_unref (rect);

  g_free (src);
  return result;
}

gint
canvas_export_png (Canvas      *canvas,
                   const gchar *path,
                   gint         x,
                   gint         y,
                   gint         width,
                   gint         height)
{
  CanvasRect *rect;
  guchar     *data;
  gint        result;

  data = g_malloc (width * 4 * height);
  if (!data)
    {
      return -1;
    }
  
  rect = canvas_rect_new (canvas, x, y, width, height);
  canvas_rect_export_buf (rect, data);
  g_object_unref (rect);
  result = horizon_save_png (path, width, height, width * 4, data);

  g_free (data);
  return result;
}

#endif



#include <jpeglib.h>

gint
canvas_import_jpg (Canvas      *canvas,
                   const gchar *path,
                   gint         dest_x,
                   gint         dest_y)
{
  gint row_stride;
  struct jpeg_decompress_struct  cinfo;
  struct jpeg_error_mgr          jerr;
  FILE                          *infile;
  JSAMPARRAY                     buffer;
  guchar                        *pixels;
  int row=0;

  if ((infile = fopen (path, "r")) == NULL)
    {
      g_warning ("unable to open %s for jpeg import", path);
      return -1;
    }

  jpeg_create_decompress (&cinfo);
  cinfo.err = jpeg_std_error (&jerr);
  jpeg_stdio_src (&cinfo, infile);

  (void) jpeg_read_header (&cinfo, TRUE);
  (void) jpeg_start_decompress (&cinfo);

  if (cinfo.output_components != 3)
    {
      g_warning ("attempted to load non RGB JPEG");
      jpeg_destroy_decompress (&cinfo);
      return -1;
    }

  row_stride = cinfo.output_width * cinfo.output_components;

  if ((row_stride) % 2)
    (row_stride)++;

  /* allocated with the jpeg library, and freed with the decompress context */
  buffer = (*cinfo.mem->alloc_sarray)
    ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
  pixels = g_malloc (cinfo.output_width * 4);
  if (!pixels)
    {
      g_warning ("OOM in jpeg load");
      jpeg_destroy_decompress (&cinfo);
      return -1;
    }

  while (cinfo.output_scanline < cinfo.output_height)
    {
      CanvasRect *rect = canvas_rect_new (canvas, dest_x, dest_y + row++, cinfo.output_width, 1);
      jpeg_read_scanlines (&cinfo, buffer, 1);
      { /* transform scanline from RGB to RGBA */
        gint i;
        guchar *src=buffer[0];
        guchar *dst=pixels;
        for (i=0; i<cinfo.output_width; i++)
          {
             dst[0]=src[0];
             dst[1]=src[1];
             dst[2]=src[2];
             dst[3]=255;
             src+=3;
             dst+=4;
          }
      }
      canvas_rect_import_buf (rect, pixels);
      g_object_unref (rect);
    }
  jpeg_destroy_decompress (&cinfo);
  fclose (infile);
  g_free (pixels);
  return 0;
}


gint
horizon_query_jpg (const gchar *path,
                   gint        *width,
                   gint        *height)
{
  struct jpeg_decompress_struct  cinfo;
  struct jpeg_error_mgr          jerr;
  FILE                          *infile;

  if ((infile = fopen (path, "r")) == NULL)
    {
      g_warning ("unable to open %s for jpeg import", path);
      return -1;
    }

  jpeg_create_decompress (&cinfo);
  cinfo.err = jpeg_std_error (&jerr);
  jpeg_stdio_src (&cinfo, infile);

  (void) jpeg_read_header (&cinfo, TRUE);
  (void) jpeg_start_decompress (&cinfo);

  if (cinfo.output_components != 3)
    {
      g_warning ("attempted to load non RGB JPEG");
      jpeg_destroy_decompress (&cinfo);
      return -1;
    }

  if (width)
    *width = cinfo.output_width;
  if (height)
    *height = cinfo.output_height;

  jpeg_destroy_decompress (&cinfo);
  return 0;
}
