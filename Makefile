horizon:
	make -C src horizon
	cp src/horizon .

clean:
	make -C src clean
	rm -f horizon *~

strip:
	make -C src strip
	cp src/horizon .

test: $(BINARY)
	make -C src test
deb:
	dpkg-buildpackage -rfakeroot
